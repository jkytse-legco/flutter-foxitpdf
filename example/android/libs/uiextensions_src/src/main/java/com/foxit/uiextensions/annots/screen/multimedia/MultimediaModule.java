/**
 * Copyright (C) 2003-2018, Foxit Software Inc..
 * All Rights Reserved.
 * <p>
 * http://www.foxitsoftware.com
 * <p>
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */
package com.foxit.uiextensions.annots.screen.multimedia;


import android.content.Context;
import android.graphics.Canvas;
import android.os.Environment;

import com.foxit.sdk.PDFViewCtrl;
import com.foxit.uiextensions.Module;
import com.foxit.uiextensions.ToolHandler;
import com.foxit.uiextensions.UIExtensionsManager;
import com.foxit.uiextensions.utils.AppFileUtil;
import com.foxit.uiextensions.utils.ToolUtil;

import java.io.File;

public class MultimediaModule implements Module {

    private Context mContext;
    private PDFViewCtrl mPdfViewCtrl;
    private PDFViewCtrl.UIExtensionsManager mUiExtensionsManager;

    private MultimediaToolHandler mAudioToolHandler;
    private MultimediaToolHandler mVideoToolHandler;
    private MultimediaAnnotHandler mAnnotHandler;

    public MultimediaModule(Context context, PDFViewCtrl pdfViewCtrl, PDFViewCtrl.UIExtensionsManager uiExtensionsManager) {
        mContext = context;
        mPdfViewCtrl = pdfViewCtrl;
        mUiExtensionsManager = uiExtensionsManager;
    }

    @Override
    public String getName() {
        return Module.MODULE_NAME_MEDIA;
    }

    @Override
    public boolean loadModule() {
        if (mUiExtensionsManager != null && mUiExtensionsManager instanceof UIExtensionsManager) {
            UIExtensionsManager.Config mModulesConfig = ((UIExtensionsManager) mUiExtensionsManager).getModulesConfig();
            UIExtensionsManager.Config.AnnotConfig annotConfig = mModulesConfig.getAnnotConfig();

            //audio
            if (annotConfig.isLoadAudio()) {
                mAudioToolHandler = new MultimediaToolHandler(mContext, mPdfViewCtrl, ToolHandler.TH_TYPE_SCREEN_AUDIO);
                ((UIExtensionsManager) mUiExtensionsManager).registerToolHandler(mAudioToolHandler);
            }

            //video
            if (annotConfig.isLoadVideo()) {
                mVideoToolHandler = new MultimediaToolHandler(mContext, mPdfViewCtrl, ToolHandler.TH_TYPE_SCREEN_VIDEO);
                ((UIExtensionsManager) mUiExtensionsManager).registerToolHandler(mVideoToolHandler);
            }

            mAnnotHandler = new MultimediaAnnotHandler(mContext, mPdfViewCtrl);
            ToolUtil.registerAnnotHandler((UIExtensionsManager) mUiExtensionsManager, mAnnotHandler);
            ((UIExtensionsManager) mUiExtensionsManager).registerModule(this);
        }

        mPdfViewCtrl.registerRecoveryEventListener(memoryEventListener);
        mPdfViewCtrl.registerDrawEventListener(mDrawEventListener);
        return true;
    }

    @Override
    public boolean unloadModule() {
        if (mUiExtensionsManager != null && mUiExtensionsManager instanceof UIExtensionsManager) {

            if (mAudioToolHandler != null) {
                ((UIExtensionsManager) mUiExtensionsManager).unregisterToolHandler(mAudioToolHandler);
            }
            if (mVideoToolHandler != null) {
                ((UIExtensionsManager) mUiExtensionsManager).unregisterToolHandler(mVideoToolHandler);
            }

            mAnnotHandler.unregisterDocEventListener();
            mAnnotHandler.unregisterLifecycleListener();
            ToolUtil.unregisterAnnotHandler((UIExtensionsManager) mUiExtensionsManager, mAnnotHandler);
        }

        mPdfViewCtrl.unregisterRecoveryEventListener(memoryEventListener);
        mPdfViewCtrl.unregisterDrawEventListener(mDrawEventListener);

        // delete temp files
        String tempPath = Environment.getExternalStorageDirectory() + "/FoxitSDK/AttaTmp/";
        File tempFile = new File(tempPath);
        if (tempFile.exists()) {
            AppFileUtil.deleteFolder(tempFile, false);
        }
        return true;
    }

    PDFViewCtrl.IRecoveryEventListener memoryEventListener = new PDFViewCtrl.IRecoveryEventListener() {
        @Override
        public void onWillRecover() {
            if (mAnnotHandler.getAnnotMenu() != null && mAnnotHandler.getAnnotMenu().isShowing()) {
                mAnnotHandler.getAnnotMenu().dismiss();
            }
        }

        @Override
        public void onRecovered() {
        }
    };

    private PDFViewCtrl.IDrawEventListener mDrawEventListener = new PDFViewCtrl.IDrawEventListener() {

        @Override
        public void onDraw(int pageIndex, Canvas canvas) {
            mAnnotHandler.onDrawForControls(canvas);
        }
    };

}
