/**
 * Copyright (C) 2003-2018, Foxit Software Inc..
 * All Rights Reserved.
 * <p>
 * http://www.foxitsoftware.com
 * <p>
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */
package com.foxit.uiextensions.controls.menu;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.support.v4.app.FragmentActivity;
import android.text.Selection;
import android.text.Spannable;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.foxit.sdk.PDFException;
import com.foxit.sdk.PDFViewCtrl;
import com.foxit.sdk.addon.xfa.XFADoc;
import com.foxit.sdk.pdf.PDFDoc;
import com.foxit.uiextensions.Module;
import com.foxit.uiextensions.R;
import com.foxit.uiextensions.UIExtensionsManager;
import com.foxit.uiextensions.annots.form.FormFillerModule;
import com.foxit.uiextensions.controls.dialog.AppDialogManager;
import com.foxit.uiextensions.controls.dialog.UIMatchDialog;
import com.foxit.uiextensions.controls.dialog.UITextEditDialog;
import com.foxit.uiextensions.controls.dialog.fileselect.UIFileSelectDialog;
import com.foxit.uiextensions.controls.filebrowser.imp.FileItem;
import com.foxit.uiextensions.modules.DocInfoModule;
import com.foxit.uiextensions.modules.DocInfoView;
import com.foxit.uiextensions.modules.DynamicXFA.DynamicXFAModule;
import com.foxit.uiextensions.modules.snapshot.SnapshotDialogFragment;
import com.foxit.uiextensions.modules.snapshot.SnapshotPresenter;
import com.foxit.uiextensions.print.PrintModule;
import com.foxit.uiextensions.security.standard.PasswordConstants;
import com.foxit.uiextensions.security.standard.PasswordModule;
import com.foxit.uiextensions.utils.AppDisplay;
import com.foxit.uiextensions.utils.AppFileUtil;
import com.foxit.uiextensions.utils.AppResource;
import com.foxit.uiextensions.utils.AppUtil;
import com.foxit.uiextensions.utils.Event;
import com.foxit.uiextensions.utils.LayoutConfig;
import com.foxit.uiextensions.utils.UIToast;

import java.io.File;
import java.io.FileFilter;
import java.util.List;

public class MoreMenuView {
    private Context mContext = null;
    private PDFViewCtrl mPdfViewCtrl = null;
    private ViewGroup mParent = null;
    private MenuViewImpl mMoreMenu = null;
    private PopupWindow mMenuPopupWindow = null;
    private ViewGroup mRootView = null;
    private String mFilePath = null;
    private MenuItemImpl mImportMenuItem = null;
    private MenuItemImpl mExportMenuItem = null;
    private MenuItemImpl mResetMenuItem = null;
    private String mExportFilePath = null;
    private ProgressDialog mProgressDlg;
    private String mProgressMsg;

    public MoreMenuView(Context context, ViewGroup parent, PDFViewCtrl pdfViewCtrl) {
        mContext = context;
        mPdfViewCtrl = pdfViewCtrl;
        mParent = parent;
    }

    public void show() {

        this.showMoreMenu();
    }

    public void hide() {
        this.hideMoreMenu();
    }

    public MenuViewImpl getMoreMenu() {
        return mMoreMenu;
    }

    public void initView() {
        if (mMoreMenu == null) {
            mMoreMenu = new MenuViewImpl(mContext, new MenuViewImpl.MenuCallback() {
                @Override
                public void onClosed() {
                    hideMoreMenu();
                }
            });
        }
        setMoreMenuView(mMoreMenu.getContentView());
    }

    /**
     * add a menu item to more menu group, click this item will show the file information and permission.
     */
    public void addDocInfoItem() {
        MenuGroupImpl group = mMoreMenu.getMenuGroup(MoreMenuConfig.GROUP_FILE);
        if (group == null) {
            group = new MenuGroupImpl(mContext, MoreMenuConfig.GROUP_FILE, AppResource.getString(mContext, R.string.rd_menu_file));
            mMoreMenu.addMenuGroup(group);
        }

        MenuItemImpl item = new MenuItemImpl(mContext, MoreMenuConfig.ITEM_DOCINFO,
                AppResource.getString(mContext, R.string.rv_doc_info), 0,
                new MenuViewCallback() {
                    /**
                     * when click "Properties", will show the document information, and hide the current more menu.
                     */
                    @Override
                    public void onClick(MenuItemImpl item) {

                        DocInfoModule docInfoModule = (DocInfoModule) ((UIExtensionsManager) mPdfViewCtrl.getUIExtensionsManager()).getModuleByName(Module.MODULE_NAME_DOCINFO);
                        if (docInfoModule != null) {
                            DocInfoView docInfoView = docInfoModule.getView();
                            if (docInfoView != null)
                                docInfoView.show();
                        }

                        hideMoreMenu();
                    }
                });

        MenuItemImpl rfsItem = new MenuItemImpl(mContext, MoreMenuConfig.ITEM_REDUCE_FILE_SIZE,
                AppResource.getString(mContext, R.string.rv_doc_reduce_file_size), 0,
                new MenuViewCallback() {
                    @Override
                    public void onClick(MenuItemImpl item) {

                        ((UIExtensionsManager) mPdfViewCtrl.getUIExtensionsManager()).setSaveDocFlag(PDFDoc.e_SaveFlagXRefStream);
                        UIToast.getInstance(mContext).show(AppResource.getString(mContext, R.string.rv_doc_reduce_file_size_toast), Toast.LENGTH_LONG);
                    }
                });

        MenuItemImpl snapshotItem = new MenuItemImpl(mContext, MoreMenuConfig.ITEM_SNAPSHOT,
                AppResource.getString(mContext, R.string.rv_doc_snapshot), 0,
                new MenuViewCallback() {
                    @Override
                    public void onClick(MenuItemImpl item) {

                        //there should verify the security info
                        if (!((UIExtensionsManager) mPdfViewCtrl.getUIExtensionsManager()).getDocumentManager().on(mPdfViewCtrl).canCopy() ||
                                !((UIExtensionsManager) mPdfViewCtrl.getUIExtensionsManager()).getDocumentManager().on(mPdfViewCtrl).canCopyForAssess()) {
                            Toast.makeText(mContext, R.string.no_permission, Toast.LENGTH_LONG).show();
                            return;
                        }

                        FragmentActivity act = ((FragmentActivity) ((UIExtensionsManager) mPdfViewCtrl.getUIExtensionsManager()).getAttachedActivity());
                        SnapshotDialogFragment mFragment = (SnapshotDialogFragment) act.getSupportFragmentManager().findFragmentByTag("SnapshotFragment");
                        if (mFragment == null) {
                            mFragment = new SnapshotDialogFragment();
                            mFragment.setPdfViewCtrl(mPdfViewCtrl);
                            mFragment.setContext(mContext);
                            new SnapshotPresenter(mFragment);
                        }

                        AppDialogManager.getInstance().showAllowManager(mFragment, act.getSupportFragmentManager(), "SnapshotFragment", null);
                        hideMoreMenu();
                    }
                });

        mMoreMenu.addMenuItem(MoreMenuConfig.GROUP_FILE, item);
        mMoreMenu.addMenuItem(MoreMenuConfig.GROUP_FILE, rfsItem);
        mMoreMenu.addMenuItem(MoreMenuConfig.GROUP_FILE, snapshotItem);


    }

    public void setFilePath(String filePath) {
        mFilePath = filePath;
        DocInfoModule docInfoModule = (DocInfoModule) ((UIExtensionsManager) mPdfViewCtrl.getUIExtensionsManager()).getModuleByName(Module.MODULE_NAME_DOCINFO);
        if (docInfoModule != null) {
            docInfoModule.setFilePath(mFilePath);
        }

    }

    public void addFormItem(final Module module) {
        MenuGroupImpl group = mMoreMenu.getMenuGroup(MoreMenuConfig.GROUP_FORM);
        if (group == null) {
            group = new MenuGroupImpl(mContext, MoreMenuConfig.GROUP_FORM, "Form");
        }
        mMoreMenu.addMenuGroup(group);
        if (mImportMenuItem == null) {
            mImportMenuItem = new MenuItemImpl(
                    mContext,
                    MoreMenuConfig.ITEM_IMPORT_FORM,
                    AppResource.getString(mContext, R.string.menu_more_item_import),
                    0,
                    new MenuViewCallback() {
                        @Override
                        public void onClick(MenuItemImpl item) {
                            importFormFromXML(mPdfViewCtrl.isDynamicXFA() ? ((UIExtensionsManager) mPdfViewCtrl.getUIExtensionsManager()).getModuleByName(Module.MODULE_NAME_DYNAMICXFA) : module);
                        }
                    });
        }
        if (mExportMenuItem == null) {
            mExportMenuItem = new MenuItemImpl(
                    mContext,
                    MoreMenuConfig.ITEM_EXPORT_FORM,
                    AppResource.getString(mContext, R.string.menu_more_item_export),
                    0,
                    new MenuViewCallback() {
                        @Override
                        public void onClick(MenuItemImpl item) {
                            exportFormToXML(mPdfViewCtrl.isDynamicXFA() ? ((UIExtensionsManager) mPdfViewCtrl.getUIExtensionsManager()).getModuleByName(Module.MODULE_NAME_DYNAMICXFA) : module);
                        }
                    });
        }
        if (mResetMenuItem == null) {
            mResetMenuItem = new MenuItemImpl(
                    mContext,
                    MoreMenuConfig.ITEM_RESET_FORM,
                    AppResource.getString(mContext, R.string.menu_more_item_reset),
                    0,
                    new MenuViewCallback() {
                        @Override
                        public void onClick(MenuItemImpl item) {
                            resetForm(mPdfViewCtrl.isDynamicXFA() ? ((UIExtensionsManager) mPdfViewCtrl.getUIExtensionsManager()).getModuleByName(Module.MODULE_NAME_DYNAMICXFA) : module);
                        }
                    });
        }
        mMoreMenu.addMenuItem(MoreMenuConfig.GROUP_FORM, mImportMenuItem);
        mMoreMenu.addMenuItem(MoreMenuConfig.GROUP_FORM, mExportMenuItem);
        mMoreMenu.addMenuItem(MoreMenuConfig.GROUP_FORM, mResetMenuItem);

    }

    protected void reloadFormItems() {
        if (mImportMenuItem != null)
            mImportMenuItem.setEnable(false);
        if (mExportMenuItem != null)
            mExportMenuItem.setEnable(false);
        if (mResetMenuItem != null)
            mResetMenuItem.setEnable(false);
        PDFDoc doc = mPdfViewCtrl.getDoc();
        try {
            if (mPdfViewCtrl.isDynamicXFA()) {
                mImportMenuItem.setEnable(true);
                mResetMenuItem.setEnable(true);
                mExportMenuItem.setEnable(true);
            } else if (doc != null && doc.hasForm()) {
                if (((UIExtensionsManager) mPdfViewCtrl.getUIExtensionsManager()).getDocumentManager().on(mPdfViewCtrl).canFillForm()) {
                    mImportMenuItem.setEnable(true);
                    mResetMenuItem.setEnable(true);
                }
                if (((UIExtensionsManager) mPdfViewCtrl.getUIExtensionsManager()).getDocumentManager().on(mPdfViewCtrl).canCopy()) {
                    mExportMenuItem.setEnable(true);
                }
            }
        } catch (PDFException e) {
            e.printStackTrace();
        }
    }

    /**
     * create one popup window, which will contain the view of more menu
     *
     * @param view
     */
    private void setMoreMenuView(View view) {
        if (mMenuPopupWindow == null) {
            mMenuPopupWindow = new PopupWindow(view, RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT, true);
        }

        mMenuPopupWindow.setBackgroundDrawable(new ColorDrawable(0x00FFFFFF));
        mMenuPopupWindow.setAnimationStyle(R.style.View_Animation_RtoL);
        mMenuPopupWindow.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
            }
        });
    }

    /**
     * if rotate the screen, will reset the size of more menu.to fit the screen.
     */
    protected void onConfigurationChanged(Configuration newConfig) {
        if (mMenuPopupWindow != null && mMenuPopupWindow.isShowing()) {
            updateMoreMenu();
        }
    }

    private void showMoreMenu() {
        mRootView = (ViewGroup) mParent.getChildAt(0);
        int width = mRootView.getWidth();
        int height = mRootView.getHeight();
        if (AppDisplay.getInstance(mContext).isPad()) {
            float scale = LayoutConfig.RD_PANEL_WIDTH_SCALE_V;
            if (width > height) {
                scale = LayoutConfig.RD_PANEL_WIDTH_SCALE_H;
            }
            width = (int) (AppDisplay.getInstance(mContext).getScreenWidth() * scale);
        }
        mMenuPopupWindow.setWidth(width);
        mMenuPopupWindow.setHeight(height);
        mMenuPopupWindow.setInputMethodMode(PopupWindow.INPUT_METHOD_NEEDED);
        mMenuPopupWindow.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_NOTHING);
        mMenuPopupWindow.showAtLocation(mRootView, Gravity.RIGHT | Gravity.TOP, 0, 0);
    }

    private void updateMoreMenu() {
        int rootWidth = mParent.getWidth();
        int rootHeight = mParent.getHeight();

        boolean bVertical = mContext.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT;
        int width, height;
        if (bVertical) {
            height = Math.max(rootWidth, rootHeight);
            width = Math.min(rootWidth, rootHeight);
        } else {
            height = Math.min(rootWidth, rootHeight);
            width = Math.max(rootWidth, rootHeight);
        }
        if (AppDisplay.getInstance(mContext).isPad()) {
            float scale = LayoutConfig.RD_PANEL_WIDTH_SCALE_V;
            if (width > height) {
                scale = LayoutConfig.RD_PANEL_WIDTH_SCALE_H;
            }
            width = (int) (AppDisplay.getInstance(mContext).getScreenWidth() * scale);
        }
        mMenuPopupWindow.update(width, height);
    }

    private void hideMoreMenu() {
        if (mMenuPopupWindow.isShowing()) {
            mMenuPopupWindow.dismiss();
        }
    }

    private void importFormFromXML(final Module module) {
        final UIFileSelectDialog dialog = new UIFileSelectDialog(((UIExtensionsManager) mPdfViewCtrl.getUIExtensionsManager()).getAttachedActivity());
        dialog.init(new FileFilter() {
            @Override
            public boolean accept(File pathname) {
                if (pathname.isHidden() || !pathname.canRead()) return false;
                if (pathname.isFile() && !pathname.getName().toLowerCase().endsWith(".xml"))
                    return false;
                return true;
            }
        }, true);
        dialog.showDialog();
        dialog.setTitle(((UIExtensionsManager) mPdfViewCtrl.getUIExtensionsManager()).getAttachedActivity().getApplicationContext().getResources().getString(R.string.formfiller_import_title));
        dialog.setButton(UIMatchDialog.DIALOG_CANCEL | UIMatchDialog.DIALOG_OK);
        dialog.setButtonEnable(false, UIMatchDialog.DIALOG_OK);
        dialog.setListener(new UIMatchDialog.DialogListener() {
            @Override
            public void onResult(long btType) {
                if (btType == UIMatchDialog.DIALOG_OK) {
                    List<FileItem> files = dialog.getSelectedFiles();

                    dialog.dismiss();
                    hideMoreMenu();
                    if (module instanceof FormFillerModule) {
                        mProgressMsg = "Importing...";
                        showProgressDlg();
                        ((FormFillerModule) module).importFormFromXML(files.get(0).path, new Event.Callback() {
                            @Override
                            public void result(Event event, boolean success) {
                                dismissProgressDlg();
                                if (success) {
                                    ((UIExtensionsManager) mPdfViewCtrl.getUIExtensionsManager()).getDocumentManager().setDocModified(true);
                                }
                            }
                        });
                    } else if (module instanceof DynamicXFAModule) {
                        mProgressMsg = "Importing...";
                        showProgressDlg();
                        ((DynamicXFAModule) module).importData(files.get(0).path, new Event.Callback() {
                            @Override
                            public void result(Event event, boolean success) {
                                dismissProgressDlg();
                                if (success) {
                                    int pageIndex = mPdfViewCtrl.getCurrentPage();
                                    mPdfViewCtrl.refresh(pageIndex, new Rect(0, 0, mPdfViewCtrl.getPageViewWidth(pageIndex), mPdfViewCtrl.getPageViewHeight(pageIndex)));
                                    ((UIExtensionsManager) mPdfViewCtrl.getUIExtensionsManager()).getDocumentManager().setDocModified(true);
                                }
                            }
                        });
                    }
                } else if (btType == UIMatchDialog.DIALOG_CANCEL) {
                    dialog.dismiss();
                }
            }

            @Override
            public void onBackClick() {

            }
        });
    }

    public void exportFormToXML(final Module module) {
        hideMoreMenu();
        final UITextEditDialog dialog = new UITextEditDialog(((UIExtensionsManager) mPdfViewCtrl.getUIExtensionsManager()).getAttachedActivity()
        );
        dialog.setTitle(mContext.getResources().getString(R.string.formfiller_export_title));
        dialog.getInputEditText().setVisibility(View.VISIBLE);
        String fileNameWithoutExt = AppFileUtil.getFileNameWithoutExt(mFilePath);
        dialog.getInputEditText().setText(fileNameWithoutExt + ".xml");
        CharSequence text = dialog.getInputEditText().getText();
        if (text instanceof Spannable) {
            Spannable spanText = (Spannable) text;
            Selection.setSelection(spanText, 0, fileNameWithoutExt.length());
        }
        AppUtil.showSoftInput(dialog.getInputEditText());
        dialog.getCancelButton().setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.getOKButton().setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dialog.dismiss();

                String name = dialog.getInputEditText().getText().toString();
                if (name.toLowerCase().endsWith(".xml")) {
                    mExportFilePath = AppFileUtil.getFileFolder(mFilePath) + "/" + name;
                } else {
                    mExportFilePath = AppFileUtil.getFileFolder(mFilePath) + "/" + name + ".xml";
                }
                File file = new File(mExportFilePath);
                if (file.exists()) {

                    final UITextEditDialog rmDialog = new UITextEditDialog(((UIExtensionsManager) mPdfViewCtrl.getUIExtensionsManager()).getAttachedActivity());
                    rmDialog.setTitle(mContext.getString(R.string.fm_file_exist));
                    rmDialog.getPromptTextView().setText(mContext.getString(R.string.fx_string_filereplace_warning));
                    rmDialog.getInputEditText().setVisibility(View.GONE);
                    rmDialog.show();

                    rmDialog.getOKButton().setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            rmDialog.dismiss();
                            mProgressMsg = "Exporting...";
                            showProgressDlg();
                            Event.Callback callback = new Event.Callback() {
                                @Override
                                public void result(Event event, boolean success) {
                                    dismissProgressDlg();
                                    if (!success) {
                                        UIToast.getInstance(mContext).show(mContext.getResources().getString(R.string.formfiller_export_error));
                                    }
                                }
                            };

                            if (module instanceof FormFillerModule) {
                                ((FormFillerModule) module).exportFormToXML(mExportFilePath, callback);
                            } else if (module instanceof DynamicXFAModule) {
                                ((DynamicXFAModule) module).exportData(mExportFilePath, XFADoc.e_ExportDataTypeXML, callback);
                            }
                        }
                    });

                    rmDialog.getCancelButton().setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            rmDialog.dismiss();
                            exportFormToXML(module);
                        }
                    });
                } else {
                    mProgressMsg = "Exporting...";
                    showProgressDlg();
                    Event.Callback callback = new Event.Callback() {
                        @Override
                        public void result(Event event, boolean success) {
                            dismissProgressDlg();
                            if (!success) {
                                UIToast.getInstance(mContext).show(mContext.getResources().getString(R.string.formfiller_export_error));
                            }
                        }
                    };

                    if (module instanceof FormFillerModule) {
                        ((FormFillerModule) module).exportFormToXML(mExportFilePath, callback);
                    } else if (module instanceof DynamicXFAModule) {
                        ((DynamicXFAModule) module).exportData(mExportFilePath, XFADoc.e_ExportDataTypeXML, callback);
                    }
                }
            }
        });

        dialog.show();
    }

    private void resetForm(final Module module) {
        mProgressMsg = "Reseting...";
        showProgressDlg();
        Event.Callback callback = new Event.Callback() {
            @Override
            public void result(Event event, boolean success) {
                dismissProgressDlg();
                if (success){
                    ((UIExtensionsManager) mPdfViewCtrl.getUIExtensionsManager()).getDocumentManager().setDocModified(true);
                }
            }
        };

        if (module instanceof FormFillerModule) {
           ((FormFillerModule) module).resetForm(callback);
        } else if (module instanceof DynamicXFAModule) {
            ((DynamicXFAModule) module).resetForm(callback);
        }
        hideMoreMenu();
    }


    // For Password Encryption
    private MenuItemImpl enItem;
    private MenuItemImpl deItem;
    private UITextEditDialog mSwitchDialog;

    public void addPasswordItems(final PasswordModule module) {
        MenuGroupImpl group = mMoreMenu.getMenuGroup(MoreMenuConfig.GROUP_PROTECT);
        if (group == null) {
            group = new MenuGroupImpl(mContext,
                    MoreMenuConfig.GROUP_PROTECT,
                    AppResource.getString(mContext, R.string.menu_more_group_protect));
            mMoreMenu.addMenuGroup(group);
        }

        enItem = new MenuItemImpl(mContext, MoreMenuConfig.ITEM_PASSWORD,
                AppResource.getString(mContext, R.string.rv_doc_encrpty_standard), 0,
                new MenuViewCallback() {
                    @Override
                    public void onClick(MenuItemImpl item) {
                        try {
                            int type = mPdfViewCtrl.getDoc().getEncryptionType();
                            if (type != PDFDoc.e_EncryptNone) {
                                mSwitchDialog = new UITextEditDialog(((UIExtensionsManager) mPdfViewCtrl.getUIExtensionsManager()).getAttachedActivity());
                                mSwitchDialog.getInputEditText().setVisibility(View.GONE);
                                mSwitchDialog.setTitle(((UIExtensionsManager) mPdfViewCtrl.getUIExtensionsManager()).getAttachedActivity().getApplicationContext().getString(R.string.rv_doc_encrypt_standard_switch_title));
                                mSwitchDialog.getPromptTextView().setText(((UIExtensionsManager) mPdfViewCtrl.getUIExtensionsManager()).getAttachedActivity().getApplicationContext().getString(R.string.rv_doc_encrypt_standard_switch_content));
                                mSwitchDialog.getCancelButton().setOnClickListener(new View.OnClickListener() {

                                    @Override
                                    public void onClick(View v) {
                                        mSwitchDialog.dismiss();
                                    }
                                });
                                mSwitchDialog.getOKButton().setOnClickListener(new View.OnClickListener() {

                                    @Override
                                    public void onClick(View v) {
                                        mSwitchDialog.dismiss();
                                        if (module.getPasswordSupport() != null) {
                                            if (module.getPasswordSupport().getFilePath() == null) {
                                                module.getPasswordSupport().setFilePath(mFilePath);
                                            }
                                            module.getPasswordSupport().passwordManager(PasswordConstants.OPERATOR_TYPE_CREATE);
                                        }
                                    }
                                });
                                mSwitchDialog.show();
                            } else {
                                if (module.getPasswordSupport() != null) {
                                    if (module.getPasswordSupport().getFilePath() == null) {
                                        module.getPasswordSupport().setFilePath(mFilePath);
                                    }
                                    module.getPasswordSupport().passwordManager(PasswordConstants.OPERATOR_TYPE_CREATE);
                                }
                            }
                        } catch (PDFException e) {
                            e.printStackTrace();
                        }
                    }
                });

        deItem = new MenuItemImpl(
                mContext,
                MoreMenuConfig.ITEM_REMOVESECURITY_PASSWORD,
                AppResource.getString(mContext, R.string.menu_more_item_remove_encrytion),
                0,
                new MenuViewCallback() {
                    @Override
                    public void onClick(MenuItemImpl item) {
                        if (module.getPasswordSupport() != null) {
                            if (module.getPasswordSupport().getFilePath() == null) {
                                module.getPasswordSupport().setFilePath(mFilePath);
                            }
                            module.getPasswordSupport().passwordManager(PasswordConstants.OPERATOR_TYPE_REMOVE);
                        }
                    }
                });
    }

    public void reloadPasswordItem(PasswordModule module) {
        if (mPdfViewCtrl.getDoc() != null) {
            try {
                int encryptType = mPdfViewCtrl.getDoc().getEncryptionType();
                if (encryptType == PDFDoc.e_EncryptPassword) {
                    mMoreMenu.removeMenuItem(MoreMenuConfig.GROUP_PROTECT, MoreMenuConfig.ITEM_PASSWORD);
                    mMoreMenu.addMenuItem(MoreMenuConfig.GROUP_PROTECT, deItem);
                } else {
                    mMoreMenu.removeMenuItem(MoreMenuConfig.GROUP_PROTECT, MoreMenuConfig.ITEM_REMOVESECURITY_PASSWORD);
                    mMoreMenu.addMenuItem(MoreMenuConfig.GROUP_PROTECT, enItem);
                }
            } catch (PDFException e) {
                e.printStackTrace();
            }

            if (module.getSecurityHandler().isAvailable()) {
                enItem.setEnable(true);
                deItem.setEnable(true);
            } else {
                enItem.setEnable(false);
                deItem.setEnable(false);
            }
        } else {
            mMoreMenu.setGroupVisibility(View.GONE, MoreMenuConfig.GROUP_PROTECT);
        }
    }

    private MenuItemImpl mPrintItem = null;

    protected void addPrintItem(final PrintModule module) {
        MenuGroupImpl group = mMoreMenu.getMenuGroup(MoreMenuConfig.GROUP_PRINT);
        if (group == null) {
            group = new MenuGroupImpl(mContext, MoreMenuConfig.GROUP_PRINT, mContext.getString(R.string.menu_more_print));
        }
        mMoreMenu.addMenuGroup(group);

        if (mPrintItem == null) {
            mPrintItem = new MenuItemImpl(
                    mContext,
                    MoreMenuConfig.ITEM_PRINT_FILE,
                    mContext.getString(R.string.menu_more_print_item),
                    0,
                    new MenuViewCallback() {
                        @Override
                        public void onClick(MenuItemImpl item) {
                            module.showPrintSettingOptions();
                        }
                    });
        }
        mMoreMenu.addMenuItem(MoreMenuConfig.GROUP_PRINT, mPrintItem);
    }

    protected void reloadPrintItem() {
        if (mPrintItem != null) {
            mPrintItem.setEnable(false);
            boolean canPrint = ((UIExtensionsManager) mPdfViewCtrl.getUIExtensionsManager()).getDocumentManager().on(mPdfViewCtrl).canPrint();
            mPrintItem.setEnable((Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) && canPrint);
        }
    }

    private void showProgressDlg() {
        UIExtensionsManager uiExtensionsManager = (UIExtensionsManager) mPdfViewCtrl.getUIExtensionsManager();

        if (mProgressDlg == null && uiExtensionsManager.getAttachedActivity() != null) {
            mProgressDlg = new ProgressDialog(uiExtensionsManager.getAttachedActivity());
            mProgressDlg.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mProgressDlg.setCancelable(false);
            mProgressDlg.setIndeterminate(false);
        }

        if (mProgressDlg != null && !mProgressDlg.isShowing()) {
            mProgressDlg.setMessage(mProgressMsg);
            AppDialogManager.getInstance().showAllowManager(mProgressDlg, null);
        }
    }

    private void dismissProgressDlg() {
        if (mProgressDlg != null && mProgressDlg.isShowing()) {
            AppDialogManager.getInstance().dismiss(mProgressDlg);
            mProgressDlg = null;
        }
    }

}
