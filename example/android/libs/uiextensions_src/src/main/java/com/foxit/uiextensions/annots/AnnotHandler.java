/**
 * Copyright (C) 2003-2018, Foxit Software Inc..
 * All Rights Reserved.
 * <p>
 * http://www.foxitsoftware.com
 * <p>
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */
package com.foxit.uiextensions.annots;

import android.graphics.PointF;
import android.graphics.RectF;
import android.view.MotionEvent;

import com.foxit.sdk.PDFViewCtrl;
import com.foxit.sdk.pdf.annots.Annot;
import com.foxit.uiextensions.utils.Event;

public interface AnnotHandler extends PDFViewCtrl.IDrawEventListener {
    int TYPE_FREETEXT_TEXTBOX = 100;
    int TYPE_FREETEXT_CALLOUT = 101;
    int TYPE_FORMFIELD_SIGNATURE = 102;

    int TYPE_SCREEN_IMAGE = 201;
    int TYPE_SCREEN_MULTIMEDIA = 202;

    int getType();

    boolean annotCanAnswer(Annot annot);

    RectF getAnnotBBox(Annot annot);

    boolean isHitAnnot(Annot annot, PointF point);

    void onAnnotSelected(Annot annot, boolean reRender);

    void onAnnotDeselected(Annot annot, boolean reRender);

    void addAnnot(int pageIndex, AnnotContent content, boolean addUndo, Event.Callback result);

    void modifyAnnot(Annot annot, AnnotContent content, boolean addUndo, Event.Callback result);

    void removeAnnot(Annot annot, boolean addUndo, Event.Callback result);

    boolean onTouchEvent(int pageIndex, MotionEvent motionEvent, Annot annot);

    boolean onLongPress(int pageIndex, MotionEvent motionEvent, Annot annot);

    boolean onSingleTapConfirmed(int pageIndex, MotionEvent motionEvent, Annot annot);

    boolean shouldViewCtrlDraw(Annot annot);
}
