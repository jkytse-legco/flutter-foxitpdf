package com.foxit.uiextensions.modules.DynamicXFA;

import com.foxit.sdk.addon.xfa.XFAWidget;

public interface IXFAWidgetEventListener {
    void onXFAWidgetAdded(XFAWidget xfaWidget);
    void onXFAWidgetWillRemove(XFAWidget xfaWidget);
}
