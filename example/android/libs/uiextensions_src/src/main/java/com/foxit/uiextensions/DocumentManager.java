/**
 * Copyright (C) 2003-2018, Foxit Software Inc..
 * All Rights Reserved.
 * <p>
 * http://www.foxitsoftware.com
 * <p>
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */
package com.foxit.uiextensions;

import android.graphics.PointF;
import android.graphics.RectF;
import android.view.KeyEvent;
import android.view.MotionEvent;

import com.foxit.sdk.ActionCallback;
import com.foxit.sdk.PDFException;
import com.foxit.sdk.PDFViewCtrl;
import com.foxit.sdk.common.Library;
import com.foxit.sdk.pdf.PDFDoc;
import com.foxit.sdk.pdf.PDFPage;
import com.foxit.sdk.pdf.Signature;
import com.foxit.sdk.pdf.annots.Annot;
import com.foxit.sdk.pdf.annots.Ink;
import com.foxit.sdk.pdf.annots.Markup;
import com.foxit.sdk.pdf.objects.PDFArray;
import com.foxit.sdk.pdf.objects.PDFDictionary;
import com.foxit.sdk.pdf.objects.PDFObject;
import com.foxit.uiextensions.annots.AnnotContent;
import com.foxit.uiextensions.annots.AnnotEventListener;
import com.foxit.uiextensions.annots.AnnotHandler;
import com.foxit.uiextensions.textselect.BlankSelectToolHandler;
import com.foxit.uiextensions.textselect.TextSelectToolHandler;
import com.foxit.uiextensions.utils.AppAnnotUtil;
import com.foxit.uiextensions.utils.AppDmUtil;
import com.foxit.uiextensions.utils.AppUtil;
import com.foxit.uiextensions.utils.Event;
import com.foxit.uiextensions.utils.ToolUtil;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import static com.foxit.uiextensions.utils.AppAnnotUtil.ANNOT_SELECT_TOLERANCE;

public class DocumentManager extends AbstractUndo {
    private static final Lock lock = new ReentrantLock();

    protected Annot mCurAnnot = null;
//    private PDFViewCtrl mPdfViewCtrl;
    private WeakReference<PDFViewCtrl> mPdfViewCtrl;
    private ArrayList<AnnotEventListener> mAnnotEventListenerList = new ArrayList<AnnotEventListener>();
    private ActionCallback mActionCallback = null;

    private Boolean isSetActionCallback = Boolean.valueOf(false);
    private long mUserPermission = 0;
    private int mMDPPermission = 0; // signature permission
    private boolean mIsSign = false;
    private boolean mHasModifyTask = false;
    private boolean mIsCDRM = false;

    private boolean mIsDocModified = false;

    public DocumentManager on(PDFViewCtrl ctrl){
        AppUtil.requireNonNull(ctrl);
        if (mPdfViewCtrl.get() == null){
            mPdfViewCtrl = new WeakReference(ctrl);
        }
        return this;
    }

    public DocumentManager(PDFViewCtrl pdfViewCtrl) {
        mPdfViewCtrl = new WeakReference<PDFViewCtrl>(pdfViewCtrl);
    }

    public void setActionCallback(ActionCallback handler) {
        lock.lock();
        if (isSetActionCallback) return;
        mActionCallback = handler;
        isSetActionCallback = Library.setActionCallback(mActionCallback);
        lock.unlock();
    }

    public void reInit() {
        lock.lock();
        isSetActionCallback = false;
        lock.unlock();
    }

    public ActionCallback getActionCallback() {
        return mActionCallback;
    }

    public void resetActionCallback() {
        lock.lock();
        isSetActionCallback = Library.setActionCallback(mActionCallback);
        lock.unlock();
    }

    public void setCurrentAnnot(Annot annot) {
        if (mCurAnnot == annot) return;
        Annot lastAnnot = mCurAnnot;
        try {
            lock.lock();
            PDFViewCtrl viewCtrl = mPdfViewCtrl.get();
            if (viewCtrl != null){
                if (mCurAnnot != null && !mCurAnnot.isEmpty()) {

                    int type = AppAnnotUtil.getAnnotHandlerType(lastAnnot);
                    if (((UIExtensionsManager) viewCtrl.getUIExtensionsManager()).getAnnotHandlerByType(type) != null) {
                        ((UIExtensionsManager) viewCtrl.getUIExtensionsManager()).getAnnotHandlerByType(type).onAnnotDeselected(lastAnnot, true);
                    }
                }

                mCurAnnot = annot;
                if (annot != null && ((UIExtensionsManager) viewCtrl.getUIExtensionsManager()).getAnnotHandlerByType(AppAnnotUtil.getAnnotHandlerType(annot)) != null) {
                    if (annot.getUniqueID() == null) {
                        annot.setUniqueID(AppDmUtil.randomUUID(null));
                    }
                    ((UIExtensionsManager) viewCtrl.getUIExtensionsManager()).getAnnotHandlerByType(AppAnnotUtil.getAnnotHandlerType(annot)).onAnnotSelected(annot, true);
                }
            }
        } catch (PDFException e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }
        onAnnotChanged(lastAnnot, mCurAnnot);
    }

    public Annot getCurrentAnnot() {
        return mCurAnnot;
    }

    protected Annot getFocusAnnot() {
        if (mCurAnnot != null && !mCurAnnot.isEmpty()) {
            return mCurAnnot;
        }

        if (mEraseAnnotList.size() > 0) {
            return mEraseAnnotList.get(0);
        }

        return null;
    }

    public boolean shouldViewCtrlDraw(Annot annot) {
        try {
            lock.lock();
            if (mCurAnnot != null && !mCurAnnot.isEmpty() && mCurAnnot.getPage().getIndex() == annot.getPage().getIndex()) {
                int type = AppAnnotUtil.getAnnotHandlerType(mCurAnnot);
                AnnotHandler annotHandler = ((UIExtensionsManager) mPdfViewCtrl.get().getUIExtensionsManager()).getAnnotHandlerByType(type);
                if (annotHandler != null) {
                    return annotHandler.shouldViewCtrlDraw(annot);
                }
            }

            //for eraser
            if (annot.getType() != Annot.e_Ink) return true;
            for (int i = 0; i < mEraseAnnotList.size(); i ++) {
                Ink ink =  mEraseAnnotList.get(i);
                if (ink.getPage().getIndex() == annot.getPage().getIndex() &&
                        ink.getIndex() == annot.getIndex()) {
                    return false;
                }
            }
        } catch (PDFException e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }
        return true;
    }

    private int getMDPDigitalSignPermissionInDocument(PDFDoc document) throws PDFException {
        PDFDictionary catalog = document.getCatalog();
        if(catalog == null)
            return 0;
        PDFObject object = catalog.getElement("Perms");
        if (object == null) {
            return 0;
        }
        PDFDictionary perms = object.getDirectObject().getDict();
        if (perms == null) {
            return 0;
        }
        object = perms.getElement("DocMDP");
        if (object == null) {
            return 0;
        }
        PDFDictionary docMDP = object.getDirectObject().getDict();
        if (docMDP == null) {
            return 0;
        }
        object = docMDP.getElement("Reference");
        if (object == null) {
            return 0;
        }
        PDFArray reference = object.getDirectObject().getArray();
        if (reference == null) {
            return 0;
        }
        for (int i = 0; i < reference.getElementCount(); i++) {
            object = reference.getElement(i);
            if (object == null) {
                return 0;
            }
            PDFDictionary tmpDict = object.getDirectObject().getDict();
            if (tmpDict == null) {
                return 0;
            }
            object = tmpDict.getElement("TransformMethod");
            if (object == null) {
                return 0;
            }
            String transformMethod = object.getDirectObject().getWideString();
            if (!transformMethod.contentEquals("DocMDP")) {
                continue;
            }
            object = tmpDict.getElement("TransformParams");
            if (object == null) {
                return 0;
            }
            PDFDictionary transformParams = object.getDirectObject().getDict();
            if (transformParams == null
                    || transformParams == tmpDict) {
                return 0;
            }
            object = transformParams.getElement("P");
            if (object == null) {
                return 0;
            }
            return object.getDirectObject().getInteger();
        }
        return 0;
    }

    public boolean canPrint() {
        if (mPdfViewCtrl.get().getDoc() == null)
            return false;
        if (mPdfViewCtrl.get().isOwner()) return true;
        if (mIsCDRM) {
            return (mCDRMPermission & PDFDoc.e_PermPrint) != 0;
        }

        if (mPdfViewCtrl.get().isDynamicXFA()){
            return true;
        }

        return (mUserPermission & PDFDoc.e_PermPrint) != 0;
    }

    public boolean canPrintHighQuality() {
        if (mPdfViewCtrl.get().getDoc() == null)
            return false;
        if (mPdfViewCtrl.get().isOwner()) return true;
        if (mIsCDRM) {
            return (mCDRMPermission & PDFDoc.e_PermPrint) != 0 ||
                    (mCDRMPermission & PDFDoc.e_PermPrintHigh) != 0;
        }

        return (mUserPermission & PDFDoc.e_PermPrint) != 0 ||
                (mUserPermission & PDFDoc.e_PermPrintHigh) != 0;
    }

    public boolean canCopy() {
        PDFViewCtrl viewCtrl = mPdfViewCtrl.get();
        if (viewCtrl == null || viewCtrl.getDoc() == null)
            return false;
        if (viewCtrl.isOwner()) return true;
        if (mIsCDRM) {
            return (mCDRMPermission & PDFDoc.e_PermExtract) != 0;
        }

        return (mUserPermission & PDFDoc.e_PermExtract) != 0;
    }

    public boolean canCopyForAssess() {
        PDFViewCtrl viewCtrl = mPdfViewCtrl.get();
        if (viewCtrl == null || viewCtrl.getDoc() == null)
            return false;

        if (mPdfViewCtrl.get().isOwner()) return true;
        if (mIsCDRM) {
            return (mCDRMPermission & PDFDoc.e_PermExtractAccess) != 0 || (mCDRMPermission & PDFDoc.e_PermExtract) != 0;
        }
        return (mUserPermission & PDFDoc.e_PermExtractAccess) != 0 || (mUserPermission & PDFDoc.e_PermExtract) != 0;
    }

    public boolean canAssemble() {
        PDFViewCtrl viewCtrl = mPdfViewCtrl.get();
        if (viewCtrl == null || viewCtrl.getDoc() == null)
            return false;

        if (mIsCDRM) {
            if (viewCtrl.isOwner()) return true;
            return (mCDRMPermission & PDFDoc.e_PermAssemble) != 0
                    || (mCDRMPermission & PDFDoc.e_PermModify) != 0;
        }

        if (!canModifyFile() && !canSaveAsFile()) {
            return false;
        }

        UIExtensionsManager uiExtensionsManager = (UIExtensionsManager)viewCtrl.getUIExtensionsManager();
        if(uiExtensionsManager != null && uiExtensionsManager.getModuleByName(Module.MODULE_NAME_DIGITALSIGNATURE) != null)
        {

            if (mMDPPermission != 0) return false;
            if (mIsSign) return false;
        }
        if (viewCtrl.isOwner()) return true;
        return (mUserPermission & PDFDoc.e_PermAssemble) != 0
                || (mUserPermission & PDFDoc.e_PermModify) != 0;
    }

    public boolean canModifyContents() {
        return canModifyContents(false);
    }

    private boolean canModifyContents(boolean isJudgeAddSignature) {
        PDFViewCtrl viewCtrl = mPdfViewCtrl.get();
        if (viewCtrl == null || viewCtrl.getDoc() == null)
            return false;

        if (mIsCDRM) {
            if (viewCtrl.isOwner()) return true;
            return (mCDRMPermission & PDFDoc.e_PermModify) != 0;
        }

        if (!canModifyFile() && !canSaveAsFile()) {
            return false;
        }

        UIExtensionsManager uiExtensionsManager = (UIExtensionsManager)viewCtrl.getUIExtensionsManager();
        if(uiExtensionsManager != null && uiExtensionsManager.getModuleByName(Module.MODULE_NAME_DIGITALSIGNATURE)!= null)
        {

            if (mMDPPermission != 0) return false;
            if (!isJudgeAddSignature) {
                if (mIsSign) return false;
            }
        }
        if (viewCtrl.isOwner()) return true;
        return (mUserPermission & PDFDoc.e_PermModify) != 0;
    }

    public boolean canFillForm() {
        PDFViewCtrl viewCtrl = mPdfViewCtrl.get();
        if (viewCtrl == null || viewCtrl.getDoc() == null)
            return false;

        if (mIsCDRM) {
            if (viewCtrl.isOwner()) return true;
            return (mCDRMPermission & PDFDoc.e_PermFillForm) != 0
                    || (mCDRMPermission & PDFDoc.e_PermAnnotForm) != 0
                    || (mCDRMPermission & PDFDoc.e_PermModify) != 0;
        }

        if (!canModifyFile() && !canSaveAsFile()) {
            return false;
        }

        UIExtensionsManager uiExtensionsManager = (UIExtensionsManager)viewCtrl.getUIExtensionsManager();
        if(uiExtensionsManager != null && uiExtensionsManager.getModuleByName(Module.MODULE_NAME_DIGITALSIGNATURE)!= null)
        {
            if (mMDPPermission == 1) return false;
        }
        if (viewCtrl.isOwner()) return true;
        return (mUserPermission & PDFDoc.e_PermFillForm) != 0
                || (mUserPermission & PDFDoc.e_PermAnnotForm) != 0
                || (mUserPermission & PDFDoc.e_PermModify) != 0;
    }

    public boolean canAddAnnot() {
        PDFViewCtrl viewCtrl = mPdfViewCtrl.get();
        if (viewCtrl == null || viewCtrl.getDoc() == null)
            return false;

        if (viewCtrl.isDynamicXFA()) return false;

        if (mIsCDRM) {
            if (viewCtrl.isOwner()) return true;
            return (mCDRMPermission & PDFDoc.e_PermAnnotForm) != 0;
        }

        if (!canModifyFile() && !canSaveAsFile()) {
            return false;
        }

        UIExtensionsManager uiExtensionsManager = (UIExtensionsManager)viewCtrl.getUIExtensionsManager();
        if(uiExtensionsManager != null && uiExtensionsManager.getModuleByName(Module.MODULE_NAME_DIGITALSIGNATURE)!= null)
        {

            if (mMDPPermission == 1 || mMDPPermission == 2) return false;
        }
        if (viewCtrl.isOwner()) return true;
        return (mUserPermission & PDFDoc.e_PermAnnotForm) != 0;
    }

    public boolean canSigning() {
            if (mMDPPermission > 0)
                return false;
//        if (isSign()) return false;
        if (canAddAnnot() || canFillForm()) return true;
        return false;
    }

    public boolean canAddSignature() {
        PDFViewCtrl viewCtrl = mPdfViewCtrl.get();
        if (viewCtrl == null || viewCtrl.getDoc() == null)
            return false;

        if (viewCtrl.isDynamicXFA()) return false;
        if (isSign() && !canAddAnnot()) return false;
        if (canSigning() && canModifyContents(true)) return true;
        return false;
    }

    public boolean canModifyFile() {
        PDFViewCtrl viewCtrl = mPdfViewCtrl.get();
        if (viewCtrl == null || viewCtrl.getDoc() == null)
            return false;
        return true;
    }

    public boolean canSaveAsFile() {
        PDFViewCtrl viewCtrl = mPdfViewCtrl.get();
        if (viewCtrl == null || viewCtrl.getDoc() == null)
            return false;
        return true;
    }

    public boolean isSign() {
        PDFViewCtrl viewCtrl = mPdfViewCtrl.get();
        if (viewCtrl == null || viewCtrl.getDoc() == null)
            return false;

        return mIsSign;
    }

    public boolean isXFA() {
        PDFViewCtrl viewCtrl = mPdfViewCtrl.get();

        if (viewCtrl == null || viewCtrl.getDoc() == null)
            return false;

        try {
            return viewCtrl.getDoc().isXFA();
        } catch (PDFException e) {
            e.printStackTrace();
        }
        return false;
    }

    // annot event listener management.
    public void registerAnnotEventListener(AnnotEventListener listener) {
        mAnnotEventListenerList.add(listener);
    }

    public void unregisterAnnotEventListener(AnnotEventListener listener) {
        mAnnotEventListenerList.remove(listener);
    }

    public void onAnnotAdded(PDFPage page, Annot annot) {
        for (AnnotEventListener listener : mAnnotEventListenerList) {
            listener.onAnnotAdded(page, annot);
        }
    }

    public void onAnnotWillDelete(PDFPage page, Annot annot) {
        for (AnnotEventListener listener : mAnnotEventListenerList) {
            listener.onAnnotWillDelete(page, annot);
        }
    }

    public void onAnnotDeleted(PDFPage page, Annot annot) {
        for (AnnotEventListener listener : mAnnotEventListenerList) {
            listener.onAnnotDeleted(page, annot);
        }
    }

    public void onAnnotModified(PDFPage page, Annot annot) {
        for (AnnotEventListener listener : mAnnotEventListenerList) {
            listener.onAnnotModified(page, annot);
        }
    }

    public void onAnnotChanged(Annot lastAnnot, Annot currentAnnot) {
        for (AnnotEventListener listener : mAnnotEventListenerList) {
            listener.onAnnotChanged(lastAnnot, currentAnnot);
        }
    }

    protected List<Ink> mEraseAnnotList = new ArrayList<Ink>();
    public void onAnnotStartEraser(Ink annot) {
        mEraseAnnotList.add(annot);
    }

    public void onAnnotEndEraser(Ink annot) {
        mEraseAnnotList.remove(annot);
    }

    public static  boolean intersects(RectF a, RectF b) {
        return a.left < b.right && b.left < a.right
                && a.top > b.bottom && b.top > a.bottom;
    }

    public ArrayList<Annot> getAnnotsInteractRect(PDFPage page, RectF rect, int type) {
        ArrayList<Annot> annotList = new ArrayList<Annot>(4);
        try {
            int count = page.getAnnotCount();
            Annot annot = null;
            for (int i = 0; i < count; i++) {
                annot = AppAnnotUtil.createAnnot(page.getAnnot(i));
                if (annot == null || (annot.getFlags() & Annot.e_FlagHidden) != 0)
                    continue;

                PDFViewCtrl viewCtrl = mPdfViewCtrl.get();
                if (viewCtrl != null){
                    int _type = AppAnnotUtil.getAnnotHandlerType(annot);
                    AnnotHandler annotHandler = ToolUtil.getAnnotHandlerByType((UIExtensionsManager) viewCtrl.getUIExtensionsManager(), _type);
                    if (annotHandler != null) {
                        RectF bbox = annotHandler.getAnnotBBox(annot);
                        if (intersects(bbox, rect) && annot.getType() == type) {
                            annotList.add(annot);
                        }
                    }
                }
            }
        } catch (PDFException e) {
            e.printStackTrace();
        }

        return annotList;
    }

    public Annot getAnnot(PDFPage page, String nm) {
        if (page == null) {
            return null;
        }

        try {
            int count = page.getAnnotCount();
            Annot annot = null;
            for (int i = 0; i < count; i++) {
                annot = AppAnnotUtil.createAnnot(page.getAnnot(i));
                if (annot == null || annot.isEmpty() || annot.getUniqueID() == null) {
                    continue;
                }

                if (annot.getUniqueID().equals(nm)) {
                    return annot;
                }
            }
        } catch (PDFException e) {
            e.printStackTrace();
        }

        return null;
    }

    public void addAnnot(PDFPage page, AnnotContent content, boolean addUndo, Event.Callback result) {
        if (page == null) {
            return;
        }

        Annot annot = getAnnot(page, content.getNM());
        if (annot != null) {
            modifyAnnot(annot, content, addUndo, result);
            return;
        }
        PDFViewCtrl viewCtrl = mPdfViewCtrl.get();
        if (viewCtrl != null){
            try {

                UIExtensionsManager uiExtensionsManager = (UIExtensionsManager) viewCtrl.getUIExtensionsManager();
                AnnotHandler annotHandler = uiExtensionsManager.getAnnotHandlerByType(content.getType());
                if (annotHandler != null) {
                    annotHandler.addAnnot(page.getIndex(), content, addUndo, result);
                } else {
                    if (result != null) {
                        result.result(null, false);
                    }
                }
            }catch (PDFException e){
                e.printStackTrace();
            }
        }
    }

    public void modifyAnnot(Annot annot, AnnotContent content, boolean addUndo, Event.Callback result) {
        try {
            if (annot.getModifiedDateTime() != null && content.getModifiedDate() != null
                    && annot.getModifiedDateTime().equals(content.getModifiedDate())) {
                if (result != null) {
                    result.result(null, true);
                }
                return;
            }

            PDFViewCtrl viewCtrl = mPdfViewCtrl.get();
            if (viewCtrl != null){
                UIExtensionsManager uiExtensionsManager = (UIExtensionsManager) viewCtrl.getUIExtensionsManager();
                AnnotHandler annotHandler = uiExtensionsManager.getAnnotHandlerByType(AppAnnotUtil.getAnnotHandlerType(annot));
                if (annotHandler != null) {
                    annotHandler.modifyAnnot(annot, content, addUndo, result);
                } else {
                    if (result != null) {
                        result.result(null, false);
                    }
                }
            }
        } catch (PDFException e) {
            e.printStackTrace();
        }
    }

    public void removeAnnot(final Annot annot, boolean addUndo, final Event.Callback result) {
        if (annot == getCurrentAnnot()) {
            setCurrentAnnot(null);
        }

        PDFViewCtrl viewCtrl = mPdfViewCtrl.get();
        if (viewCtrl != null){
            UIExtensionsManager uiExtensionsManager = (UIExtensionsManager) viewCtrl.getUIExtensionsManager();
            AnnotHandler annotHandler = uiExtensionsManager.getAnnotHandlerByType(AppAnnotUtil.getAnnotHandlerType(annot));

            if (annotHandler != null) {
                annotHandler.removeAnnot(annot, addUndo, result);
                return;
            }
        }
    }

    //deal with annot
    public boolean onTouchEvent(int pageIndex, MotionEvent motionEvent) {
        Annot annot = null;
        AnnotHandler annotHandler = null;
        PDFPage page = null;
        int action = motionEvent.getActionMasked();
        try {
            switch (action) {
                case MotionEvent.ACTION_DOWN: {
                    PDFViewCtrl viewCtrl = mPdfViewCtrl.get();
                    if (viewCtrl != null){

                        annot = getCurrentAnnot();
                        if (annot != null && !annot.isEmpty()) {
                            int type = AppAnnotUtil.getAnnotHandlerType(annot);
                            annotHandler = ((UIExtensionsManager) viewCtrl.getUIExtensionsManager()).getAnnotHandlerByType(type);
                            if (annotHandler == null)
                                return false;
                            if (annotHandler.onTouchEvent(pageIndex, motionEvent, annot)) {
                                hideSelectorAnnotMenu(viewCtrl);
                                return true;
                            }
                        }
                        PointF pdfPoint = AppAnnotUtil.getPdfPoint(viewCtrl, pageIndex, motionEvent);

                        page = viewCtrl.getDoc().getPage(pageIndex);
                        if (page != null) {
                            annot = AppAnnotUtil.createAnnot(page.getAnnotAtPoint(new com.foxit.sdk.common.fxcrt.PointF(pdfPoint.x, pdfPoint.y), ANNOT_SELECT_TOLERANCE));
                        }
                    }
                    break;
                }
                case MotionEvent.ACTION_MOVE:
                case MotionEvent.ACTION_UP:
                case MotionEvent.ACTION_CANCEL:
                    annot = getCurrentAnnot();
                    break;
                default:
                    return false;
            }
            PDFViewCtrl viewCtrl = mPdfViewCtrl.get();
            if (annot != null && !annot.isEmpty() && viewCtrl != null) {
                int type = AppAnnotUtil.getAnnotHandlerType(annot);
                annotHandler = ((UIExtensionsManager) viewCtrl.getUIExtensionsManager()).getAnnotHandlerByType(type);
                if (annotHandler != null && annotHandler.annotCanAnswer(annot)) {
                    hideSelectorAnnotMenu(viewCtrl);
                    return annotHandler.onTouchEvent(pageIndex, motionEvent, annot);
                }
            }
        } catch (PDFException e1) {
            e1.printStackTrace();
        }
        return false;
    }

    public boolean onLongPress(int pageIndex, MotionEvent motionEvent) {
        Annot annot = null;
        AnnotHandler annotHandler = null;
        PDFPage page = null;
        try {
            PDFViewCtrl viewCtrl = mPdfViewCtrl.get();
            if (viewCtrl != null){

                boolean annotCanceled = false;
                annot = getCurrentAnnot();
                if (annot != null && !annot.isEmpty()) {
                    int type = AppAnnotUtil.getAnnotHandlerType(annot);
                    annotHandler = ((UIExtensionsManager) viewCtrl.getUIExtensionsManager()).getAnnotHandlerByType(type);
                    if (annotHandler != null && annotHandler.onLongPress(pageIndex, motionEvent, annot)) {
                        hideSelectorAnnotMenu(viewCtrl);
                        return true;
                    }
                    if (getCurrentAnnot() == null) {
                        annotCanceled = true;
                    }
                }
                PointF pdfPoint = AppAnnotUtil.getPdfPoint(viewCtrl, pageIndex, motionEvent);
                page = viewCtrl.getDoc().getPage(pageIndex);
                if (page != null && !page.isEmpty()) {
                    annot = AppAnnotUtil.createAnnot(page.getAnnotAtPoint(new com.foxit.sdk.common.fxcrt.PointF(pdfPoint.x, pdfPoint.y), ANNOT_SELECT_TOLERANCE));
                }
                if (annot != null && !annot.isEmpty() && AppAnnotUtil.isSupportGroup(annot)) {
                    annot = AppAnnotUtil.createAnnot(((Markup)annot).getGroupHeader());
                }

                if (annot != null && !annot.isEmpty()) {
                    int type = AppAnnotUtil.getAnnotHandlerType(annot);
                    annotHandler = ((UIExtensionsManager) viewCtrl.getUIExtensionsManager()).getAnnotHandlerByType(type);
                    if (annotHandler != null && annotHandler.annotCanAnswer(annot)) {
                        if (annotHandler.onLongPress(pageIndex, motionEvent, annot)) {
                            hideSelectorAnnotMenu(viewCtrl);
                            return true;
                        }
                    }
                }

                if (annotCanceled) return true;
            }
            return false;
        } catch (PDFException e1) {
            e1.printStackTrace();
        }

        return false;
    }

    public boolean onSingleTapConfirmed(int pageIndex, MotionEvent motionEvent) {
        Annot annot = null;
        AnnotHandler annotHandler = null;
        PDFPage page = null;
        try {
            PDFViewCtrl viewCtrl = mPdfViewCtrl.get();
            if (viewCtrl != null){

                boolean annotCanceled = false;
                annot = getCurrentAnnot();
                if (annot != null && !annot.isEmpty()) {
                    int type = AppAnnotUtil.getAnnotHandlerType(annot);
                    annotHandler = ((UIExtensionsManager) viewCtrl.getUIExtensionsManager()).getAnnotHandlerByType(type);
                    if (annotHandler != null && annotHandler.onSingleTapConfirmed(pageIndex, motionEvent, annot)) {
                        hideSelectorAnnotMenu(viewCtrl);
                        return true;
                    }
                    if (getCurrentAnnot() == null) {
                        annotCanceled = true;
                    }
                }
                PointF pdfPoint = AppAnnotUtil.getPdfPoint(viewCtrl, pageIndex, motionEvent);
                page = viewCtrl.getDoc().getPage(pageIndex);
                if (page != null && !page.isEmpty()) {
                    annot = AppAnnotUtil.createAnnot(page.getAnnotAtPoint(new com.foxit.sdk.common.fxcrt.PointF(pdfPoint.x, pdfPoint.y), ANNOT_SELECT_TOLERANCE));
                }
                if (annot != null && !annot.isEmpty() && AppAnnotUtil.isSupportGroup(annot)) {
                    annot = AppAnnotUtil.createAnnot(((Markup)annot).getGroupHeader());
                }

                if (annot != null && !annot.isEmpty()) {
                    int type = AppAnnotUtil.getAnnotHandlerType(annot);
                    annotHandler = ((UIExtensionsManager) viewCtrl.getUIExtensionsManager()).getAnnotHandlerByType(type);
                    if (annotHandler != null && annotHandler.annotCanAnswer(annot)) {
                        if (annotHandler.onSingleTapConfirmed(pageIndex, motionEvent, annot)) {
                            hideSelectorAnnotMenu(viewCtrl);
                            return true;
                        }
                    }
                }

                if (annotCanceled) {
                    return true;
                }
            }
            return false;
        } catch (PDFException e1) {
            e1.printStackTrace();
        }

        return false;
    }

    private void hideSelectorAnnotMenu(PDFViewCtrl pdfViewCtrl) {
        BlankSelectToolHandler blankSelectToolHandler = (BlankSelectToolHandler) ((UIExtensionsManager) pdfViewCtrl.getUIExtensionsManager()).getToolHandlerByType(ToolHandler.TH_TYPE_BLANKSELECT);
        if (blankSelectToolHandler != null) {
            blankSelectToolHandler.dismissMenu();
        }

        TextSelectToolHandler textSelectionTool = (TextSelectToolHandler) ((UIExtensionsManager) pdfViewCtrl.getUIExtensionsManager()).getToolHandlerByType(ToolHandler.TH_TYPE_TEXTSELECT);
        if (textSelectionTool != null) {
            textSelectionTool.mAnnotationMenu.dismiss();
        }
    }

    //for Undo&Redo

    @Override
    public void undo() {
        PDFViewCtrl viewCtrl = mPdfViewCtrl.get();

        if (viewCtrl != null){
            if (((UIExtensionsManager) viewCtrl.getUIExtensionsManager()).getCurrentToolHandler() != null) {
                ((UIExtensionsManager) viewCtrl.getUIExtensionsManager()).setCurrentToolHandler(null);
            }

            if (getCurrentAnnot() != null) {
                setCurrentAnnot(null);
            }

            if (haveModifyTasks()) {
                Runnable delayRunnable = new Runnable() {
                    @Override
                    public void run() {
                        undo();
                    }
                };
                viewCtrl.post(delayRunnable);
                return;
            }
        }
        super.undo();
    }

    @Override
    public void redo() {
        PDFViewCtrl viewCtrl = mPdfViewCtrl.get();

        if (viewCtrl != null){
            if (((UIExtensionsManager) viewCtrl.getUIExtensionsManager()).getCurrentToolHandler() != null) {
                ((UIExtensionsManager) viewCtrl.getUIExtensionsManager()).setCurrentToolHandler(null);
            }

            if (getCurrentAnnot() != null) {
                setCurrentAnnot(null);
            }

            if (haveModifyTasks()) {
                Runnable delayRunnable = new Runnable() {
                    @Override
                    public void run() {
                        redo();
                    }
                };
                viewCtrl.post(delayRunnable);
                return;
            }

        }
        super.redo();
    }

    @Override
    protected String getDiskCacheFolder() {
        PDFViewCtrl viewCtrl = mPdfViewCtrl.get();
        if (viewCtrl == null){
            return "";
        }
        return viewCtrl.getContext().getCacheDir().getParent();
    }

    @Override
    protected boolean haveModifyTasks() {
        return mHasModifyTask;
    }

    public void setHasModifyTask(boolean hasModifyTask) {
        this.mHasModifyTask = hasModifyTask;
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (mCurAnnot == null || mCurAnnot.isEmpty()) return false;
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            setCurrentAnnot(null);
            return true;
        }
        return false;
    }

    public boolean onKeyBack() {
        if (mCurAnnot == null || mCurAnnot.isEmpty()) return false;
        setCurrentAnnot(null);
        return true;
    }

    private boolean isOwner(PDFDoc doc) {
        try {
            if (!doc.isEncrypted()) return true;
            if (PDFDoc.e_PwdOwner == doc.getPasswordType()) {
                return true;
            }
        } catch (PDFException e) {
            e.printStackTrace();
        }
        return false;
    }


    private boolean isSign(PDFDoc doc) {
        try {
            int count = doc.getSignatureCount();
            for (int i = 0; i < count; i++) {
                Signature signature = doc.getSignature(i);
                if (signature != null && !signature.isEmpty() && signature.isSigned()) {
                    return true;
                }
            }
        } catch (PDFException e) {

        }
        return false;
    }

    protected void initDocProperties(final PDFDoc doc) {
        if (doc == null) return;

        try {
            mIsCDRM = doc.isCDRM();

            PDFViewCtrl viewCtrl = mPdfViewCtrl.get();
            if (viewCtrl != null){
                mUserPermission = viewCtrl.getUserPermission();
                UIExtensionsManager uiExtensionsManager = (UIExtensionsManager)viewCtrl.getUIExtensionsManager();
                if(uiExtensionsManager != null && uiExtensionsManager.getModuleByName(Module.MODULE_NAME_DIGITALSIGNATURE)!= null) {
                    mMDPPermission = getMDPDigitalSignPermissionInDocument(doc);
                }

                mIsSign = isSign(doc);
            }
        } catch (PDFException e) {
            e.printStackTrace();
        }
    }

    protected void destroy() {
        mAnnotEventListenerList.clear();
        mAnnotEventListenerList = null;
        mActionCallback = null;
        isSetActionCallback = false;
        mPdfViewCtrl.clear();
    }

    public boolean isDocModified() {
        return mIsDocModified;
    }

    public void setDocModified(boolean isModified) {
        mIsDocModified = isModified;
    }

    //for cdrm document
    private int mCDRMPermission = 0xffffffff;
    private boolean mIsAccessPages = false;
    protected ArrayList<Integer> mAccessPages = new ArrayList<>();
    protected String mWrapperContent;
    public void parseACL(String acl) {
        try {
            JSONArray jACLArray = new JSONArray(acl);
            if (jACLArray.length() < 1) return;
            JSONObject data = jACLArray.getJSONObject(0);
            // permission
            if (!data.isNull("permission")) {
                mCDRMPermission = data.getInt("permission");
            }

            //accessPages
            if (!data.isNull("accessPages")) {
                JSONObject json_accessPages = data.getJSONObject("accessPages");
                mIsAccessPages = json_accessPages.getInt("isAccessPages") == 1;

                if (mIsAccessPages) {
                    JSONArray jsonArray_pages = json_accessPages.getJSONArray("Pages");
                    for (int i = 0; i < jsonArray_pages.length(); i ++) {
                        Object o = jsonArray_pages.get(i);
                        if (o instanceof JSONArray) {
                            int start = ((JSONArray) o).getInt(0);
                            int end = ((JSONArray) o).getInt(1);
                            for (int n = start; n <= end; n ++) {
                                mAccessPages.add(n - 1);
                            }
                        } else {
                            mAccessPages.add((Integer) o - 1);
                        }
                    }
                    mWrapperContent = json_accessPages.getString("wrapperContent");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean isAccessPages() {
        return mIsAccessPages;
    }

    public String getWrapperContent() {
        return mWrapperContent;
    }

    public boolean isAccessPage(int pageIndex) {
        if (!mIsAccessPages) return true;
        if (mAccessPages.size() == 0) return false;
        return mAccessPages.contains(pageIndex);
    }
}
