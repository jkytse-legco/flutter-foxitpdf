/**
 * Copyright (C) 2003-2018, Foxit Software Inc..
 * All Rights Reserved.
 * <p>
 * http://www.foxitsoftware.com
 * <p>
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */
package com.foxit.uiextensions.modules.signature;

import android.graphics.Bitmap;
import android.graphics.Matrix;

import com.foxit.sdk.PDFException;
import com.foxit.sdk.Task;
import com.foxit.sdk.common.fxcrt.Matrix2D;
import com.foxit.sdk.pdf.graphics.GraphicsObject;
import com.foxit.sdk.pdf.graphics.ImageObject;
import com.foxit.sdk.pdf.PSI;
import com.foxit.uiextensions.utils.Event;


class SignaturePSITask extends Task {

    private SignatureEvent mEvent;
    protected static PSI mPsi;
    private static final int INK_DIAMETER_SCALE = 10;

    public SignaturePSITask(final SignatureEvent event, final Event.Callback callBack) {
        super(new CallBack() {
            @Override
            public void result(Task task) {
                if(event instanceof SignatureDrawEvent) {
                    if(((SignatureDrawEvent) event).mCallBack!=null)
                        ((SignatureDrawEvent) event).mCallBack.result(event,true);

                }else if(event instanceof SignatureSignEvent){

                    if(((SignatureSignEvent) event).mCallBack!=null)
                        ((SignatureSignEvent) event).mCallBack.result(event,true);
                }

                if(callBack!= null)
                    callBack.result(event, true);

            }
        });
        mEvent= event;
    }

    public Bitmap getBitmap()
    {

        try {
            if(mPsi!= null && !mPsi.isEmpty())
                return mPsi.getBitmap();
        } catch (PDFException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void execute() {

        if(mEvent instanceof SignatureDrawEvent) {
            try {
            if(((SignatureDrawEvent) mEvent).mType == SignatureConstants.SG_EVENT_DRAW)
            {
                //Currently the demo turns off simulation of Pressure Sensitive Ink
                mPsi = new PSI(((SignatureDrawEvent) mEvent).mBitmap, false);
                mPsi.setColor(((SignatureDrawEvent) mEvent).mColor);
                int diameter = (int) (((SignatureDrawEvent) mEvent).mThickness* INK_DIAMETER_SCALE);
                if(diameter == 0) {
                    diameter = 1;
                }
                mPsi.setDiameter(diameter);
                mPsi.setOpacity(1f);
            }
            else if(((SignatureDrawEvent) mEvent).mType == SignatureConstants.SG_EVENT_THICKNESS) {
                int diameter = (int) (((SignatureDrawEvent) mEvent).mThickness * INK_DIAMETER_SCALE);
                if(diameter == 0) {
                    diameter = 1;
                }
                mPsi.setDiameter(diameter);
            }
            else if(((SignatureDrawEvent) mEvent).mType == SignatureConstants.SG_EVENT_COLOR) {
                mPsi.setColor((int) ((SignatureDrawEvent) mEvent).mColor);
            }

            } catch (PDFException e) {
                e.printStackTrace();
            }

        }
        else if(mEvent instanceof SignatureSignEvent)
        {
            try {
                ImageObject imageObject = ImageObject.create(((SignatureSignEvent) mEvent).mPage.getDocument());
                imageObject.setBitmap(((SignatureSignEvent) mEvent).mBitmap, null);

                Matrix matrix = new Matrix();
                float width = ((SignatureSignEvent) mEvent).mRect.width();
                float height = ((SignatureSignEvent) mEvent).mRect.height();
                matrix.setScale(Math.abs(width), Math.abs(height));
                matrix.postTranslate(((SignatureSignEvent) mEvent).mRect.left, ((SignatureSignEvent) mEvent).mRect.bottom);
                float[] values = new float[9];
                matrix.getValues(values);
                Matrix2D matrix2D = new Matrix2D(values[0], values[3], values[1], values[4], values[2], values[5]);
                imageObject.setMatrix(matrix2D);
                long pos = ((SignatureSignEvent) mEvent).mPage.getLastGraphicsObjectPosition(GraphicsObject.e_TypeAll);
                ((SignatureSignEvent) mEvent).mPage.insertGraphicsObject(pos,imageObject);
                ((SignatureSignEvent) mEvent).mPage.generateContent();
            } catch (PDFException e) {
                e.printStackTrace();
            }
        }
    }
}
