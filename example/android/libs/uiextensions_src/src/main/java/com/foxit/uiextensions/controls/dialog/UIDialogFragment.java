/**
 * Copyright (C) 2003-2018, Foxit Software Inc..
 * All Rights Reserved.
 * <p>
 * http://www.foxitsoftware.com
 * <p>
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */
package com.foxit.uiextensions.controls.dialog;


import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.PopupWindow;
import android.widget.Toast;

import com.foxit.sdk.PDFViewCtrl;
import com.foxit.uiextensions.R;
import com.foxit.uiextensions.UIExtensionsManager;
import com.foxit.uiextensions.utils.UIToast;


public class UIDialogFragment extends DialogFragment {
    private static final String TAG = UIDialogFragment.class.getSimpleName();

    private PDFViewCtrl.UIExtensionsManager mUiExtensionsManager;
    private Context mContext;

    private View mContentView;
    private Point mPosition = new Point();
    private PopupWindow.OnDismissListener mOnDismissListener;

    private boolean mIsShowing = false;

    private int mWidth;
    private int mHeight;
    private int mGrivaty;

    public static UIDialogFragment create(Context context, PDFViewCtrl.UIExtensionsManager uiExtensionsManager, View contentView) {
        UIDialogFragment popupDlg = new UIDialogFragment();
        popupDlg.init(context, uiExtensionsManager, contentView);
        return popupDlg;
    }

    void init(Context context, PDFViewCtrl.UIExtensionsManager uiExtensionsManager, View contentView) {
        this.mContext = context;
        this.mUiExtensionsManager = uiExtensionsManager;
        mContentView = contentView;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = new Dialog(getActivity(), R.style.rv_dialog_style);
        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        dialog.setCanceledOnTouchOutside(true);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0x00FFFFFF));
        dialog.getWindow().getAttributes().windowAnimations = R.style.View_Animation_LtoR;
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_NOTHING);
        if (mContentView != null) {
            if (mContentView.getParent() != null) {
                ViewGroup vg = (ViewGroup) mContentView.getParent();
                for (int i = 0; i < vg.getChildCount(); i++) {
                    if (vg.getChildAt(i) == mContentView) {
                        vg.removeView(mContentView);
                        break;
                    }
                }
            }

            dialog.setContentView(mContentView);
        }
        setDialogPositionAndSize(dialog);
        return dialog;
    }

    private void setDialogPositionAndSize(Dialog dialog) {
        if (dialog == null) return;
        Window window = dialog.getWindow();
        if (window != null) {
            window.setGravity(mGrivaty);
            WindowManager.LayoutParams lp = window.getAttributes();
            if (lp != null) {
                lp.x = mPosition.x;
                lp.y = mPosition.y;
                lp.width = mWidth;
                lp.height = mHeight;
                window.setAttributes(lp);
            }
        }
    }

    public void setWidth(int width) {
        mWidth = width;
        setDialogPositionAndSize(getDialog());
    }

    public void setHeight(int height) {
        mHeight = height;
        setDialogPositionAndSize(getDialog());
    }

    public void showAtLocation(View parent, int gravity, int x, int y) {
        mGrivaty = gravity;
        mPosition.x = x;
        mPosition.y = y;
        setDialogPositionAndSize(getDialog());

        if (!isShowing()) {
            if (mUiExtensionsManager == null) {
                return;
            }
            Activity activity = ((UIExtensionsManager) mUiExtensionsManager).getAttachedActivity();
            if (activity == null) {
                return;
            }

            if (!(activity instanceof FragmentActivity)) {
                UIToast.getInstance(mContext).show("The attached activity is not a FragmentActivity");
                return;
            }

            FragmentManager fm = ((FragmentActivity) activity).getSupportFragmentManager();
            AppDialogManager.getInstance().showAllowManager(this, fm, TAG, null);
        }

        mIsShowing = true;
    }

    @Override
    public void dismiss() {
        if (((UIExtensionsManager) mUiExtensionsManager).getAttachedActivity() != null) {
            super.dismiss();
        }
        mIsShowing = false;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mIsShowing = false;
        if (mOnDismissListener != null) {
            mOnDismissListener.onDismiss();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mIsShowing = false;
    }

    public void setOnDismissListener(PopupWindow.OnDismissListener onDismissListener) {
        mOnDismissListener = onDismissListener;
    }

    public boolean isShowing() {
        return mIsShowing;
    }

    public void update(int width, int height) {
        mWidth = width;
        mHeight = height;
        setDialogPositionAndSize(getDialog());
    }
}
