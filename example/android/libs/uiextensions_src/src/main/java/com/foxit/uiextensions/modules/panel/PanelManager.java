/**
 * Copyright (C) 2003-2018, Foxit Software Inc..
 * All Rights Reserved.
 * <p>
 * http://www.foxitsoftware.com
 * <p>
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */
package com.foxit.uiextensions.modules.panel;

import android.content.Context;
import android.content.res.Configuration;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupWindow;

import com.foxit.sdk.PDFViewCtrl;
import com.foxit.uiextensions.controls.dialog.UIDialogFragment;
import com.foxit.uiextensions.controls.panel.PanelHost;
import com.foxit.uiextensions.controls.panel.PanelSpec;
import com.foxit.uiextensions.controls.panel.impl.PanelHostImpl;
import com.foxit.uiextensions.utils.AppDisplay;
import com.foxit.uiextensions.utils.LayoutConfig;

public class PanelManager implements IPanelManager {

    private Context mContext;
    private ViewGroup mRootView;
    private PanelHost mPanel;
    private UIDialogFragment mPanelPopupWindow;
    private IPanelManager.OnShowPanelListener mShowListener = null;

    public PanelManager(Context context, PDFViewCtrl.UIExtensionsManager uiExtensionsManager, ViewGroup parent, PopupWindow.OnDismissListener dismissListener) {
        mContext = context;
        mRootView = parent;
        mPanel = new PanelHostImpl(mContext, new PanelHost.ICloseDefaultPanelCallback() {
            @Override
            public void closeDefaultPanel(View v) {
                if (mPanelPopupWindow != null && mPanelPopupWindow.isShowing()){
                    mPanelPopupWindow.dismiss();
                }
            }
        });
        setPanelView(mPanel.getContentView(), uiExtensionsManager, dismissListener);
    }

    private void setPanelView(final View view, PDFViewCtrl.UIExtensionsManager uiExtensionsManager, PopupWindow.OnDismissListener dismissListener) {
        mPanelPopupWindow = UIDialogFragment.create(mContext,uiExtensionsManager,view);
        if (dismissListener != null) {
            mPanelPopupWindow.setOnDismissListener(dismissListener);
        }
    }

    @Override
    public PanelHost getPanel() {
        return mPanel;
    }

    @Override
    public UIDialogFragment getPanelWindow() {
        return mPanelPopupWindow;
    }

    @Override
    public void setOnShowPanelListener(OnShowPanelListener listener) {
        mShowListener = listener;
    }

    @Override
    public void showPanel() {
        if (mPanel.getCurrentSpec() == null) {
            showPanel(PanelSpec.PanelType.Outline);
        } else {
            showPanel(mPanel.getCurrentSpec().getPanelType());
        }
    }

    @Override
    public void showPanel(PanelSpec.PanelType panelType) {
        int[] location = new int[2];
        mRootView.getLocationOnScreen(location);
        int y = location[1];

        int width = mRootView.getWidth();
        int height = mRootView.getHeight();
        if (AppDisplay.getInstance(mContext).isPad()) {
            float scale = LayoutConfig.RD_PANEL_WIDTH_SCALE_V;
            if (width > height) {
                scale = LayoutConfig.RD_PANEL_WIDTH_SCALE_H;
            }
            width = (int) (AppDisplay.getInstance(mContext).getScreenWidth() * scale);
        }
        mPanelPopupWindow.setWidth(width);
        mPanelPopupWindow.setHeight(height + y);
        resetPanelFocus(panelType);
        mPanelPopupWindow.showAtLocation(mRootView, Gravity.LEFT | Gravity.TOP, 0, 0);
        if (mShowListener != null) {
            mShowListener.onShow();
        }
    }

    @Override
    public void updatePanel(){
        int rootWidth = mRootView.getWidth();
        int rootHeight = mRootView.getHeight();

        boolean bVertical = mContext.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT;
        int width, height;
        if (bVertical) {
            height = Math.max(rootWidth, rootHeight);
            width = Math.min(rootWidth, rootHeight);
        } else {
            height = Math.min(rootWidth, rootHeight);
            width = Math.max(rootWidth, rootHeight);
        }

        if (AppDisplay.getInstance(mContext).isPad()) {
            float scale = LayoutConfig.RD_PANEL_WIDTH_SCALE_V;
            if (width > height) {
                scale = LayoutConfig.RD_PANEL_WIDTH_SCALE_H;
            }
            width = (int) (AppDisplay.getInstance(mContext).getScreenWidth() * scale);
        }
        mPanelPopupWindow.update(width,height);
        if (mShowListener != null) {
            mShowListener.onShow();
        }
    }

    private void resetPanelFocus(PanelSpec.PanelType panelType) {
        if (panelType != null) {
            mPanel.setCurrentSpec(panelType);
        }
    }

    @Override
    public void hidePanel() {
        if (mPanelPopupWindow.isShowing()) {
            mPanelPopupWindow.dismiss();
        }
    }
}
