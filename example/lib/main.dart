import 'dart:typed_data';
import 'dart:io';
import 'dart:async' show Future;
import 'package:flutter/services.dart' show rootBundle;

import 'package:flutter/material.dart';
import 'dart:async';

import 'package:flutter/services.dart';
import 'package:flutter_foxitpdf/flutter_foxitpdf.dart';
import 'package:path_provider/path_provider.dart';
// import 'package:permission/permission.dart';
import 'package:path/path.dart' as Path;

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  String _platformVersion = 'Unknown';
  int _error = -1;

  String _sn = 'f13hAT732tnBdUV7wf6WRVpbYGtVkCcLyKQlxTkX3gIVimlTC96uYw==';
  String _key = 'ezJvjl3GtGh397voL2Xkb3l6wo0NvErcymNKK3u5R57mhJ96b7hnzUH9nkhtIg4S3VzRQLXZ5kQltvw0wlkThnejDu7aSopeLq/3/74c2vg6ZJm3TTAEaeGKcs5zbCG+GQCaBBlaO+m2Gki/bsa9NWtbqNI0YioOZ6IYu9zYClvfWDg3Hfq5TetGoCDSsAEy/VyLHfrngIlLxA3+paKukMmPrCzuK1XlDSBYQn4DxDSGxq8MmObo9pUqRsBU1vMa82NPW6ydv17Nc9VvV9tf/flQt1OGmzcUfZtqp/EGTltZneMI/0YGCIX0yHRoGR5wnH7SvK/RSp39nXIyXRpMtjFlE2n2/MksUM/eJgJsPMwWIoh48rgJCukFczuAV0lsFVuHKxjBb35sGE9OQMkRVn8PMmWGIr/9pDQhK4MHoqUoHbelCPwxKjJZUXIx+Xlemb7aIWlVUf1VR5RvyXQtfTHK0Gi9Smi7xons5qaJIVw1EsMkLl0PUOpq95IqV52Fo/EVzuaJEDvEF3/1fSIPemNhU+llZeYunHdjN/gf8RvWnhhi7N6HD+b2TcMOS2TNICN3niL7+DRoJPjdFH/OQYi8JlvOo5/CZUZXvaw98Y87RTTA5ExPA89LGcUZa3VvwJAUhoGc991q9/3YlEhTzhluol1hvIjrEstf4SS+z5YFwDVTpqV8ICqbmqusegS8cDs6CpicCpLErsKXBDPSdMhn3faBn5Ow6xIs7MA85dHkOFM6MYPjJAZwNW42d+drrr1Ussj7Wedv8akCd2dJyzTPTGmbzWvl7RsG9Kpsvzdh0cCWu3kSRYF2p8oyHm2f6S3OVxU/XV0YTQ7/+L5y4+Le+5DjLrLdH1DMHb2FBQ/h14OycNBDZCx3tRO/0YdEfl9R0wnxw1EajoYaU68BoCXrjjyn0A4b2tRWP8Jce+ybJ3yX1wOT/+DW3f5bnlX6a18euXEKc70AuPZ7q74OZI1sMy342kxkpGwV2Cd5884HsltxqUnU/qxZTG63xmb2ttDyUSI+rYAmBLutR6pS8FjlT2+GgxwRvHajhlcd3vdQ/YzBm+ayyeroPov8CsoYMcLV3WV5NZFB4vs7C/xZFFEjsKwVf8Aq1pcni7qXYAr7kt3CAmlkpnzk5v7z8V7qUKu+f2nJH/v1twRUANwv6O1A1oUUz6tdHNxujCIQKEUrMgdcYobijenFwzlOVpQ4hWsaI3qv7Qy4/5yi0sDKrbSv5ll76jBzE1rgWP7xipHkFEfd+/ze';

  String _pdfAsset = '/assets/rop_c.pdf';
  String _filepath = '/SDCARD/rop1.pdf';
  //String _filepath = '/storage/eBooks/rop.pdf';

  @override
  void initState() {
    super.initState();
    initPlatformState();

    init(_sn, _key).then((String tempFilepath ) {
      File pdfFile = File(tempFilepath );
      pdfFile.exists().then((isExist) {
        print(tempFilepath  + ' exists: ' + isExist.toString());
        openDocument(tempFilepath, null);
      });

//      File pdfFile = File(appRoot+_pdfAsset);
//      pdfFile.exists().then((isExist) {
//        print('pdf path is.......................');
//        print(appRoot+_pdfAsset);
//        print('exists: ' + isExist.toString());
//        openDocument(appRoot+_pdfAsset, null);
//      });
    });
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initPlatformState() async {
    String platformVersion;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      platformVersion = await FlutterFoxitpdf.platformVersion;
    } on PlatformException {
      platformVersion = 'Failed to get platform version.';
    }
    // _filepath = await loadAsset(_pdfAsset);

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;

    setState(() {
      _platformVersion = platformVersion;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Plugin example app'),
        ),
        body: Center(
          child: Text('Running on: $_platformVersion\n'),
        ),
      ),
    );
  }

  Future<String> init(String sn, String key) async {
    int error;
    try {
      error = await FlutterFoxitpdf.initialize(sn, key);
    } on PlatformException {
      error = -1;
    }
    setState(() {
      _error = error;
    });

//    List<PermissionName> pList = new List<PermissionName>();
//    pList.add(PermissionName.Storage);
//    await Permission.requestPermissions(pList);
//    Directory appDocDir = await getApplicationDocumentsDirectory();
//    return appDocDir.path;
      String filepath = await copyAsset();
      return filepath;
  }

  Future<String> copyAsset() async {
    Directory tempDir = await getTemporaryDirectory();
    String tempPath = tempDir.path;
    File tempFile = File('$tempPath/rop.pdf');
    ByteData bd = await rootBundle.load('assets/rop_c.pdf');
    await tempFile.writeAsBytes(bd.buffer.asUint8List(), flush: true);
    return '$tempPath/rop.pdf';
  }

  Future<void> openDocument(String path, Uint8List password) async {
    await FlutterFoxitpdf.openDocument(path, password);
  }

  Future<String> loadAsset(String asset) async {
    return await rootBundle.loadString(asset);
  }
}
