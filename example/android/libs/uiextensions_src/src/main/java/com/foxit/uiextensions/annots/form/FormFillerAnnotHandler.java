/**
 * Copyright (C) 2003-2018, Foxit Software Inc..
 * All Rights Reserved.
 * <p>
 * http://www.foxitsoftware.com
 * <p>
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */
package com.foxit.uiextensions.annots.form;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Canvas;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.PaintFlagsDrawFilter;
import android.graphics.PathEffect;
import android.graphics.PointF;
import android.graphics.RectF;
import android.os.Build;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnKeyListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import com.foxit.sdk.PDFException;
import com.foxit.sdk.PDFViewCtrl;
import com.foxit.sdk.pdf.PDFPage;
import com.foxit.sdk.pdf.annots.Annot;
import com.foxit.sdk.pdf.annots.Widget;
import com.foxit.sdk.pdf.interform.Control;
import com.foxit.sdk.pdf.interform.Field;
import com.foxit.sdk.pdf.interform.Filler;
import com.foxit.sdk.pdf.interform.Form;
import com.foxit.uiextensions.Module;
import com.foxit.uiextensions.UIExtensionsManager;
import com.foxit.uiextensions.annots.AnnotContent;
import com.foxit.uiextensions.annots.AnnotHandler;
import com.foxit.uiextensions.utils.AppAnnotUtil;
import com.foxit.uiextensions.utils.AppDisplay;
import com.foxit.uiextensions.utils.AppDmUtil;
import com.foxit.uiextensions.utils.AppKeyboardUtil;
import com.foxit.uiextensions.utils.AppUtil;
import com.foxit.uiextensions.utils.Event;
import com.foxit.uiextensions.utils.ToolUtil;
import com.foxit.uiextensions.utils.thread.AppThreadManager;

import java.util.concurrent.CountDownLatch;


public class FormFillerAnnotHandler implements AnnotHandler {

    private Context mContext;
    private PDFViewCtrl mPdfViewCtrl;
    private ViewGroup mParent;
    private Filler mFormFiller;
    private FormFillerAssistImpl mAssist;
    private Form mForm;
    private EditText mEditView = null;
    private PointF mLastTouchPoint = new PointF(0, 0);
    private FormNavigationModule mFNModule = null;
    private int mPageOffset;
    private boolean mIsBackBtnPush = false; //for some input method, double backspace click
    private boolean mAdjustPosition = false;
    private boolean mIsShowEditText = false;
    private String mLastInputText = "";
    private String mChangeText = null;
    private Paint mPathPaint;
    private boolean bInitialize = false;
    private int mOffset;
    private Blink mBlink = null;

    public FormFillerAnnotHandler(Context context, ViewGroup parent, PDFViewCtrl pdfViewCtrl) {
        mContext = context;
        mPdfViewCtrl = pdfViewCtrl;
        mParent = parent;
    }

    public void init(final Form form) {
        mAssist = new FormFillerAssistImpl(mPdfViewCtrl);
        mAssist.bWillClose = false;
        mForm = form;
        mPathPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mPathPaint.setStyle(Paint.Style.STROKE);
        mPathPaint.setAntiAlias(true);
        mPathPaint.setDither(true);
        PathEffect effects = new DashPathEffect(new float[]{1, 2, 4, 8}, 1);
        mPathPaint.setPathEffect(effects);
        try {
            mFormFiller = new Filler(form, mAssist);

            boolean mEnableFormHighlight = ((UIExtensionsManager) mPdfViewCtrl.getUIExtensionsManager()).isFormHighlightEnable();
            mFormFiller.highlightFormFields(mEnableFormHighlight);
            if (mEnableFormHighlight) {
                mFormFiller.setHighlightColor((int) ((UIExtensionsManager) mPdfViewCtrl.getUIExtensionsManager()).getFormHighlightColor());
            }
        } catch (PDFException e) {
            e.printStackTrace();
            return;
        }

        initFormNavigation();
        bInitialize = true;
    }

    private void initFormNavigation() {
        mFNModule = (FormNavigationModule) ((UIExtensionsManager) mPdfViewCtrl.getUIExtensionsManager()).getModuleByName(Module.MODULE_NAME_FORM_NAVIGATION);
        if (mFNModule != null) {
            mFNModule.hide();
            mFNModule.getPreView().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AppThreadManager.getInstance().startThread(preNavigation);
                }
            });

            mFNModule.getNextView().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AppThreadManager.getInstance().startThread(nextNavigation);
                }
            });

            mFNModule.getClearView().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final Annot annot = ((UIExtensionsManager)mPdfViewCtrl.getUIExtensionsManager()).getDocumentManager().getCurrentAnnot();
                    if (annot != null && annot instanceof Widget) {
                        try {
                            PDFViewCtrl.lock();
                            Control formControl = ((Widget) annot).getControl();
                            mFormFiller.killFocus();
                            Field field = formControl.getField();
                            field.reset();
                            mFormFiller.setFocus(formControl);
                            refreshField(field);
                            ((UIExtensionsManager)mPdfViewCtrl.getUIExtensionsManager()).getDocumentManager().setDocModified(true);
                        } catch (PDFException e) {
                            e.printStackTrace();
                        } finally {
                            PDFViewCtrl.unlock();
                        }
                    }
                }
            });

            mFNModule.getFinishView().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (((UIExtensionsManager)mPdfViewCtrl.getUIExtensionsManager()).getDocumentManager().getCurrentAnnot() != null) {
                        if (shouldShowInputSoft(((UIExtensionsManager)mPdfViewCtrl.getUIExtensionsManager()).getDocumentManager().getCurrentAnnot())) {
                            if (mBlink != null)
                                mBlink.removeCallbacks((Runnable) mBlink);
                            mBlink = null;
                            AppUtil.dismissInputSoft(mEditView);
                            mParent.removeView(mEditView);
                        }
                        ((UIExtensionsManager)mPdfViewCtrl.getUIExtensionsManager()).getDocumentManager().setCurrentAnnot(null);
                    }
                    mFNModule.hide();
                    resetDocViewerOffset();
                }
            });

            mFNModule.setClearEnable(false);
        }

        ViewGroup parent = ((UIExtensionsManager)mPdfViewCtrl.getUIExtensionsManager()).getRootView();
        AppKeyboardUtil.setKeyboardListener(parent, parent, new AppKeyboardUtil.IKeyboardListener() {
            @Override
            public void onKeyboardOpened(int keyboardHeight) {
                if (Build.VERSION.SDK_INT < 14 && keyboardHeight < AppDisplay.getInstance(mContext).getRawScreenHeight() / 5) {
                    keyboardHeight = 0;
                }
                mFNModule.setPadding(0, 0, 0, keyboardHeight);
            }

            @Override
            public void onKeyboardClosed() {
            }
        });
    }

    protected boolean hasInitialized() {
        return bInitialize;
    }

    protected void showSoftInput(){
        AppUtil.showSoftInput(mEditView);
    }

    private void postDismissNavigation() {
        DismissNavigation dn = new DismissNavigation();
        dn.postDelayed(dn, 500);
    }

    private class DismissNavigation extends Handler implements Runnable {

        @Override
        public void run() {
            if (mPdfViewCtrl == null || mPdfViewCtrl.getDoc() == null) return;
            Annot annot = ((UIExtensionsManager)mPdfViewCtrl.getUIExtensionsManager()).getDocumentManager().getCurrentAnnot();
            if (annot == null || !(annot instanceof Widget)) {
                if (mFNModule != null)
                    mFNModule.getLayout().setVisibility(View.INVISIBLE);
                AppUtil.dismissInputSoft(mEditView);
                resetDocViewerOffset();
            }
        }
    }

    private boolean shouldShowNavigation(Annot annot) {
        if (annot == null) return false;
        if (!(annot instanceof Widget)) return false;
        if (FormFillerUtil.getAnnotFieldType(mForm, annot) == Field.e_TypePushButton) return false;

        return true;
    }

    public void navigationDismiss() {
        if (mFNModule != null) {
            mFNModule.hide();
            mFNModule.setPadding(0, 0, 0, 0);
        }
        if (mBlink != null)
            mBlink.removeCallbacks((Runnable) mBlink);
        mBlink = null;
        if (mEditView != null) {
            mParent.removeView(mEditView);
        }
        resetDocViewerOffset();
        AppUtil.dismissInputSoft(mEditView);
    }

    private boolean isFind = false;
    private boolean isDocFinish = false;
    private PDFPage curPage = null;
    private int prePageIdx;
    private int preAnnotIdx;
    private int nextPageIdx;
    private int nextAnnotIdx;
    private CountDownLatch mCountDownLatch;
    private Runnable preNavigation = new Runnable() {

        @Override
        public void run() {

            Annot curAnnot = ((UIExtensionsManager)mPdfViewCtrl.getUIExtensionsManager()).getDocumentManager().getCurrentAnnot();
            try {
                if (curAnnot != null && curAnnot instanceof Widget) {
//                    refreshField(((Widget) curAnnot).getField());
                    curPage = curAnnot.getPage();
                    final int curPageIdx = curPage.getIndex();
                    prePageIdx = curPageIdx;
                    final int curAnnotIdx = curAnnot.getIndex();
                    preAnnotIdx = curAnnotIdx;
                    isFind = false;
                    isDocFinish = false;
                    while (prePageIdx >= 0) {
                        mCountDownLatch = new CountDownLatch(1);
                        curPage = mPdfViewCtrl.getDoc().getPage(prePageIdx);
                        if (prePageIdx == curPageIdx && isDocFinish == false) {
                            preAnnotIdx = curAnnotIdx - 1;
                        } else {
                            preAnnotIdx = curPage.getAnnotCount() - 1;
                        }

                        while (curPage != null && preAnnotIdx >= 0) {
                            final Annot preAnnot = AppAnnotUtil.createAnnot(curPage.getAnnot(preAnnotIdx));
                            final int preAnnotType = FormFillerUtil.getAnnotFieldType(mForm, preAnnot);
                            if (preAnnot != null
                                    && preAnnot instanceof Widget
                                    && FormFillerUtil.isReadOnly(preAnnot) == false
                                    && FormFillerUtil.isVisible(preAnnot) == true
                                    && preAnnotType != Field.e_TypePushButton
                                    && preAnnotType != Field.e_TypeSignature) {
                                isFind = true;
                                AppThreadManager.getInstance().getMainThreadHandler().post(new Runnable() {

                                    @Override
                                    public void run() {
                                        try {
                                            UIExtensionsManager uiExtensionsManager = ((UIExtensionsManager) mPdfViewCtrl.getUIExtensionsManager());
                                            if (preAnnotType == Field.e_TypeComboBox) {
                                                RectF rect = AppUtil.toRectF(preAnnot.getRect());
                                                rect.left += 5;
                                                rect.top -= 5;
                                                mLastTouchPoint.set(rect.left, rect.top);
                                            }
                                            ((UIExtensionsManager)mPdfViewCtrl.getUIExtensionsManager()).getDocumentManager().setCurrentAnnot(null);
                                            if (preAnnot != null && !preAnnot.isEmpty()) {
                                                if (uiExtensionsManager.getCurrentToolHandler() != null) {
                                                    uiExtensionsManager.setCurrentToolHandler(null);
                                                }
                                                RectF bbox = AppUtil.toRectF(preAnnot.getRect());
                                                RectF rect = new RectF(bbox);

                                                if (mPdfViewCtrl.convertPdfRectToPageViewRect(rect, rect, prePageIdx)) {
                                                    float devX = rect.left - (mPdfViewCtrl.getWidth() - rect.width()) / 2;
                                                    float devY = rect.top - (mPdfViewCtrl.getHeight() - rect.height()) / 2;
                                                    mPdfViewCtrl.gotoPage(prePageIdx, devX, devY);
                                                } else {
                                                    mPdfViewCtrl.gotoPage(prePageIdx, new PointF(bbox.left, bbox.top));
                                                }

                                                ((UIExtensionsManager)mPdfViewCtrl.getUIExtensionsManager()).getDocumentManager().setCurrentAnnot(preAnnot);
                                                if (preAnnot instanceof Widget) {
                                                    mFormFiller.setFocus(((Widget) preAnnot).getControl());
                                                }
                                            }
                                        } catch (PDFException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                });

                                break;
                            } else {
                                preAnnotIdx--;
                            }
                        }
                        mCountDownLatch.countDown();

                        try {
                            if (mCountDownLatch.getCount() > 0)
                                mCountDownLatch.await();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }

                        if (isFind) break;
                        prePageIdx--;
                        if (prePageIdx < 0) {
                            prePageIdx = mPdfViewCtrl.getDoc().getPageCount() - 1;
                            isDocFinish = true;
                        }
                    }
                }
            } catch (PDFException e) {
                e.printStackTrace();
            }
        }
    };

    private void refreshField(Field field) {
        int nPageCount = mPdfViewCtrl.getPageCount();
        for (int i = 0; i < nPageCount; i++) {
            if (!mPdfViewCtrl.isPageVisible(i))
                continue;
            RectF rectF = getRefreshRect(field, i);
            if (rectF == null)
                continue;
            mPdfViewCtrl.convertPdfRectToPageViewRect(rectF, rectF, i);
            mPdfViewCtrl.refresh(i, AppDmUtil.rectFToRect(rectF));
        }
    }

    private RectF getRefreshRect(Field field, int pageIndex) {
        RectF rectF = null;
        try {
            PDFPage page = mPdfViewCtrl.getDoc().getPage(pageIndex);
            int nControlCount = field.getControlCount(page);
            for (int i = 0; i < nControlCount; i++) {
                Control formControl = field.getControl(page, i);
                if (rectF == null) {
                    rectF = AppUtil.toRectF(formControl.getWidget().getRect());
                } else {
                    rectF.union(AppUtil.toRectF(formControl.getWidget().getRect()));
                }
            }
        } catch (PDFException e) {
            e.printStackTrace();
        }
        return rectF;
    }

    private Runnable nextNavigation = new Runnable() {

        @Override
        public void run() {
            Annot curAnnot = ((UIExtensionsManager)mPdfViewCtrl.getUIExtensionsManager()).getDocumentManager().getCurrentAnnot();
            try {
                if (curAnnot != null && curAnnot instanceof Widget) {
//                    refreshField(((Widget) curAnnot).getField());
                    curPage = curAnnot.getPage();

                    final int curPageIdx = curPage.getIndex();
                    nextPageIdx = curPageIdx;
                    final int curAnnotIdx = curAnnot.getIndex();
                    nextAnnotIdx = curAnnotIdx;
                    isFind = false;
                    isDocFinish = false;

                    while (nextPageIdx < mPdfViewCtrl.getDoc().getPageCount()) {

                        mCountDownLatch = new CountDownLatch(1);
                        curPage = mPdfViewCtrl.getDoc().getPage(nextPageIdx);
                        if (nextPageIdx == curPageIdx && isDocFinish == false) {
                            nextAnnotIdx = curAnnotIdx + 1;
                        } else {
                            nextAnnotIdx = 0;
                        }

                        while (curPage != null && nextAnnotIdx < curPage.getAnnotCount()) {
                            final Annot nextAnnot = AppAnnotUtil.createAnnot(curPage.getAnnot(nextAnnotIdx));
                            final int nextAnnotType = FormFillerUtil.getAnnotFieldType(mForm, nextAnnot);
                            if (nextAnnot != null
                                    && nextAnnot instanceof Widget
                                    && FormFillerUtil.isReadOnly(nextAnnot) == false
                                    && FormFillerUtil.isVisible(nextAnnot) == true
                                    && nextAnnotType != Field.e_TypePushButton
                                    && nextAnnotType != Field.e_TypeSignature) {
                                isFind = true;

                                AppThreadManager.getInstance().getMainThreadHandler().post(new Runnable() {

                                    @Override
                                    public void run() {
                                        try {
                                            UIExtensionsManager uiExtensionsManager = ((UIExtensionsManager) mPdfViewCtrl.getUIExtensionsManager());
                                            if (nextAnnotType == Field.e_TypeComboBox) {
                                                RectF rect = AppUtil.toRectF(nextAnnot.getRect());
                                                rect.left += 5;
                                                rect.top -= 5;
                                                mLastTouchPoint.set(rect.left, rect.top);
                                            }
                                            uiExtensionsManager.getDocumentManager().setCurrentAnnot(null);
                                            if (nextAnnot != null && !nextAnnot.isEmpty()) {
                                                if (uiExtensionsManager.getCurrentToolHandler() != null) {
                                                    uiExtensionsManager.setCurrentToolHandler(null);
                                                }
                                                RectF bbox = AppUtil.toRectF(nextAnnot.getRect());
                                                RectF rect = new RectF(bbox);

                                                if (mPdfViewCtrl.convertPdfRectToPageViewRect(rect, rect, nextPageIdx)) {
                                                    float devX = rect.left - (mPdfViewCtrl.getWidth() - rect.width()) / 2;
                                                    float devY = rect.top - (mPdfViewCtrl.getHeight() - rect.height()) / 2;
                                                    mPdfViewCtrl.gotoPage(nextPageIdx, devX, devY);
                                                } else {
                                                    mPdfViewCtrl.gotoPage(nextPageIdx, new PointF(bbox.left, bbox.top));
                                                }

                                                ((UIExtensionsManager)mPdfViewCtrl.getUIExtensionsManager()).getDocumentManager().setCurrentAnnot(nextAnnot);
                                                if (nextAnnot instanceof Widget) {
                                                    mFormFiller.setFocus(((Widget) nextAnnot).getControl());
                                                }
                                            }
                                        } catch (PDFException e) {
                                            e.printStackTrace();
                                        }
                                    }

                                });

                                break;
                            } else {
                                nextAnnotIdx++;
                            }
                        }
                        mCountDownLatch.countDown();

                        try {
                            if (mCountDownLatch.getCount() > 0)
                                mCountDownLatch.await();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }

                        if (isFind) break;
                        nextPageIdx++;
                        if (nextPageIdx >= mPdfViewCtrl.getDoc().getPageCount()) {
                            nextPageIdx = 0;
                            isDocFinish = true;
                        }
                    }
                }
            } catch (PDFException e) {
                e.printStackTrace();
            }
        }
    };

    protected void clear() {
        if (mAssist != null) {
            mAssist.bWillClose = true;
        }

        mForm = null;

        if (mFormFiller != null) {
//            mFormFiller.delete();
            mFormFiller = null;
        }
    }

    public FormFillerAssistImpl getFormFillerAssist() {
        return mAssist;
    }

    @Override
    public int getType() {
        return Annot.e_Widget;
    }


    @Override
    public boolean annotCanAnswer(Annot annot) {
        return true;
    }

    @Override
    public RectF getAnnotBBox(Annot annot) {

        try {
            return AppUtil.toRectF(annot.getRect());
        } catch (PDFException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean isHitAnnot(Annot annot, PointF point) {
        try {
            RectF r = AppUtil.toRectF(annot.getRect());
            RectF rf = new RectF(r.left, r.top, r.right, r.bottom);
            PointF p = new PointF(point.x, point.y);
            int pageIndex = annot.getPage().getIndex();
            Control control = AppAnnotUtil.getControlAtPos(annot.getPage(), p, 1);

            mPdfViewCtrl.convertPdfRectToPageViewRect(rf, rf, pageIndex);
            mPdfViewCtrl.convertPdfPtToPageViewPt(p, p, pageIndex);

            if (rf.contains(p.x, p.y)) {
                return true;
            } else {
                if (AppAnnotUtil.isSameAnnot(annot, control != null? control.getWidget():null))
                    return true;
            }
        } catch (PDFException e) {
            e.printStackTrace();
        }

        return false;
    }


    public void onBackspaceBtnDown() {
        try {
            mFormFiller.onChar((char) 8, 0);
        } catch (PDFException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onAnnotSelected(final Annot annot, boolean needInvalid) {
        if (shouldShowInputSoft(annot)) {
            mIsShowEditText = true;
            mAdjustPosition = true;
            mLastInputText = " ";

            if (mEditView != null) {
                mParent.removeView(mEditView);
            }
            mEditView = new EditText(mContext);
            mEditView.setLayoutParams(new LayoutParams(1, 1));
            mEditView.setSingleLine(false);
            mEditView.setText(" ");
            mEditView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    if (actionId == EditorInfo.IME_ACTION_DONE) {
                        AppThreadManager.getInstance().startThread(nextNavigation);
                        return true;
                    }
                    return false;
                }
            });

            mParent.addView(mEditView);
            showSoftInput();

            mEditView.setOnKeyListener(new OnKeyListener() {
                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    if (keyCode == KeyEvent.KEYCODE_DEL && event.getAction() == KeyEvent.ACTION_DOWN) {
                        onBackspaceBtnDown();
                        mIsBackBtnPush = true;
                    }
                    return false;
                }
            });

            mEditView.addTextChangedListener(new TextWatcher() {

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    try {
                        if (s.length() >= mLastInputText.length()) {
                            String afterchange = s.subSequence(start, start + before).toString();
                            if (mChangeText.equals(afterchange)) {
                                for (int i = 0; i < s.length() - mLastInputText.length(); i++) {
                                    char c = s.charAt(mLastInputText.length() + i);
                                    if (FormFillerUtil.isEmojiCharacter((int) c))
                                        break;
                                    if ((int) c == 10)
                                        c = 13;
                                    final char value = c;

                                    mFormFiller.onChar(value, 0);
                                }
                            } else {
                                for (int i = 0; i < before; i++) {
                                    onBackspaceBtnDown();
                                }
                                for (int i = 0; i < count; i++) {
                                    char c = s.charAt(s.length() - count + i);

                                    if (FormFillerUtil.isEmojiCharacter((int) c))
                                        break;
                                    if ((int) c == 10)
                                        c = 13;
                                    final char value = c;

                                    mFormFiller.onChar(value, 0);
                                }
                            }
                        } else if (s.length() < mLastInputText.length()) {

                            if (mIsBackBtnPush == false){
                                for (int i=0; i<before; i++){
                                    onBackspaceBtnDown();
                                }

                                for (int i = 0; i < count; i++) {
                                    char c = s.charAt(s.length() - count + i);

                                    if (FormFillerUtil.isEmojiCharacter((int) c))
                                        break;
                                    if ((int) c == 10)
                                        c = 13;
                                    final char value = c;

                                    mFormFiller.onChar(value, 0);
                                }
                            }
                            mIsBackBtnPush = false;
                        }

                        if (s.toString().length() == 0)
                            mLastInputText = " ";
                        else
                            mLastInputText = s.toString();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count,
                                              int after) {
                    mChangeText = s.subSequence(start, start + count).toString();
                }

                @Override
                public void afterTextChanged(Editable s) {
                    if (s.toString().length() == 0)
                        s.append(" ");
                }
            });

            if (mBlink == null) {
                mBlink = new Blink(annot);
                mBlink.postDelayed((Runnable) mBlink, 300);
            } else {
                mBlink.setAnnot(annot);
            }
        }

        int fieldType = FormFillerUtil.getAnnotFieldType(mForm, annot);

        if (mFNModule != null) {
            if (!FormFillerUtil.isReadOnly(annot))
                mFNModule.setClearEnable(true);
            else
                mFNModule.setClearEnable(false);
            if (fieldType != Field.e_TypePushButton)
                mFNModule.show();
        }
    }

    @Override
    public void onAnnotDeselected(final Annot annot, boolean needInvalid) {
        postDismissNavigation();
        try {
            mFormFiller.killFocus();
            if (annot != null && annot instanceof Widget)
                refreshField(((Widget) annot).getField());
        } catch (PDFException e) {
            e.printStackTrace();
        }
        if (mIsShowEditText) {
            AppUtil.dismissInputSoft(mEditView);
            mParent.removeView(mEditView);
            mIsShowEditText = false;
        }
    }

    private boolean isDown = false;
    PointF oldPoint = new PointF();

    @Override
    public boolean onTouchEvent(int pageIndex, MotionEvent motionEvent, Annot annot) {
        try {
            if (!hasInitialized()) return false;
            if (!((UIExtensionsManager)mPdfViewCtrl.getUIExtensionsManager()).getDocumentManager().canFillForm()) return false;
            if (FormFillerUtil.isReadOnly(annot))
                return false;
            final PDFPage page = mPdfViewCtrl.getDoc().getPage(pageIndex);

            final RectF annotRectF = AppUtil.toRectF(annot.getRect());
            mPdfViewCtrl.convertPdfRectToPageViewRect(annotRectF, annotRectF, pageIndex);

            PointF devPt = new PointF(motionEvent.getX(), motionEvent.getY());
            PointF pageViewPt = new PointF();
            mPdfViewCtrl.convertDisplayViewPtToPageViewPt(devPt, pageViewPt, pageIndex);
            PointF pdfPointF = new PointF();
            mPdfViewCtrl.convertPageViewPtToPdfPt(pageViewPt, pdfPointF, pageIndex);


            int action = motionEvent.getActionMasked();
            switch (action) {

                case MotionEvent.ACTION_DOWN:
                    if (annot == ((UIExtensionsManager)mPdfViewCtrl.getUIExtensionsManager()).getDocumentManager().getCurrentAnnot() && pageIndex == annot.getPage().getIndex() && isHitAnnot(annot, pdfPointF)) {
                        isDown = true;
                        mFormFiller.onLButtonDown(page, AppUtil.toFxPointF(pdfPointF), 0);
                        return true;
                    }
                    return false;
                case MotionEvent.ACTION_MOVE:
                    if (getDistanceOfPoints(pageViewPt, oldPoint) > 0 && annot == ((UIExtensionsManager)mPdfViewCtrl.getUIExtensionsManager()).getDocumentManager().getCurrentAnnot() && pageIndex == annot.getPage().getIndex()) {
                        oldPoint.set(pageViewPt);
                        mFormFiller.onMouseMove(page, AppUtil.toFxPointF(pdfPointF), 0);
                        return true;
                    }
                    return false;
                case MotionEvent.ACTION_UP:
                case MotionEvent.ACTION_CANCEL:

                    if (pageIndex == annot.getPage().getIndex() && (isHitAnnot(annot, pdfPointF) || isDown)) {
                        isDown = false;

                        mFormFiller.onLButtonUp(page, AppUtil.toFxPointF(pdfPointF), 0);
                        return true;
                    }
                    return false;

            }
        } catch (PDFException e) {
            e.printStackTrace();
        }
        return false;

    }

    private double getDistanceOfPoints(PointF p1, PointF p2) {
        return Math.sqrt(Math.abs((p1.x - p2.x)
                * (p1.x - p2.x) + (p1.y - p2.y)
                * (p1.y - p2.y)));
    }

    private class Blink extends Handler implements Runnable {
        private Annot mAnnot;

        public Blink(Annot annot) {
            mAnnot = annot;
        }

        public void setAnnot(Annot annot) {
            mAnnot = annot;
        }

        @Override
        public void run() {
            try {
                int pageIndex = mAnnot.getPage().getIndex();
                if (!mPdfViewCtrl.isPageVisible(pageIndex)) return;
                RectF rect = AppUtil.toRectF(mAnnot.getRect());
                mPdfViewCtrl.convertPdfRectToPageViewRect(rect, rect, pageIndex);
                mPdfViewCtrl.refresh(pageIndex, AppDmUtil.rectFToRect(rect));
            } catch (PDFException e) {
                e.printStackTrace();
            }

            if (shouldShowInputSoft(mAnnot)){
                int keybordHeight = AppKeyboardUtil.getKeyboardHeight(mParent);
                if (0 < keybordHeight && keybordHeight < AppDisplay.getInstance(mContext).getRawScreenHeight() / 5){
                    mFNModule.setPadding(0, 0, 0, 0);
                }
            } else {
                mFNModule.setPadding(0, 0, 0, 0);
            }

            postDelayed(Blink.this, 500);
        }

    }

    @Override
    public boolean onLongPress(int pageIndex, MotionEvent motionEvent, Annot annot) {
        if (!hasInitialized()) return false;
        if (!((UIExtensionsManager)mPdfViewCtrl.getUIExtensionsManager()).getDocumentManager().canFillForm()) return false;
        if (FormFillerUtil.isReadOnly(annot)) return false;
        return true;
    }

    @Override
    public boolean onSingleTapConfirmed(int pageIndex, MotionEvent motionEvent, Annot annot) {
        if (!hasInitialized()) return false;
        mLastTouchPoint.set(0, 0);
        boolean ret = false;
        if (!((UIExtensionsManager)mPdfViewCtrl.getUIExtensionsManager()).getDocumentManager().canFillForm()) return false;
        if (FormFillerUtil.isReadOnly(annot))
            return false;
        try {
            PointF docViewerPt = new PointF(motionEvent.getX(), motionEvent.getY());

            PointF point = new PointF();
            mPdfViewCtrl.convertDisplayViewPtToPageViewPt(docViewerPt, point, pageIndex);
            PointF pageViewPt = new PointF(point.x, point.y);
            final PointF pdfPointF = new PointF();
            mPdfViewCtrl.convertPageViewPtToPdfPt(pageViewPt, pdfPointF, pageIndex);
            PDFPage page = mPdfViewCtrl.getDoc().getPage(pageIndex);

            Annot annotTmp = AppAnnotUtil.createAnnot(page.getAnnotAtPoint(AppUtil.toFxPointF(pdfPointF), 1));

            boolean isHit = isHitAnnot(annot, pdfPointF);

            if (annot == ((UIExtensionsManager)mPdfViewCtrl.getUIExtensionsManager()).getDocumentManager().getCurrentAnnot()) {
                if (pageIndex == annot.getPage().getIndex() && isHit) {
                    ret = true;
                } else {
                    if (shouldShowInputSoft(annot)) {
                        if (mBlink != null)
                            mBlink.removeCallbacks((Runnable) mBlink);
                        mBlink = null;
                        AppUtil.dismissInputSoft(mEditView);
                        mParent.removeView(mEditView);
                    }
                    if (shouldShowNavigation(annot)) {
                        if (mFNModule != null) {
                            mFNModule.hide();
                            mFNModule.setPadding(0, 0, 0, 0);
                        }
                        resetDocViewerOffset();
                    }
                    ((UIExtensionsManager)mPdfViewCtrl.getUIExtensionsManager()).getDocumentManager().setCurrentAnnot(null);
                    ret = false;
                }
            } else {
                ((UIExtensionsManager)mPdfViewCtrl.getUIExtensionsManager()).getDocumentManager().setCurrentAnnot(annot);
                ret = true;
            }

            final PDFPage finalPage = page;
            if (annotTmp == null || (annot == ((UIExtensionsManager)mPdfViewCtrl.getUIExtensionsManager()).getDocumentManager().getCurrentAnnot()
                    && pageIndex == annot.getPage().getIndex() && isHit)) {
                mFormFiller.onLButtonDown(finalPage, AppUtil.toFxPointF(pdfPointF), 0);
                mFormFiller.onLButtonUp(finalPage, AppUtil.toFxPointF(pdfPointF), 0);
            }
        } catch (PDFException e) {
            e.printStackTrace();
        }
        return ret;
    }

    @Override
    public boolean shouldViewCtrlDraw(Annot annot) {
        return true;
    }

    private static PointF getPageViewOrigin(PDFViewCtrl pdfViewCtrl, int pageIndex, float x, float y) {
        PointF pagePt = new PointF(x, y);
        pdfViewCtrl.convertPageViewPtToDisplayViewPt(pagePt, pagePt, pageIndex);
        RectF rect = new RectF(0, 0, pagePt.x, pagePt.y);
        pdfViewCtrl.convertDisplayViewRectToPageViewRect(rect, rect, pageIndex);
        PointF originPt = new PointF(x - rect.width(), y - rect.height());
        return originPt;
    }

    @Override
    public void onDraw(int pageIndex, Canvas canvas) {
        Annot annot = ((UIExtensionsManager)mPdfViewCtrl.getUIExtensionsManager()).getDocumentManager().getCurrentAnnot();
        if (annot == null || !(annot instanceof Widget))
            return;
        if (ToolUtil.getCurrentAnnotHandler((UIExtensionsManager) mPdfViewCtrl.getUIExtensionsManager()) != this)
            return;
        try {
            int index = annot.getPage().getIndex();
            if (index != pageIndex) return;
            RectF rect = AppUtil.toRectF(annot.getRect());

            PointF viewpoint = new PointF(rect.left, rect.bottom);
            PointF point = new PointF(rect.left, rect.bottom);
            mPdfViewCtrl.convertPdfPtToPageViewPt(viewpoint, viewpoint, pageIndex);
            mPdfViewCtrl.convertPdfPtToPageViewPt(point, point, pageIndex);
            mPdfViewCtrl.convertPageViewPtToDisplayViewPt(viewpoint, viewpoint, pageIndex);
            int type = FormFillerUtil.getAnnotFieldType(mForm, annot);

            if ((type == Field.e_TypeTextField) ||
                    (type == Field.e_TypeComboBox && (((Widget) annot).getField().getFlags() & Field.e_FlagComboEdit) != 0)) {
                int keyboardHeight = AppKeyboardUtil.getKeyboardHeight(mParent);
                if (mAdjustPosition && keyboardHeight > AppDisplay.getInstance(mContext).getRawScreenHeight() / 5) {
                    if (AppDisplay.getInstance(mContext).getRawScreenHeight() - viewpoint.y < (keyboardHeight + AppDisplay.getInstance(mContext).dp2px(116))) {
                        int rawScreenHeight = AppDisplay.getInstance(mContext).getRawScreenHeight();
                        mPageOffset = (int) (keyboardHeight - (rawScreenHeight - viewpoint.y));

                        if (mPageOffset != 0 && pageIndex == mPdfViewCtrl.getPageCount() - 1
                                || (mPdfViewCtrl.getPageLayoutMode() == PDFViewCtrl.PAGELAYOUTMODE_SINGLE ||
                                mPdfViewCtrl.getPageLayoutMode() == PDFViewCtrl.PAGELAYOUTMODE_FACING ||
                                mPdfViewCtrl.getPageLayoutMode() == PDFViewCtrl.PAGELAYOUTMODE_COVER)) {

                            PointF point1 = new PointF(0, mPdfViewCtrl.getPageViewHeight(pageIndex));
                            mPdfViewCtrl.convertPageViewPtToDisplayViewPt(point1, point1, pageIndex);
                            float screenHeight = AppDisplay.getInstance(mContext).getScreenHeight();
                            if (point1.y <= screenHeight) {
                                int offset = mPageOffset + AppDisplay.getInstance(mContext).dp2px(116);
//                                mOffset = 0;
                                setBottomOffset(offset);
                            }
                        }

                        PointF oriPoint = getPageViewOrigin(mPdfViewCtrl, pageIndex, point.x, point.y);
                        mPdfViewCtrl.gotoPage(pageIndex,
                                oriPoint.x, oriPoint.y + mPageOffset + AppDisplay.getInstance(mContext).dp2px(116));
                        mAdjustPosition = false;
                    } else {
                        resetDocViewerOffset();
                    }
                }
            }

            if ((pageIndex != mPdfViewCtrl.getPageCount() - 1 && mPdfViewCtrl.getPageLayoutMode() != PDFViewCtrl.PAGELAYOUTMODE_SINGLE &&
                    mPdfViewCtrl.getPageLayoutMode() != PDFViewCtrl.PAGELAYOUTMODE_FACING &&
                    mPdfViewCtrl.getPageLayoutMode() != PDFViewCtrl.PAGELAYOUTMODE_COVER)) {
                resetDocViewerOffset();
            }
            if (AppKeyboardUtil.getKeyboardHeight(mParent) < AppDisplay.getInstance(mContext).getRawScreenHeight() / 5
                    && (pageIndex == mPdfViewCtrl.getPageCount() - 1 || mPdfViewCtrl.getPageLayoutMode() == PDFViewCtrl.PAGELAYOUTMODE_SINGLE ||
                    mPdfViewCtrl.getPageLayoutMode() == PDFViewCtrl.PAGELAYOUTMODE_FACING ||
                    mPdfViewCtrl.getPageLayoutMode() == PDFViewCtrl.PAGELAYOUTMODE_COVER)) {
                resetDocViewerOffset();
            }

            int fieldType = FormFillerUtil.getAnnotFieldType(mForm, annot);
            if (mFNModule != null) {
                if (fieldType != Field.e_TypePushButton) {
                    if ((fieldType == Field.e_TypeTextField ||
                            (fieldType == Field.e_TypeComboBox && (((Widget) annot).getField().getFlags() & Field.e_FlagComboEdit) != 0))) {
                        int paddingBottom = 0;

                        paddingBottom = AppKeyboardUtil.getKeyboardHeight(mParent);
                        if (Build.VERSION.SDK_INT < 14 && paddingBottom < AppDisplay.getInstance(mContext).getRawScreenHeight() / 5) {
                            paddingBottom = 0;
                        }
                        mFNModule.setPadding(0, 0, 0, paddingBottom);

                    } else {
                        mFNModule.setPadding(0, 0, 0, 0);
                    }
                }
                if (((UIExtensionsManager)mPdfViewCtrl.getUIExtensionsManager()).getDocumentManager().getCurrentAnnot() == null) {
                    mFNModule.hide();
                }
            }
            canvas.save();
            canvas.setDrawFilter(new PaintFlagsDrawFilter(0, Paint.ANTI_ALIAS_FLAG | Paint.FILTER_BITMAP_FLAG));
            if (index == pageIndex && fieldType != Field.e_TypePushButton) {
                RectF bbox = AppUtil.toRectF(annot.getRect());
                mPdfViewCtrl.convertPdfRectToPageViewRect(bbox, bbox, pageIndex);
                bbox.sort();
                bbox.inset(-5, -5);

                canvas.drawLine(bbox.left, bbox.top, bbox.left, bbox.bottom, mPathPaint);
                canvas.drawLine(bbox.left, bbox.bottom, bbox.right, bbox.bottom, mPathPaint);
                canvas.drawLine(bbox.right, bbox.bottom, bbox.right, bbox.top, mPathPaint);
                canvas.drawLine(bbox.left, bbox.top, bbox.right, bbox.top, mPathPaint);
            }
            canvas.restore();
        } catch (PDFException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void addAnnot(int pageIndex, AnnotContent contentSupplier, boolean addUndo,
                         Event.Callback result) {
    }

    @Override
    public void modifyAnnot(Annot annot, AnnotContent content, boolean addUndo, Event.Callback result) {
    }

    @Override
    public void removeAnnot(Annot annot, boolean addUndo, Event.Callback result) {

    }

    protected boolean shouldShowInputSoft(Annot annot) {
        if (annot == null) return false;
        if (!(annot instanceof Widget)) return false;
        int type = FormFillerUtil.getAnnotFieldType(mForm, annot);
        try {
            if ((type == Field.e_TypeTextField) ||
                    (type == Field.e_TypeComboBox && (((Widget) annot).getField().getFlags() & Field.e_FlagComboEdit) != 0))
                return true;

        } catch (PDFException e) {
            e.printStackTrace();
        }
        return false;
    }

    public void resetDocViewerOffset() {
        if (mPageOffset != 0) {
            mPageOffset = 0;
            setBottomOffset(0);
        }
    }

    private void setBottomOffset(int offset) {
        if (mOffset == -offset)
            return;
        mOffset = -offset;
        mPdfViewCtrl.layout(0, 0 + mOffset, mPdfViewCtrl.getWidth(), mPdfViewCtrl.getHeight() + mOffset);
    }

    protected boolean onKeyBack() {
        Annot curAnnot = ((UIExtensionsManager)mPdfViewCtrl.getUIExtensionsManager()).getDocumentManager().getCurrentAnnot();
        try {
            if (curAnnot == null) return false;
            if (curAnnot.getType() != Annot.e_Widget) return false;
            Field field = ((Widget) curAnnot).getField();
            if (field == null || field.isEmpty()) return false;
            int type = field.getType();
            if (type != Field.e_TypeSignature && type != Field.e_TypeUnknown) {
                ((UIExtensionsManager)mPdfViewCtrl.getUIExtensionsManager()).getDocumentManager().setCurrentAnnot(null);
                navigationDismiss();
                return true;
            }
        } catch (PDFException e) {
            e.printStackTrace();
        }

        return false;
    }

    protected void onConfigurationChanged(Configuration newConfig){
        mAdjustPosition = true;
    }
}
