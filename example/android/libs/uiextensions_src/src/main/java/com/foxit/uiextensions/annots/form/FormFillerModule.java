/**
 * Copyright (C) 2003-2018, Foxit Software Inc..
 * All Rights Reserved.
 * <p>
 * http://www.foxitsoftware.com
 * <p>
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */
package com.foxit.uiextensions.annots.form;


import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.view.ScaleGestureDetector;
import android.view.ViewGroup;

import com.foxit.sdk.PDFException;
import com.foxit.sdk.PDFViewCtrl;
import com.foxit.sdk.Task;
import com.foxit.sdk.common.Constants;
import com.foxit.sdk.pdf.PDFDoc;
import com.foxit.sdk.pdf.annots.Annot;
import com.foxit.sdk.pdf.interform.Form;
import com.foxit.uiextensions.Module;
import com.foxit.uiextensions.ToolHandler;
import com.foxit.uiextensions.UIExtensionsManager;
import com.foxit.uiextensions.annots.AnnotHandler;
import com.foxit.uiextensions.controls.propertybar.PropertyBar;
import com.foxit.uiextensions.pdfreader.ILifecycleEventListener;
import com.foxit.uiextensions.pdfreader.impl.LifecycleEventListener;
import com.foxit.uiextensions.utils.Event;
import com.foxit.uiextensions.utils.OnPageEventListener;
import com.foxit.uiextensions.utils.ToolUtil;
import com.foxit.uiextensions.utils.thread.AppThreadManager;


public class FormFillerModule implements Module, PropertyBar.PropertyChangeListener {

    private static final int RESET_FORM_FIELDS = 111;
    private static final int IMPORT_FORM_DATA = 222;
    private static final int EXPORT_FORM_DATA = 333;

    private FormFillerToolHandler mToolHandler;
    private FormFillerAnnotHandler mAnnotHandler;
    private Context mContext;
    private PDFViewCtrl mPdfViewCtrl;
    private ViewGroup mParent;
    private Form mForm = null;
    private PDFViewCtrl.UIExtensionsManager mUiExtensionsManager;

    private PDFViewCtrl.IDocEventListener mDocumentEventListener = new PDFViewCtrl.IDocEventListener() {
        @Override
        public void onDocWillOpen() {

        }

        @Override
        public void onDocOpened(PDFDoc document, int errCode) {
            if (errCode != Constants.e_ErrSuccess) {
                return;
            }
            try {
                if (document != null && !mPdfViewCtrl.isDynamicXFA()) {
                    boolean hasForm = document.hasForm();
                    if (!hasForm)
                        return;
                    mForm = new Form(document);
                    mAnnotHandler.init(mForm);
                }
            } catch (PDFException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onDocWillClose(PDFDoc document) {
            mAnnotHandler.clear();
        }

        @Override
        public void onDocClosed(PDFDoc document, int errCode) {
        }

        @Override
        public void onDocWillSave(PDFDoc document) {

        }

        @Override
        public void onDocSaved(PDFDoc document, int errCode) {

        }
    };

    private PDFViewCtrl.IPageEventListener mPageEventListener = new OnPageEventListener() {
        @Override
        public void onPagesInserted(boolean success, int dstIndex, int[] range) {
            if (!success || mAnnotHandler.hasInitialized())
                return;
            try {
                if (mPdfViewCtrl.getDoc() != null && !mPdfViewCtrl.isDynamicXFA()) {
                    boolean hasForm = mPdfViewCtrl.getDoc().hasForm();
                    if (!hasForm)
                        return;
                    mForm = new Form(mPdfViewCtrl.getDoc());
                    mAnnotHandler.init(mForm);
                }
            } catch (PDFException e) {
                e.printStackTrace();
            }
        }
    };

    private PDFViewCtrl.IScaleGestureEventListener mScaleGestureEventListener = new PDFViewCtrl.IScaleGestureEventListener() {

        @Override
        public boolean onScale(ScaleGestureDetector detector) {
            return false;
        }

        @Override
        public boolean onScaleBegin(ScaleGestureDetector detector) {
            if (mAnnotHandler.getFormFillerAssist() != null) {
                mAnnotHandler.getFormFillerAssist().setScaling(true);
            }

            return false;
        }

        @Override
        public void onScaleEnd(ScaleGestureDetector detector) {
            if (mAnnotHandler.getFormFillerAssist() != null) {
                mAnnotHandler.getFormFillerAssist().setScaling(false);
            }
        }
    };


    public FormFillerModule(Context context, ViewGroup parent, PDFViewCtrl pdfViewCtrl, PDFViewCtrl.UIExtensionsManager uiExtensionsManager) {
        mContext = context;
        mParent = parent;
        mPdfViewCtrl = pdfViewCtrl;
        mUiExtensionsManager = uiExtensionsManager;
    }

    @Override
    public String getName() {
        return Module.MODULE_NAME_FORMFILLER;
    }

    public ToolHandler getToolHandler() {
        return mToolHandler;
    }

    public AnnotHandler getAnnotHandler() {
        return mAnnotHandler;
    }

    public void resetForm(Event.Callback callback) {
        EditFormTask editFormTask = new EditFormTask(RESET_FORM_FIELDS, callback);
        mPdfViewCtrl.addTask(editFormTask);
    }

    public void exportFormToXML(String path, Event.Callback callback) {
        EditFormTask editFormTask = new EditFormTask(EXPORT_FORM_DATA, path, callback);
        mPdfViewCtrl.addTask(editFormTask);
    }

    public void importFormFromXML(final String path, Event.Callback callback) {
        EditFormTask editFormTask = new EditFormTask(IMPORT_FORM_DATA, path, callback);
        mPdfViewCtrl.addTask(editFormTask);
    }

    class EditFormTask extends Task {

        private boolean ret;
        private int mType;
        private String mPath;

        private EditFormTask(int type, String path, final Event.Callback callBack) {
            super(new CallBack() {
                @Override
                public void result(Task task) {
                    callBack.result(null, ((EditFormTask) task).ret);
                }
            });
            this.mPath = path;
            mType = type;
        }

        private EditFormTask(int type, final Event.Callback callBack) {
            super(new CallBack() {
                @Override
                public void result(Task task) {
                    callBack.result(null, ((EditFormTask) task).ret);
                }
            });
            mType = type;
        }

        @Override
        protected void execute() {
            switch (mType) {
                case RESET_FORM_FIELDS:
                    try {
                        PDFViewCtrl.lock();
                        ret = mForm.reset();
                    } catch (PDFException e) {
                        e.printStackTrace();
                        ret = false;
                    } finally {
                        PDFViewCtrl.unlock();
                    }
                    break;
                case IMPORT_FORM_DATA:
                    try {
                        PDFViewCtrl.lock();
                        ret = mForm.importFromXML(mPath);
                    } catch (PDFException e) {
                        ret = false;
                        e.printStackTrace();
                    } finally {
                        PDFViewCtrl.unlock();
                    }
                    break;
                case EXPORT_FORM_DATA:
                    try {
                        PDFViewCtrl.lock();
                        ret = mForm.exportToXML(mPath);
                    } catch (PDFException e) {
                        ret = false;
                        e.printStackTrace();
                    } finally {
                        PDFViewCtrl.unlock();
                    }
                    break;
                default:
                    break;
            }
        }
    }

    @Override
    public boolean loadModule() {
        mToolHandler = new FormFillerToolHandler(mContext, mPdfViewCtrl);
        mAnnotHandler = new FormFillerAnnotHandler(mContext, mParent, mPdfViewCtrl);

        if (mUiExtensionsManager != null && mUiExtensionsManager instanceof UIExtensionsManager) {
            ((UIExtensionsManager) mUiExtensionsManager).registerToolHandler(mToolHandler);
            ToolUtil.registerAnnotHandler((UIExtensionsManager) mUiExtensionsManager, mAnnotHandler);
            ((UIExtensionsManager) mUiExtensionsManager).registerConfigurationChangedListener(mConfigureChangeListener);
            ((UIExtensionsManager) mUiExtensionsManager).registerModule(this);
            ((UIExtensionsManager) mUiExtensionsManager).registerLifecycleListener(mLifecycleEventListener);
        }
        mPdfViewCtrl.registerDocEventListener(mDocumentEventListener);
        mPdfViewCtrl.registerPageEventListener(mPageEventListener);
        mPdfViewCtrl.registerScaleGestureEventListener(mScaleGestureEventListener);
        mPdfViewCtrl.registerRecoveryEventListener(memoryEventListener);
        return true;
    }

    @Override
    public boolean unloadModule() {
        if (mUiExtensionsManager != null && mUiExtensionsManager instanceof UIExtensionsManager) {
            ((UIExtensionsManager) mUiExtensionsManager).unregisterToolHandler(mToolHandler);
            ((UIExtensionsManager) mUiExtensionsManager).unregisterConfigurationChangedListener(mConfigureChangeListener);
            ToolUtil.unregisterAnnotHandler((UIExtensionsManager) mUiExtensionsManager, mAnnotHandler);
            ((UIExtensionsManager) mUiExtensionsManager).unregisterLifecycleListener(mLifecycleEventListener);
        }
        mPdfViewCtrl.unregisterDocEventListener(mDocumentEventListener);
        mPdfViewCtrl.unregisterPageEventListener(mPageEventListener);
        mPdfViewCtrl.unregisterScaleGestureEventListener(mScaleGestureEventListener);
        mPdfViewCtrl.unregisterRecoveryEventListener(memoryEventListener);
        return true;
    }

    PDFViewCtrl.IRecoveryEventListener memoryEventListener = new PDFViewCtrl.IRecoveryEventListener() {
        @Override
        public void onWillRecover() {
            mAnnotHandler.clear();
            ((UIExtensionsManager) mPdfViewCtrl.getUIExtensionsManager()).getDocumentManager().reInit();
        }

        @Override
        public void onRecovered() {
            mToolHandler.initActionHandler();
        }
    };

    @Override
    public void onValueChanged(long property, int value) {

    }

    @Override
    public void onValueChanged(long property, float value) {

    }

    @Override
    public void onValueChanged(long property, String value) {

    }

    public boolean onKeyBack() {
        return mAnnotHandler.onKeyBack();
    }

    private ILifecycleEventListener mLifecycleEventListener = new LifecycleEventListener() {

        @Override
        public void onResume(Activity act) {
            UIExtensionsManager uiExtensionsManager = ((UIExtensionsManager) mPdfViewCtrl.getUIExtensionsManager());
            AnnotHandler currentAnnotHandler = ToolUtil.getCurrentAnnotHandler(uiExtensionsManager);
            Annot curAnnot = uiExtensionsManager.getDocumentManager().getCurrentAnnot();
            if (curAnnot != null && currentAnnotHandler == mAnnotHandler) {
                if (mAnnotHandler.shouldShowInputSoft(curAnnot)) {

                    AppThreadManager.getInstance().getMainThreadHandler().post(new Runnable() {
                        @Override
                        public void run() {
                            mAnnotHandler.showSoftInput();
                        }
                    });
                }
            }
        }
    };


    private UIExtensionsManager.ConfigurationChangedListener mConfigureChangeListener = new UIExtensionsManager.ConfigurationChangedListener() {
        @Override
        public void onConfigurationChanged(Configuration newConfig) {
            mAnnotHandler.onConfigurationChanged(newConfig);
        }
    };


}
