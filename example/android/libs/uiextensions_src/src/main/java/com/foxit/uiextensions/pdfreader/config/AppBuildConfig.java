/**
 * Copyright (C) 2003-2018, Foxit Software Inc..
 * All Rights Reserved.
 * <p>
 * http://www.foxitsoftware.com
 * <p>
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */
package com.foxit.uiextensions.pdfreader.config;

import android.os.Build;

import com.foxit.uiextensions.BuildConfig;
import com.foxit.uiextensions.utils.AppUtil;
import com.foxit.uiextensions.utils.IResult;


public class AppBuildConfig {
    public static final boolean DEBUG = BuildConfig.DEBUG;

//    public static final boolean VERSION_BUSINESS = BuildConfig.BUSINESS;
//    public static final int VERSION = BuildConfig.VERSION_NUMBER;
//    public static final String VERSION_DISPLAY = BuildConfig.VERSION_NAME;
    public static final int SDK_VERSION = Build.VERSION.SDK_INT;

//    public static final boolean VERSION_PURCHASE_GOOGLE = BuildConfig.PURCHASE.equals("play_store");
//    public static final boolean VERSION_PURCHASE_AMAZON = BuildConfig.PURCHASE.equals("amazon");

//    public static final boolean VERSION_INTEL = BuildConfig.LICENSE.equals("intel");
//    public static final boolean VERSION_TRIAL = BuildConfig.LICENSE.equals("trial");
//    public static final boolean VERSION_LICENSE = BuildConfig.LICENSE.equals("foxit");

//    public static final boolean VERSION_RMS_SELFWATERMARK = !VERSION_BUSINESS;
    public static final boolean VERSION_RMS_PPDF = true;
    public static final boolean VERSION_RMS_WRAPPER_V2 = true;

//    public static final boolean VERSION_STATISTICS = !VERSION_BUSINESS;
//    public static final boolean VERSION_INTUNE = VERSION_BUSINESS;
//    public static final boolean VERSION_OFD = BuildConfig.OFD;

//    public static final boolean VERSION_FULLSCREEN_AD = BuildConfig.BAIDUAD || BuildConfig.TAOBAO;//if true show the splash baidu ad
    //    public static final boolean VERSION_READING_AD = !VERSION_BUSINESS;
    public static final boolean VERSION_READING_AD = false;//if true show the google ad

    public static final boolean VERSION_FORM = true;
    public static final boolean VERSION_FOXIT_CLOUD = true;
    public static final boolean VERSION_RDK = false;
    public static final int VERSION_FEATURE = 1000000;
    public static boolean AUTO_SAVE_DOCUMENT = true;

    public static boolean VERSION_CPDF = true;
    public static String JS_API_VERSION = "0.7";
    public static String WEBTOOL_CONVERT_SERVER_ADDR = "http://cwebtools.connectedpdf.com";

    //private static String CTPAPI_CONFIG_SERVER_ADDR = BuildConfig.CTPAPI_END_POINT_SERVER_ADDR;
    private static String CTPAPI_END_POINT_SERVER_ADDR = "https://www-fz02.connectedpdf.com";
//    private static String CTPAPI_CLOUD_SERVER_ADDR = BuildConfig.CTPAPI_CLOUD_SERVER_ADDR;
    //private static String CTPAPI_CLOUD_CMIS_ADDR = BuildConfig.CTPAPI_SERVER_CMIS;

    static String SP_NAME_END_POINT = "sp_name_cpdf_end_point";
    static String SP_KEY_END_POINT = "sp_key_cpdf_end_point";
    static String SP_KEY_LAST_END_POINT = "sp_key_cpdf_last_end_point";
    static String SP_KEY_CLOUD_SERVER = "sp_key_cpdf_cloud_server";
    static String SP_KEY_DEBUG_END_POINT = "sp_key_cpdf_debug_end_point";

    static String getCwsEndPointServerAddr() {
//        if (AppBuildConfig.DEBUG) {
//            String debugServer = App.instance().getSP().getString(SP_NAME_END_POINT, SP_KEY_DEBUG_END_POINT, CTPAPI_END_POINT_SERVER_ADDR);
//            return debugServer;
//        }
        return CTPAPI_END_POINT_SERVER_ADDR;
    }

    public static boolean isEntEndPoint(boolean forSubscripe) {
        if (forSubscripe) {
            return !AppUtil.isEqual(getEndPointServerAddr(), CTPAPI_END_POINT_SERVER_ADDR);
        } else {
            return !AppUtil.isEqual(getEndPointServerAddr(), getCwsEndPointServerAddr());
        }
    }

    public static boolean isEntEndPoint() {
        return !AppUtil.isEqual(getEndPointServerAddr(), getCwsEndPointServerAddr());
    }

    public static boolean restoreOriginalEndPointServerAddr(IResult<Void, Void, Void> result) {
        String endPoint = getEndPointServerAddr();
        if (AppUtil.isEqual(endPoint, getCwsEndPointServerAddr())) {
            return false;
        }
        setEndPointServerAddr(getCwsEndPointServerAddr(), result);

        return true;
    }

    public static String getEndPointServerAddr() {
//        String server = App.instance().getSP().getString(
//                SP_NAME_END_POINT, SP_KEY_END_POINT, getCwsEndPointServerAddr());
//        if (AppUtil.isEmpty(server)) {
            return getCwsEndPointServerAddr();
//        }
//        return server;
    }

    public static void setEndPointServerAddr(final String server, final IResult<Void, Void, Void> result) {
        final String oldEndPoint = getEndPointServerAddr();
        if (AppUtil.isEqual(server, oldEndPoint)) {
            if (result != null) {
                result.onResult(true, null, null, null);
            }
            return;
        }
//        App.instance().getSP().setString(SP_NAME_END_POINT, SP_KEY_END_POINT, server);
//        App.instance().getSP().setString(CTP_Constants.getSPModuleName(), CTP_Constants.SharePreferenceAppID, null);
//
//        CTP_ServiceListFactory.instance().setupServiceList(server, new AppTaskResult<Void, Void, Void>() {
//            @Override
//            public void onResult(boolean success, Void p1, Void p2, Void p3) {
//                CTP_ToolHandler.checkAppID(new CTP_ToolHandler.IRequestAppIdCallback() {
//                    @Override
//                    public void saveAppIdOver() {
//                        if (result != null) {
//                            result.onResult(true, null, null, null);
//                        }
//
//                        App.instance().getThreadManager().runOnUiThread(new Runnable() {
//                            @Override
//                            public void run() {
//                                App.instance().getEventManager().onCpdfEndPointChanged(oldEndPoint, server);
//                            }
//                        });
//                    }
//                });
//            }
//        });
    }

//    public static void setDebugEndPointServerAddr(final String server, final IAppTaskResult<Void, Void, Void> result) {
//        if (AppBuildConfig.DEBUG) {
//            setEndPointServerAddr(server, result);
//            App.instance().getSP().setString(SP_NAME_END_POINT, SP_KEY_DEBUG_END_POINT, server);
//        }
//    }

    public static String getLastEndPointServerAddr() {
//        String server = App.instance().getSP().getString(
//                SP_NAME_END_POINT, SP_KEY_LAST_END_POINT, "");
//        return server;
        return getCwsEndPointServerAddr();
    }

    public static void setLastEndPointServerAddr(final String server) {
        if (AppUtil.isEqual(server, getCwsEndPointServerAddr())) {
            return;
        }
//        App.instance().getSP().setString(SP_NAME_END_POINT, SP_KEY_LAST_END_POINT, server);
    }

//    public static String getCloudServerAddr() {
//        String server = App.instance().getSP().getString(
//                SP_NAME_END_POINT, SP_KEY_CLOUD_SERVER, CTPAPI_CLOUD_SERVER_ADDR);
//        return server;
//    }
//
//    public static void setCloudServerAddr(final String server) {
//        final String oldEndPoint = getCloudServerAddr();
//        if (AppUtil.isEqual(server, oldEndPoint)) {
//            return;
//        }
//        App.instance().getSP().setString(SP_NAME_END_POINT, SP_KEY_CLOUD_SERVER, server);
//    }
}
