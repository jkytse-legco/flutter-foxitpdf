/**
 * Copyright (C) 2003-2018, Foxit Software Inc..
 * All Rights Reserved.
 * <p>
 * http://www.foxitsoftware.com
 * <p>
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */
package com.foxit.uiextensions.controls.propertybar;

import android.graphics.RectF;
import android.view.View;

public interface MoreTools {
    interface IMT_MoreClickListener {
        void onMTClick(int type);

        int getType();
    }

    interface IMT_DismissListener {
        void onMTDismiss();
    }

    int MT_TYPE_HIGHLIGHT = 1;
    int MT_TYPE_ANNOTTEXT = 2;
    int MT_TYPE_STRIKEOUT = 3;
    int MT_TYPE_SQUIGGLY = 4;
    int MT_TYPE_UNDERLINE = 5;
    int MT_TYPE_CIRCLE = 6;
    int MT_TYPE_SQUARE = 7;
    int MT_TYPE_TYPEWRITER = 8;
    int MT_TYPE_STAMP = 9;
    int MT_TYPE_INSERTTEXT = 10;
    int MT_TYPE_REPLACE = 11;
    int MT_TYPE_ERASER = 12;
    int MT_TYPE_INK = 13;
    int MT_TYPE_LINE = 14;
    int MT_TYPE_ARROW = 15;
    int MT_TYPE_FILEATTACHMENT = 16;
    int MT_TYPE_TEXTBOX = 17;
    int MT_TYPE_POLYGON = 18;
    int MT_TYPE_POLYGONCLOUD = 19;
    int MT_TYPE_IMAGE = 20;
    int MT_TYPE_DISTANCE = 21;
    int MT_TYPE_CALLOUT = 22;
    int MT_TYPE_POLYLINE= 23;
    int MT_TYPE_AUDIO= 24;
    int MT_TYPE_VIDEO= 25;

    boolean isShowing();

    void show(RectF rectF, boolean showMask);

    void update(RectF rectF);

    void dismiss();

    View getContentView();

    void setButtonEnable(int buttonType, boolean enable);

    void registerListener(IMT_MoreClickListener listener);

    void unRegisterListener(IMT_MoreClickListener listener);

    void setMTDismissListener(IMT_DismissListener dismissListener);
}
