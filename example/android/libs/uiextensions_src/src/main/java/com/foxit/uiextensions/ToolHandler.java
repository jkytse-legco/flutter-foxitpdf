/**
 * Copyright (C) 2003-2018, Foxit Software Inc..
 * All Rights Reserved.
 * <p>
 * http://www.foxitsoftware.com
 * <p>
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */
package com.foxit.uiextensions;

import android.view.MotionEvent;

import com.foxit.sdk.PDFViewCtrl;

public interface ToolHandler extends PDFViewCtrl.IDrawEventListener {
    // Tool handler type
    String TH_TYPE_TEXTSELECT = "TextSelect Tool";
    String TH_TYPE_BLANKSELECT = "BlankSelect Tool";

    //Annot tool
    String TH_TYPE_HIGHLIGHT = "Highlight Tool";
    String TH_TYPE_UNDERLINE = "Underline Tool";
    String TH_TYPE_STRIKEOUT = "Strikeout Tool";
    String TH_TYPE_SQUIGGLY = "Squiggly Tool";
    String TH_TYPE_NOTE = "Note Tool";
    String TH_TYPE_CIRCLE = "Circle Tool";
    String TH_TYPE_SQUARE = "Square Tool";
    String TH_TYPE_TYPEWRITER = "Typewriter Tool";
    String TH_TYPE_TEXTBOX = "Textbox Tool";
    String TH_TYPR_INSERTTEXT = "InsetText Tool";
    String TH_TYPE_CALLOUT = "Callout Tool";
    String TH_TYPE_REPLACE = "Replace Tool";
    String TH_TYPE_INK = "Ink Tool";
    String TH_TYPE_ERASER = "Eraser Tool";
    String TH_TYPE_STAMP = "Stamp Tool";
    String TH_TYPE_LINE = "Line Tool";
    String TH_TYPE_ARROW = "Arrow Tool";
    String TH_TYPE_DISTANCE = "Distance Tool";
    String TH_TYPE_FORMFILLER = "FormFiller Tool";
    String TH_TYPE_FILEATTACHMENT = "FileAttachment Tool";
    String TH_TYPE_SIGNATURE = "Signature Tool";
    String TH_TYPE_PDFIMAGE= "PDFImage Tool";
    String TH_TYPE_SCREEN_AUDIO= "Audio Tool";
    String TH_TYPE_SCREEN_VIDEO= "Video Tool";
    String TH_TYPE_POLYGON = "polygon Tool";
    String TH_TYPE_POLYGONCLOUD = "polygon cloud Tool";
    String TH_TYPE_POLYLINE = "polyline Tool";

    String getType();

    void onActivate();

    void onDeactivate();

    boolean onTouchEvent(int pageIndex, MotionEvent motionEvent);

    boolean onLongPress(int pageIndex, MotionEvent motionEvent);

    boolean onSingleTapConfirmed(int pageIndex, MotionEvent motionEvent);
}
