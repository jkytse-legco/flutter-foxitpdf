/**
 * Copyright (C) 2003-2018, Foxit Software Inc..
 * All Rights Reserved.
 * <p>
 * http://www.foxitsoftware.com
 * <p>
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */
package com.foxit.uiextensions.modules.panel;

import com.foxit.uiextensions.controls.dialog.UIDialogFragment;
import com.foxit.uiextensions.controls.panel.PanelHost;
import com.foxit.uiextensions.controls.panel.PanelSpec;

public interface IPanelManager {
    interface OnShowPanelListener{
        void onShow();
    }
    PanelHost getPanel();
    UIDialogFragment getPanelWindow();
    void showPanel();
    void updatePanel();
    void showPanel(PanelSpec.PanelType panelType);
    void hidePanel();
//    void onConfigurationChanged(Configuration newConfig);
    void setOnShowPanelListener(OnShowPanelListener listener);
}
