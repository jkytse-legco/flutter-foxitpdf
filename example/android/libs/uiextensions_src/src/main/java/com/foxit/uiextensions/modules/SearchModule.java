/**
 * Copyright (C) 2003-2018, Foxit Software Inc..
 * All Rights Reserved.
 * <p>
 * http://www.foxitsoftware.com
 * <p>
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */
package com.foxit.uiextensions.modules;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.view.View;
import android.view.ViewGroup;

import com.foxit.sdk.PDFViewCtrl;
import com.foxit.sdk.common.Constants;
import com.foxit.sdk.pdf.PDFDoc;
import com.foxit.uiextensions.Module;
import com.foxit.uiextensions.R;
import com.foxit.uiextensions.UIExtensionsManager;
import com.foxit.uiextensions.controls.toolbar.BaseBar;
import com.foxit.uiextensions.controls.toolbar.IBaseItem;
import com.foxit.uiextensions.controls.toolbar.ToolbarItemConfig;
import com.foxit.uiextensions.controls.toolbar.impl.BaseItemImpl;
import com.foxit.uiextensions.pdfreader.config.ReadStateConfig;
import com.foxit.uiextensions.utils.AppUtil;

public class SearchModule implements Module {
    private Context mContext = null;
    private PDFViewCtrl mPdfViewCtrl = null;
    private ViewGroup mParent = null;
    private SearchView mSearchView = null;
    private PDFViewCtrl.UIExtensionsManager mUiExtensionsManager;

    private IBaseItem mSearchButtonItem;

    public SearchModule(Context context, ViewGroup parent, PDFViewCtrl pdfViewCtrl, PDFViewCtrl.UIExtensionsManager uiExtensionsManager) {
        if (context == null || parent == null || pdfViewCtrl == null) {
            throw new NullPointerException();
        }
        mContext = context;
        mParent = parent;
        mPdfViewCtrl = pdfViewCtrl;
        mUiExtensionsManager = uiExtensionsManager;
    }

    @Override
    public boolean loadModule() {
        mSearchView = new SearchView(mContext, mParent, mPdfViewCtrl);
        mPdfViewCtrl.registerDocEventListener(mDocEventListener);
        mPdfViewCtrl.registerDrawEventListener(mDrawEventListener);

        if (mUiExtensionsManager != null && mUiExtensionsManager instanceof UIExtensionsManager) {
            ((UIExtensionsManager) mUiExtensionsManager).registerModule(this);

            initSearchItem();
        }
        return true;
    }


    private void initSearchItem(){
        final UIExtensionsManager uiExtensionsManager = (UIExtensionsManager) mUiExtensionsManager;

        mSearchButtonItem = new BaseItemImpl(mContext);
        mSearchButtonItem.setTag(ToolbarItemConfig.ITEM_TOPBAR_SEARCH_TAG);
        mSearchButtonItem.setImageResource(R.drawable.rd_search_selector);
        mSearchButtonItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (AppUtil.isFastDoubleClick()) {
                    return;
                }
                uiExtensionsManager.triggerDismissMenuEvent();
                uiExtensionsManager.getMainFrame().hideToolbars();
                uiExtensionsManager.changeState(ReadStateConfig.STATE_SEARCH);

                mSearchView.setSearchCancelListener(new SearchView.SearchCancelListener() {
                    @Override
                    public void onSearchCancel() {
                        uiExtensionsManager.changeState(ReadStateConfig.STATE_NORMAL);
                        uiExtensionsManager.getMainFrame().showToolbars();
                    }
                });
                mSearchView.launchSearchView();
                mSearchView.show();
            }
        });
        uiExtensionsManager.getMainFrame().getTopToolbar().addView(mSearchButtonItem, BaseBar.TB_Position.Position_RB);
    }

    @Override
    public String getName() {
        return Module.MODULE_NAME_SEARCH;
    }

    @Override
    public boolean unloadModule() {
        mPdfViewCtrl.unregisterDocEventListener(mDocEventListener);
        mPdfViewCtrl.unregisterDrawEventListener(mDrawEventListener);
        mDocEventListener = null;
        mDrawEventListener = null;
        return true;
    }

    public SearchView getSearchView() {
        return mSearchView;
    }

    private PDFViewCtrl.IDrawEventListener mDrawEventListener = new PDFViewCtrl.IDrawEventListener() {


        @Override
        public void onDraw(int pageIndex, Canvas canvas) {
            if (mSearchView.mIsCancel) {
                return;
            }

            if (mSearchView.mRect == null || mSearchView.mPageIndex == -1) {
                return;
            }

            if (mSearchView.mPageIndex == pageIndex) {
                if (mSearchView.mRect.size() > 0) {
                    Paint paint = new Paint();
                    paint.setARGB(150, 23, 156, 216);
                    for (int i = 0; i < mSearchView.mRect.size(); i++) {
                        RectF rectF = new RectF(mSearchView.mRect.get(i));
                        RectF deviceRect = new RectF();
                        if (mPdfViewCtrl.convertPdfRectToPageViewRect(rectF, deviceRect, mSearchView.mPageIndex)) {
                            canvas.drawRect(deviceRect, paint);
                        }
                    }
                }
            }
        }
    };

    PDFViewCtrl.IDocEventListener mDocEventListener = new PDFViewCtrl.IDocEventListener() {
        @Override
        public void onDocWillOpen() {

        }

        @Override
        public void onDocOpened(PDFDoc pdfDoc, int i) {
            if (i == Constants.e_ErrSuccess && mPdfViewCtrl.isDynamicXFA()) {
                mSearchButtonItem.setImageResource(R.drawable.rd_search_pressed);
                mSearchButtonItem.setEnable(false);
            }
        }

        @Override
        public void onDocWillClose(PDFDoc pdfDoc) {

        }

        @Override
        public void onDocClosed(PDFDoc pdfDoc, int i) {
            mSearchView.onDocumentClosed();
        }

        @Override
        public void onDocWillSave(PDFDoc pdfDoc) {

        }

        @Override
        public void onDocSaved(PDFDoc pdfDoc, int i) {

        }
    };

    public boolean onKeyBack() {
        if (mSearchView.getView().getVisibility() == View.VISIBLE) {
            mSearchView.cancel();
            return true;

        }
        return false;
    }
}
