/**
 * Copyright (C) 2003-2018, Foxit Software Inc..
 * All Rights Reserved.
 * <p>
 * http://www.foxitsoftware.com
 * <p>
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */
package com.foxit.uiextensions.modules.crop;

import android.content.Context;
import android.view.KeyEvent;
import android.view.ViewGroup;

import com.foxit.sdk.PDFViewCtrl;
import com.foxit.sdk.common.Constants;
import com.foxit.sdk.pdf.PDFDoc;
import com.foxit.uiextensions.Module;
import com.foxit.uiextensions.UIExtensionsManager;
import com.foxit.uiextensions.controls.propertybar.IMultiLineBar;

public class CropModule implements Module {
    private Context mContext = null;
    private PDFViewCtrl mPdfViewCtrl = null;
    private ViewGroup mParent = null;
    private IMultiLineBar mSettingBar;
    private boolean mIsCropModule = false;
    private CropView mCropView;
    private PDFViewCtrl.UIExtensionsManager mUiExtensionsManager;

    public CropModule(Context context, ViewGroup parent, PDFViewCtrl pdfViewCtrl, PDFViewCtrl.UIExtensionsManager uiExtensionsManager) {
        mContext = context;
        mParent = parent;
        mPdfViewCtrl = pdfViewCtrl;
        mUiExtensionsManager = uiExtensionsManager;
    }

    @Override
    public String getName() {
        return MODULE_NAME_CROP;
    }

    @Override
    public boolean loadModule() {
        if (mUiExtensionsManager != null && mUiExtensionsManager instanceof UIExtensionsManager) {
            ((UIExtensionsManager) mUiExtensionsManager).registerModule(this);
        }
        mPdfViewCtrl.registerDocEventListener(docEventListener);
        return true;
    }

    @Override
    public boolean unloadModule() {
        mPdfViewCtrl.unregisterDocEventListener(docEventListener);
        return true;
    }

    PDFViewCtrl.IDocEventListener docEventListener = new PDFViewCtrl.IDocEventListener() {
        @Override
        public void onDocWillOpen() {

        }

        @Override
        public void onDocOpened(PDFDoc document, int errCode) {
            if (errCode != Constants.e_ErrSuccess) return;

            if (!initMLBarValue()) return;
            registerMLListener();

            mCropView = new CropView(mContext, mParent, mPdfViewCtrl);
            mCropView.setSettingBar(mSettingBar);
        }

        @Override
        public void onDocWillClose(PDFDoc document) {

        }

        @Override
        public void onDocClosed(PDFDoc document, int errCode) {
            unRegisterMLListener();
        }

        @Override
        public void onDocWillSave(PDFDoc document) {

        }

        @Override
        public void onDocSaved(PDFDoc document, int errCode) {

        }
    };

    private boolean initMLBarValue() {
        mSettingBar = ((UIExtensionsManager)mUiExtensionsManager).getMainFrame().getSettingBar();
        if (mPdfViewCtrl.isDynamicXFA()) {
            mSettingBar.enableBar(IMultiLineBar.TYPE_CROP, false);
            return false;
        }
        mSettingBar.setProperty(IMultiLineBar.TYPE_CROP, mIsCropModule);
        return true;
    }

    private void registerMLListener() {
        mSettingBar.registerListener(mCropChangeListener);
    }

    private void unRegisterMLListener() {
        mSettingBar.unRegisterListener(mCropChangeListener);
    }

    private IMultiLineBar.IML_ValueChangeListener mCropChangeListener = new IMultiLineBar.IML_ValueChangeListener() {

        @Override
        public void onValueChanged(int type, Object value) {
            if (type == IMultiLineBar.TYPE_CROP) {
                mIsCropModule = (boolean) value;
                mCropView.openCropView();
                ((UIExtensionsManager)mUiExtensionsManager).getMainFrame().hideSettingBar();
                if (((UIExtensionsManager)mPdfViewCtrl.getUIExtensionsManager()).getDocumentManager().getCurrentAnnot() != null) {
                    ((UIExtensionsManager)mPdfViewCtrl.getUIExtensionsManager()).getDocumentManager().setCurrentAnnot(null);
                }
            }
        }

        @Override
        public void onDismiss() {

        }

        @Override
        public int getType() {
            return IMultiLineBar.TYPE_CROP;
        }
    };

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (mCropView != null) {
            return mCropView.onKeyDown(keyCode, event);
        }
        return false;
    }

    public boolean onKeyBack() {
        if (mCropView != null) {
            return mCropView.onKeyBack();
        }
        return false;
    }
}
