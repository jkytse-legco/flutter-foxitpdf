/**
 * Copyright (C) 2003-2018, Foxit Software Inc..
 * All Rights Reserved.
 * <p>
 * http://www.foxitsoftware.com
 * <p>
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */
package com.foxit.uiextensions;


import android.widget.RelativeLayout;

import com.foxit.sdk.PDFViewCtrl;
import com.foxit.uiextensions.controls.propertybar.IMultiLineBar;
import com.foxit.uiextensions.controls.toolbar.IBarsHandler;
import com.foxit.uiextensions.pdfreader.ILifecycleEventListener;
import com.foxit.uiextensions.pdfreader.IMainFrame;
import com.foxit.uiextensions.pdfreader.IStateChangeListener;

public interface IPDFReader {
    interface BackEventListener {
        /**
         * Called when the back button clicked.
         *
         * @return Return <code>true</code> to prevent this event from being propagated
         *         further, or <code>false</code> to indicate that you have not handled
         *         this event and it should continue to be propagated by Foxit.
         */
        boolean onBack();
    }

    boolean registerLifecycleListener(ILifecycleEventListener listener);

    boolean unregisterLifecycleListener(ILifecycleEventListener listener);

    boolean registerStateChangeListener(IStateChangeListener listener);

    boolean unregisterStateChangeListener(IStateChangeListener listener);

    int getState();

    void changeState(int state);

    IMainFrame getMainFrame();

    PDFViewCtrl getPDFViewCtrl();

    IBarsHandler getBarManager();

    IMultiLineBar getSettingBar();

    DocumentManager getDocumentManager();

    RelativeLayout getContentView();

    void backToPrevActivity();

    void setBackEventListener(BackEventListener listener);

    BackEventListener getBackEventListener();
}
