/**
 * Copyright (C) 2003-2018, Foxit Software Inc..
 * All Rights Reserved.
 * <p>
 * http://www.foxitsoftware.com
 * <p>
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */
package com.foxit.uiextensions.annots.fileattachment;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.ColorDrawable;
import android.os.Environment;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.foxit.sdk.PDFViewCtrl;
import com.foxit.sdk.common.Constants;
import com.foxit.sdk.pdf.PDFDoc;
import com.foxit.uiextensions.Module;
import com.foxit.uiextensions.R;
import com.foxit.uiextensions.ToolHandler;
import com.foxit.uiextensions.UIExtensionsManager;
import com.foxit.uiextensions.annots.AnnotHandler;
import com.foxit.uiextensions.controls.propertybar.MoreTools;
import com.foxit.uiextensions.controls.propertybar.PropertyBar;
import com.foxit.uiextensions.controls.propertybar.imp.PropertyBarImpl;
import com.foxit.uiextensions.controls.toolbar.BaseBar;
import com.foxit.uiextensions.controls.toolbar.IBaseItem;
import com.foxit.uiextensions.controls.toolbar.PropertyCircleItem;
import com.foxit.uiextensions.controls.toolbar.ToolbarItemConfig;
import com.foxit.uiextensions.controls.toolbar.impl.CircleItemImpl;
import com.foxit.uiextensions.controls.toolbar.impl.PropertyCircleItemImp;
import com.foxit.uiextensions.modules.PageNavigationModule;
import com.foxit.uiextensions.pdfreader.config.ReadStateConfig;
import com.foxit.uiextensions.utils.AppAnnotUtil;
import com.foxit.uiextensions.utils.AppFileUtil;
import com.foxit.uiextensions.utils.AppUtil;
import com.foxit.uiextensions.utils.ToolUtil;

import java.io.File;


public class FileAttachmentModule implements Module, PropertyBar.PropertyChangeListener {

    private Context mContext;
    private PDFViewCtrl mPdfViewCtrl;
    private FileAttachmentAnnotHandler mAnnotHandler;
    private FileAttachmentToolHandler mToolHandler;
    private PDFViewCtrl.UIExtensionsManager mUiExtensionsManager;
    private PropertyBar mPropertyBar;

    @Override
    public String getName() {
        return Module.MODULE_NAME_FILEATTACHMENT;
    }

    public FileAttachmentModule(Context context, PDFViewCtrl pdfViewCtrl, PDFViewCtrl.UIExtensionsManager uiExtensionsManager) {
        mContext = context;
        mPdfViewCtrl = pdfViewCtrl;
        mUiExtensionsManager = uiExtensionsManager;
    }

    public ToolHandler getToolHandler() {
        return mToolHandler;
    }

    public AnnotHandler getAnnotHandler() {
        return mAnnotHandler;
    }

    @Override
    public boolean loadModule() {

        mToolHandler = new FileAttachmentToolHandler(mContext, mPdfViewCtrl);
        mCurrentColor = PropertyBar.PB_COLORS_FILEATTACHMENT[0];
        mCurrentOpacity = 100;
        mFlagType = FileAttachmentConstants.ICONTYPE_PUSHPIN;

        mToolHandler.setColor(mCurrentColor);
        mToolHandler.setOpacity(mCurrentOpacity);
        String[] iconNames = FileAttachmentUtil.getIconNames();
        mToolHandler.setIconName(iconNames[mFlagType]);

        mPropertyBar = new PropertyBarImpl(mContext, mPdfViewCtrl);
        mTypeNames = FileAttachmentUtil.getIconNames();
        mAdapter = new FileAttachmentPBAdapter(mContext, mTypePicIds, mTypeNames);
        mAdapter.setNoteIconType(mFlagType);

        mAnnotHandler = new FileAttachmentAnnotHandler(mContext, mPdfViewCtrl, this);
        mAnnotHandler.setToolHandler(mToolHandler);
        mAnnotHandler.setPropertyListViewAdapter(mAdapter);

        if (mUiExtensionsManager != null && mUiExtensionsManager instanceof UIExtensionsManager) {
            ((UIExtensionsManager) mUiExtensionsManager).registerToolHandler(mToolHandler);
            ToolUtil.registerAnnotHandler((UIExtensionsManager) mUiExtensionsManager, mAnnotHandler);
            ((UIExtensionsManager) mUiExtensionsManager).registerModule(this);
            mAnnotHandler.registerAttachmentDocEventListener(mAttachmentDocEvent);

            ((UIExtensionsManager) mUiExtensionsManager).getMainFrame().getMoreToolsBar().registerListener(new MoreTools.IMT_MoreClickListener() {
                @Override
                public void onMTClick(int type) {
                    if (type == MoreTools.MT_TYPE_FILEATTACHMENT) {
                        ((UIExtensionsManager) mUiExtensionsManager).setCurrentToolHandler(mToolHandler);
                        ((UIExtensionsManager) mUiExtensionsManager).changeState(ReadStateConfig.STATE_ANNOTTOOL);

                        resetAnnotBar();
                        resetPropertyBar();
                    }
                }

                @Override
                public int getType() {
                    return MoreTools.MT_TYPE_FILEATTACHMENT;
                }
            });
        }
        mPdfViewCtrl.registerDocEventListener(mDocEventListener);
        mPdfViewCtrl.registerDrawEventListener(mDrawEventListener);
        return true;
    }

    @Override
    public boolean unloadModule() {
        mPdfViewCtrl.unregisterDocEventListener(mDocEventListener);
        mPdfViewCtrl.unregisterDrawEventListener(mDrawEventListener);

        if (mUiExtensionsManager != null && mUiExtensionsManager instanceof UIExtensionsManager) {
            ((UIExtensionsManager) mUiExtensionsManager).unregisterToolHandler(mToolHandler);
            ToolUtil.unregisterAnnotHandler((UIExtensionsManager) mUiExtensionsManager, mAnnotHandler);

            mAnnotHandler.unregisterAttachmentDocEventListener(mAttachmentDocEvent);
        }
        // delete temp files
        String tempPath = Environment.getExternalStorageDirectory() + "/FoxitSDK/AttaTmp/";
        File tempFile = new File(tempPath);
        if (tempFile.exists()) {
            AppFileUtil.deleteFolder(tempFile, false);
        }
        return true;
    }

    private int mCurrentColor;
    private int mCurrentOpacity;
    private int mFlagType;

    @Override
    public void onValueChanged(long property, int value) {
        UIExtensionsManager uiExtensionsManager = (UIExtensionsManager) mUiExtensionsManager;
        AnnotHandler currentAnnotHandler = ToolUtil.getCurrentAnnotHandler(uiExtensionsManager);
        ToolHandler currentToolHandler = uiExtensionsManager.getCurrentToolHandler();
        if (property == PropertyBar.PROPERTY_COLOR || property == PropertyBar.PROPERTY_SELF_COLOR) {
            if (currentToolHandler == mToolHandler) {
                mCurrentColor = value;
                mToolHandler.setColor(mCurrentColor);
                if (mPropertyItem != null)
                    mPropertyItem.setCentreCircleColor(value);
            } else if (currentAnnotHandler == mAnnotHandler) {
                mAnnotHandler.modifyAnnotColor(value);
            }
        } else if (property == PropertyBar.PROPERTY_OPACITY) {
            if (currentToolHandler == mToolHandler) {
                mCurrentOpacity = value;
                mToolHandler.setOpacity(mCurrentOpacity);
            } else if (currentAnnotHandler == mAnnotHandler) {
                mAnnotHandler.modifyAnnotOpacity(value);
            }
        }
    }

    @Override
    public void onValueChanged(long property, float value) {
    }

    @Override
    public void onValueChanged(long property, String value) {
    }

    private PDFViewCtrl.IDrawEventListener mDrawEventListener = new PDFViewCtrl.IDrawEventListener() {

        @Override
        public void onDraw(int pageIndex, Canvas canvas) {
            mAnnotHandler.onDrawForControls(canvas);
        }
    };

    private PDFViewCtrl.IDocEventListener mDocEventListener = new PDFViewCtrl.IDocEventListener() {
        @Override
        public void onDocWillOpen() {

        }

        @Override
        public void onDocOpened(PDFDoc doc, int err) {
            if (!isAttachmentOpening()) {
                mAnnotHandler.deleteTmpPath();
            }
        }

        @Override
        public void onDocWillClose(PDFDoc doc) {

        }

        @Override
        public void onDocClosed(PDFDoc doc, int err) {

        }

        @Override
        public void onDocWillSave(PDFDoc document) {

        }

        @Override
        public void onDocSaved(PDFDoc document, int errCode) {

        }

    };

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        return (mToolHandler.onKeyDown(keyCode, event) || mAnnotHandler.onKeyDown(keyCode, event));
    }

    public boolean onKeyBack() {
        return (mToolHandler.onKeyBack() || mAnnotHandler.onKeyBack());
    }

    private int[] mTypePicIds = new int[]{R.drawable.pb_fat_type_graph, R.drawable.pb_fat_type_paperclip, R.drawable.pb_fat_type_pushpin,
            R.drawable.pb_fat_type_tag};
    private String[] mTypeNames;
    private FileAttachmentPBAdapter mAdapter;

    private int[] mPBColors = new int[PropertyBar.PB_COLORS_FILEATTACHMENT.length];

    private void resetPropertyBar() {
        final FileAttachmentToolHandler toolHandler = (FileAttachmentToolHandler) getToolHandler();
        long supportProperty = PropertyBar.PROPERTY_COLOR | PropertyBar.PROPERTY_OPACITY;
        System.arraycopy(PropertyBar.PB_COLORS_FILEATTACHMENT, 0, mPBColors, 0, mPBColors.length);
        mPBColors[0] = PropertyBar.PB_COLORS_FILEATTACHMENT[0];

        mPropertyBar.setColors(mPBColors);
        mPropertyBar.setProperty(PropertyBar.PROPERTY_COLOR, toolHandler.getColor());

        int opacity = toolHandler.getOpacity();
        mPropertyBar.setProperty(PropertyBar.PROPERTY_OPACITY, opacity);
        mPropertyBar.reset(supportProperty);

        mPropertyBar.setPropertyChangeListener(this);
        mPropertyBar.addTab("", 0, mContext.getString(com.foxit.uiextensions.R.string.pb_type_tab), 0);
        mPropertyBar.addCustomItem(PropertyBar.PROPERTY_FILEATTACHMENT, getIconTypeView(), 0, 0);
        mAdapter.setNoteIconType(mFlagType);
    }

    protected View getIconTypeView() {
        //IconListView
        LinearLayout iconItem_ly = new LinearLayout(mContext);
        iconItem_ly.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
        iconItem_ly.setGravity(Gravity.CENTER);
        iconItem_ly.setOrientation(LinearLayout.HORIZONTAL);

        ListView listView = new ListView(mContext);
        listView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
        listView.setCacheColorHint(mContext.getResources().getColor(R.color.ux_color_translucent));

        listView.setDivider(new ColorDrawable(mContext.getResources().getColor(R.color.ux_color_seperator_gray)));
        listView.setDividerHeight(1);
        iconItem_ly.addView(listView);
        listView.setAdapter(mAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                UIExtensionsManager uiExtensionsManager = (UIExtensionsManager) mUiExtensionsManager;
                AnnotHandler currentAnnotHandler = ToolUtil.getCurrentAnnotHandler(uiExtensionsManager);
                ToolHandler currentToolHandler = uiExtensionsManager.getCurrentToolHandler();

                if (mToolHandler == currentToolHandler){
                    mFlagType = position;
                    String[] iconNames = FileAttachmentUtil.getIconNames();
                    mToolHandler.setIconName(iconNames[mFlagType]);
                } else if (mAnnotHandler == currentAnnotHandler){
                    mAnnotHandler.modifyIconType(position);
                }

                mAdapter.setNoteIconType(position);
                mAdapter.notifyDataSetChanged();
            }
        });
        return iconItem_ly;
    }


    public PropertyBar getPropertyBar() {
        return mPropertyBar;
    }

    public boolean isAttachmentOpening() {
        return mAnnotHandler.isAttachmentOpening();
    }

    public interface IAttachmentDocEvent {
        void onAttachmentDocWillOpen();
        void onAttachmentDocOpened(PDFDoc document, int errCode);
        void onAttachmentDocWillClose();
        void onAttachmentDocClosed();
    }

    private PropertyCircleItem mPropertyItem;
    private IBaseItem mMoreItem;
    private IBaseItem mOKItem;
    private IBaseItem mContinuousCreateItem;

    private boolean mIsContinuousCreate = false;

    private void resetAnnotBar() {
        if (mUiExtensionsManager != null && mUiExtensionsManager instanceof UIExtensionsManager) {
            final UIExtensionsManager uiExtensionsManager = (UIExtensionsManager) mUiExtensionsManager;
            uiExtensionsManager.getMainFrame().getToolSetBar().removeAllItems();

            mMoreItem = new CircleItemImpl(mContext) {

                @Override
                public void onItemLayout(int l, int t, int r, int b) {

                    if (mToolHandler == uiExtensionsManager.getCurrentToolHandler()) {
                        if (uiExtensionsManager.getMainFrame().getMoreToolsBar().isShowing()) {
                            Rect rect = new Rect();
                            mMoreItem.getContentView().getGlobalVisibleRect(rect);
                            uiExtensionsManager.getMainFrame().getMoreToolsBar().update(new RectF(rect));
                        }
                    }
                }
            };
            mMoreItem.setTag(ToolbarItemConfig.ANNOT_BAR_ITEM_MORE);
            mMoreItem.setImageResource(R.drawable.mt_more_selector);
            mMoreItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Rect rect = new Rect();
                    mMoreItem.getContentView().getGlobalVisibleRect(rect);
                    uiExtensionsManager.getMainFrame().getMoreToolsBar().show(new RectF(rect), true);
                }
            });

            mOKItem = new CircleItemImpl(mContext);
            mOKItem.setTag(ToolbarItemConfig.ANNOT_BAR_ITEM_OK);
            mOKItem.setImageResource(R.drawable.rd_annot_create_ok_selector);
            mOKItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    uiExtensionsManager.changeState(ReadStateConfig.STATE_EDIT);
                    uiExtensionsManager.setCurrentToolHandler(null);
                }
            });

            mPropertyItem = new PropertyCircleItemImp(mContext) {

                @Override
                public void onItemLayout(int l, int t, int r, int b) {
                    if (mToolHandler == uiExtensionsManager.getCurrentToolHandler()) {
                        if (mPropertyBar.isShowing()) {
                            Rect rect = new Rect();
                            mPropertyItem.getContentView().getGlobalVisibleRect(rect);
                            mPropertyBar.update(new RectF(rect));
                        }
                    }
                }
            };
            mPropertyItem.setTag(ToolbarItemConfig.ITEM_PROPERTY_TAG);
            mPropertyItem.setCentreCircleColor(mToolHandler.getColor());

            final Rect rect = new Rect();
            mPropertyItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    resetPropertyBar();
                    mPropertyBar.setArrowVisible(true);
                    mPropertyItem.getContentView().getGlobalVisibleRect(rect);
                    mPropertyBar.show(new RectF(rect), true);
                }
            });

            mContinuousCreateItem = new CircleItemImpl(mContext);
            mContinuousCreateItem.setTag(ToolbarItemConfig.ANNOT_BAR_ITEM_CONTINUE);

            mIsContinuousCreate = false;
            mToolHandler.setIsContinuousCreate(mIsContinuousCreate);
            mContinuousCreateItem.setImageResource(R.drawable.rd_annot_create_continuously_false_selector);

            mContinuousCreateItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (AppUtil.isFastDoubleClick()) {
                        return;
                    }

                    if (mIsContinuousCreate) {
                        mIsContinuousCreate = false;
                        mContinuousCreateItem.setImageResource(R.drawable.rd_annot_create_continuously_false_selector);
                    } else {
                        mIsContinuousCreate = true;
                        mContinuousCreateItem.setImageResource(R.drawable.rd_annot_create_continuously_true_selector);
                    }

                    mToolHandler.setIsContinuousCreate(mIsContinuousCreate);
                    AppAnnotUtil.getInstance(mContext).showAnnotContinueCreateToast(mIsContinuousCreate);
                }
            });

            uiExtensionsManager.getMainFrame().getToolSetBar().addView(mMoreItem, BaseBar.TB_Position.Position_CENTER);
            uiExtensionsManager.getMainFrame().getToolSetBar().addView(mPropertyItem, BaseBar.TB_Position.Position_CENTER);
            uiExtensionsManager.getMainFrame().getToolSetBar().addView(mOKItem, BaseBar.TB_Position.Position_CENTER);
            uiExtensionsManager.getMainFrame().getToolSetBar().addView(mContinuousCreateItem, BaseBar.TB_Position.Position_CENTER);
        }
    }

    private int mOldState;

    private FileAttachmentModule.IAttachmentDocEvent mAttachmentDocEvent = new FileAttachmentModule.IAttachmentDocEvent(){
        @Override
        public void onAttachmentDocWillOpen() {
            ((UIExtensionsManager) mUiExtensionsManager).getMainFrame().getPanelManager().hidePanel();
        }

        @Override
        public void onAttachmentDocOpened(PDFDoc document, int errCode) {

            if (errCode == Constants.e_ErrSuccess) {
                UIExtensionsManager uiExtensionsManager = (UIExtensionsManager) mUiExtensionsManager;

                mOldState = uiExtensionsManager.getState();
                uiExtensionsManager.changeState(ReadStateConfig.STATE_NORMAL);
                PageNavigationModule module = (PageNavigationModule) uiExtensionsManager.getModuleByName(Module.MODULE_NAME_PAGENAV);
                if (module != null) {
                    module.changPageNumberState(false);
                }
            }
        }

        @Override
        public void onAttachmentDocWillClose() {
            ((UIExtensionsManager) mUiExtensionsManager).changeState(mOldState);
        }

        @Override
        public void onAttachmentDocClosed() {
        }
    };

}
