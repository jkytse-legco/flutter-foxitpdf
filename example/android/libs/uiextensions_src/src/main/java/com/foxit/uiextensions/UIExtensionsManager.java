/**
 * Copyright (C) 2003-2018, Foxit Software Inc..
 * All Rights Reserved.
 * <p>
 * http://www.foxitsoftware.com
 * <p>
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */
package com.foxit.uiextensions;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Canvas;
import android.graphics.PointF;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.InputType;
import android.util.SparseArray;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.foxit.sdk.PDFViewCtrl;
import com.foxit.sdk.addon.ConnectedPDF.ClientInfo;
import com.foxit.sdk.addon.xfa.XFADoc;
import com.foxit.sdk.addon.xfa.XFAWidget;
import com.foxit.sdk.pdf.PDFDoc;
import com.foxit.sdk.pdf.annots.Annot;
import com.foxit.uiextensions.annots.AnnotActionHandler;
import com.foxit.uiextensions.annots.AnnotHandler;
import com.foxit.uiextensions.annots.caret.CaretModule;
import com.foxit.uiextensions.annots.circle.CircleModule;
import com.foxit.uiextensions.annots.fileattachment.FileAttachmentModule;
import com.foxit.uiextensions.annots.form.FormFillerModule;
import com.foxit.uiextensions.annots.form.FormNavigationModule;
import com.foxit.uiextensions.annots.freetext.callout.CalloutModule;
import com.foxit.uiextensions.annots.freetext.textbox.TextBoxModule;
import com.foxit.uiextensions.annots.freetext.typewriter.TypewriterModule;
import com.foxit.uiextensions.annots.ink.EraserModule;
import com.foxit.uiextensions.annots.ink.InkModule;
import com.foxit.uiextensions.annots.line.LineModule;
import com.foxit.uiextensions.annots.link.LinkModule;
import com.foxit.uiextensions.annots.note.NoteModule;
import com.foxit.uiextensions.annots.polygon.PolygonModule;
import com.foxit.uiextensions.annots.polyline.PolyLineModule;
import com.foxit.uiextensions.annots.screen.image.PDFImageModule;
import com.foxit.uiextensions.annots.screen.multimedia.MultimediaModule;
import com.foxit.uiextensions.annots.square.SquareModule;
import com.foxit.uiextensions.annots.stamp.StampModule;
import com.foxit.uiextensions.annots.textmarkup.highlight.HighlightModule;
import com.foxit.uiextensions.annots.textmarkup.squiggly.SquigglyModule;
import com.foxit.uiextensions.annots.textmarkup.strikeout.StrikeoutModule;
import com.foxit.uiextensions.annots.textmarkup.underline.UnderlineModule;
import com.foxit.uiextensions.controls.dialog.AppDialogManager;
import com.foxit.uiextensions.controls.dialog.MatchDialog;
import com.foxit.uiextensions.controls.dialog.UITextEditDialog;
import com.foxit.uiextensions.controls.dialog.fileselect.UIFolderSelectDialog;
import com.foxit.uiextensions.controls.menu.MoreMenuModule;
import com.foxit.uiextensions.controls.panel.PanelSpec;
import com.foxit.uiextensions.controls.panel.PanelSpec.PanelType;
import com.foxit.uiextensions.controls.propertybar.IMultiLineBar;
import com.foxit.uiextensions.controls.toolbar.IBarsHandler;
import com.foxit.uiextensions.controls.toolbar.impl.BaseBarManager;
import com.foxit.uiextensions.home.local.LocalModule;
import com.foxit.uiextensions.modules.BrightnessModule;
import com.foxit.uiextensions.modules.DocInfoModule;
import com.foxit.uiextensions.modules.DynamicXFA.DynamicXFAModule;
import com.foxit.uiextensions.modules.DynamicXFA.DynamicXFAWidgetHandler;
import com.foxit.uiextensions.modules.DynamicXFA.IXFAPageEventListener;
import com.foxit.uiextensions.modules.DynamicXFA.IXFAWidgetEventListener;
import com.foxit.uiextensions.modules.OutlineModule;
import com.foxit.uiextensions.modules.PageNavigationModule;
import com.foxit.uiextensions.modules.ReadingBookmarkModule;
import com.foxit.uiextensions.modules.ReflowModule;
import com.foxit.uiextensions.modules.ScreenLockModule;
import com.foxit.uiextensions.modules.SearchModule;
import com.foxit.uiextensions.modules.UndoModule;
import com.foxit.uiextensions.modules.connectpdf.account.AccountModule;
import com.foxit.uiextensions.modules.crop.CropModule;
import com.foxit.uiextensions.modules.panel.IPanelManager;
import com.foxit.uiextensions.modules.panel.PanelManager;
import com.foxit.uiextensions.modules.panel.annot.AnnotPanelModule;
import com.foxit.uiextensions.modules.panel.filespec.FileSpecPanelModule;
import com.foxit.uiextensions.modules.panzoom.PanZoomModule;
import com.foxit.uiextensions.modules.signature.SignatureModule;
import com.foxit.uiextensions.modules.signature.SignatureToolHandler;
import com.foxit.uiextensions.modules.thumbnail.ThumbnailModule;
import com.foxit.uiextensions.pdfreader.ILifecycleEventListener;
import com.foxit.uiextensions.pdfreader.IMainFrame;
import com.foxit.uiextensions.pdfreader.IStateChangeListener;
import com.foxit.uiextensions.pdfreader.config.ReadStateConfig;
import com.foxit.uiextensions.pdfreader.impl.MainFrame;
import com.foxit.uiextensions.print.IPrintResultCallback;
import com.foxit.uiextensions.print.PDFPrint;
import com.foxit.uiextensions.print.PDFPrintAdapter;
import com.foxit.uiextensions.print.PrintModule;
import com.foxit.uiextensions.print.XFAPrintAdapter;
import com.foxit.uiextensions.security.digitalsignature.DigitalSignatureModule;
import com.foxit.uiextensions.security.standard.PasswordModule;
import com.foxit.uiextensions.textselect.BlankSelectToolHandler;
import com.foxit.uiextensions.textselect.TextSelectModule;
import com.foxit.uiextensions.textselect.TextSelectToolHandler;
import com.foxit.uiextensions.utils.AppAnnotUtil;
import com.foxit.uiextensions.utils.AppDevice;
import com.foxit.uiextensions.utils.AppDmUtil;
import com.foxit.uiextensions.utils.AppFileUtil;
import com.foxit.uiextensions.utils.AppResource;
import com.foxit.uiextensions.utils.AppSystemUtil;
import com.foxit.uiextensions.utils.AppUtil;
import com.foxit.uiextensions.utils.IResult;
import com.foxit.uiextensions.utils.UIToast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.foxit.sdk.common.Constants.e_ErrCanNotGetUserToken;
import static com.foxit.sdk.common.Constants.e_ErrPassword;
import static com.foxit.sdk.common.Constants.e_ErrSuccess;

/**
 * Class <CODE>UIExtensionsManager</CODE> represents a UI extensions manager.
 * <p/>
 * The <CODE>UIExtensionsManager</CODE> class is mainly used for manage the UI extensions which implement {@link ToolHandler} interface, it implements the {@link PDFViewCtrl.UIExtensionsManager}
 * interface that is a listener to listen common interaction events and view event, and will dispatch some events to UI extensions, it also defines functions to manage the UI extensions.
 */
public class UIExtensionsManager implements PDFViewCtrl.UIExtensionsManager, IPDFReader {


    /**
     *
     * The interface of {@link ToolHandler} change listener.<br/>
     * Note: This method is only used within RDK
     *
     */
    public interface ToolHandlerChangedListener {
        /**
         * Called when current {@link ToolHandler} is changed.
         *
         * @param oldToolHandler The old tool handler.
         * @param newToolHandler The new tool handler.
         */
        void onToolHandlerChanged(ToolHandler oldToolHandler, ToolHandler newToolHandler);
    }

    /**
     * Note: This method is only used within RDK
     */
    public interface ConfigurationChangedListener {
        /**
         * Called when {@link UIExtensionsManager#onConfigurationChanged(Activity, Configuration)} is called.
         *
         * @param newConfig
         */
        void onConfigurationChanged(Configuration newConfig);
    }

    /**
     * Note: This method is only used within RDK
     */
    public interface MenuEventListener {
        /**
         * Called when {@link #triggerDismissMenuEvent()} is called.
         */
        void onTriggerDismissMenu();
    }

    private ILinkEventListener mLinkEventListener = null;
    public static final int LINKTYPE_ANNOT = 0;
    public static final int LINKTYPE_TEXT = 1;

    public static class LinkInfo {

        /**
         * @see #LINKTYPE_ANNOT
         * @see #LINKTYPE_TEXT
         */
        public int linkType;
        /**
         * Should be link annotation or text link.
         */
        public Object link;
    }

    public interface ILinkEventListener {
        /**
         * Called when tap a link.
         *
         * @param linkInfo The link information of the tapped link.
         *
         * @return Return <code>true</code> to prevent this event from being propagated
         *         further, or <code>false</code> to indicate that you have not handled
         *         this event and it should continue to be propagated by Foxit.
         */
        boolean onLinkTapped(LinkInfo linkInfo);
    }

    /**
     * Set link event listener.
     *
     * @param listener The specified link event listener.
     */
    public void setLinkEventListener(ILinkEventListener listener) {
        mLinkEventListener = listener;
    }

    public interface OnFinishListener {
        /**
         * Usually called when close document and exit the current activity.
         */
        void onFinish();
    }

    public void setOnFinishListener(OnFinishListener listener) {
        onFinishListener = listener;
    }

    /**
     * Get link event listener object.
     *
     * @return The link event listener object.
     */
    public ILinkEventListener getLinkEventListener() {
        return mLinkEventListener;
    }

    private ToolHandler mCurToolHandler = null;
    private PDFViewCtrl mPdfViewCtrl = null;
    private List<Module> mModules = new ArrayList<Module>();
    private HashMap<String, ToolHandler> mToolHandlerList;
    private Map<PanelSpec.PanelType, Boolean> mMapPanelHiddenState;
    private SparseArray<AnnotHandler> mAnnotHandlerList;
    private ArrayList<ToolHandlerChangedListener> mHandlerChangedListeners;
    // IXFAPageEventListener
    private ArrayList<IXFAPageEventListener> mXFAPageEventListeners;
    private ArrayList<IXFAWidgetEventListener> mXFAWidgetEventListener;
    private ArrayList<ConfigurationChangedListener> mConfigurationChangedListeners;
    private ArrayList<MenuEventListener> mMenuEventListeners;

    private Activity mAttachActivity = null;
    private Context mContext;
    private ViewGroup mParent;
    private Config mModulesConfig;
    private boolean mEnableLinkAnnot = true;
    private boolean mEnableLinkHighlight = true;
    private boolean mEnableFormHighlight = true;
    private long mFormHighlightColor = 0x200066cc;
    private long mLinkHighlightColor = 0x16007FFF;
    private int mSelectHighlightColor = 0xFFACDAED;
    private IPanelManager mPanelManager = null;

    private DocumentManager documentManager;

    private String currentFileCachePath = null;
    private String mSavePath = null; // default save path
    private String mDocPath;
    private String mLanguage;
    private String mProgressMsg = null;
    private int mState = ReadStateConfig.STATE_NORMAL;
    private int mSaveFlag = PDFDoc.e_SaveFlagIncremental;
    private boolean bDocClosed = false;
    private boolean mPasswordError = false;
    private boolean isSaveDocInCurPath = false;
    private boolean isCloseDocAfterSaving = false;

    private ArrayList<ILifecycleEventListener> mLifecycleEventList;
    private ArrayList<IStateChangeListener> mStateChangeEventList;

    private MainFrame mMainFrame;
    private IBarsHandler mBaseBarMgr;
    private ProgressDialog mProgressDlg;
    private OnFinishListener onFinishListener;
    private BackEventListener mBackEventListener = null;
    private AlertDialog mSaveAlertDlg;

    private RelativeLayout mActivityLayout;
    private String mUserSavePath = null;

    /**
     * Instantiates a new UI extensions manager.
     *
     * @param context     A <CODE>Context</CODE> object which species the context.
     * @param pdfViewCtrl A <CODE>PDFViewCtrl</CODE> object which species the PDF view control.
     */
    public UIExtensionsManager(Context context, PDFViewCtrl pdfViewCtrl) {
        if (pdfViewCtrl == null) {
            throw new NullPointerException("PDF view control can't be null");
        }

        init(context, pdfViewCtrl, null);
    }

    /**
     * Instantiates a new UI extensions manager with modules config.
     *
     * @param context     A <CODE>Context</CODE> object which species the context.
     * @param pdfViewCtrl A <CODE>PDFViewCtrl</CODE> object which species the PDF view control.
     * @param config      A <CODE>Config</CODE> object which species a modules loading config,
     *                    if null, UIExtension manager will load all modules by default, and equal to {@link #UIExtensionsManager(Context, PDFViewCtrl)}.
     */
    public UIExtensionsManager(Context context, PDFViewCtrl pdfViewCtrl, Config config) {
        AppUtil.requireNonNull(pdfViewCtrl, "PDF view control can't be null");
        init(context, pdfViewCtrl, config);
    }

    private void init(Context context, PDFViewCtrl pdfViewCtrl, Config config) {
        mContext = context;
        mPdfViewCtrl = pdfViewCtrl;
        documentManager = new DocumentManager(pdfViewCtrl);

        mToolHandlerList = new HashMap<String, ToolHandler>(8);
        mMapPanelHiddenState = new HashMap<PanelSpec.PanelType, Boolean>();
        mAnnotHandlerList = new SparseArray<AnnotHandler>(8);
        mHandlerChangedListeners = new ArrayList<ToolHandlerChangedListener>();
        mXFAPageEventListeners = new ArrayList<IXFAPageEventListener>();
        mXFAWidgetEventListener = new ArrayList<IXFAWidgetEventListener>();
        mConfigurationChangedListeners = new ArrayList<ConfigurationChangedListener>();
        mLifecycleEventList = new ArrayList<ILifecycleEventListener>();
        mStateChangeEventList = new ArrayList<IStateChangeListener>();
        mMenuEventListeners = new ArrayList<MenuEventListener>();
        pdfViewCtrl.registerDocEventListener(mDocEventListener);
        pdfViewCtrl.registerRecoveryEventListener(mRecoveryEventListener);
        pdfViewCtrl.registerDoubleTapEventListener(mDoubleTapEventListener);
        pdfViewCtrl.registerTouchEventListener(mTouchEventListener);
        pdfViewCtrl.registerPageEventListener(mPageEventListener);

        pdfViewCtrl.setUIExtensionsManager(this);
        registerMenuEventListener(mMenuEventListener);

        if (config == null) {
            mModulesConfig = new Config();
        } else {
            mModulesConfig = config;
        }

        mMainFrame = new MainFrame(mContext, mModulesConfig);
        mBaseBarMgr = new BaseBarManager(mContext, mMainFrame);

        if (mActivityLayout == null) {
            mActivityLayout = new RelativeLayout(mContext);
        } else {
            mActivityLayout.removeAllViews();
            mActivityLayout = new RelativeLayout(mContext);
        }
        mActivityLayout.setId(R.id.rd_main_id);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);

        mMainFrame.init(this);
        mMainFrame.addDocView(mPdfViewCtrl);
        mActivityLayout.addView(mMainFrame.getContentView(), params);
        mParent = mMainFrame.getContentView();
        mPanelManager = mMainFrame.getPanelManager();
        if (mMainFrame.getAttachedActivity() != null) {
            setAttachedActivity(mMainFrame.getAttachedActivity());
        }

        if (mPanelManager == null) {
            mPanelManager = new PanelManager(context, this, mParent, null);
        }

        loadAllModules();

        if (mModulesConfig.isLoadForm() || mModulesConfig.getAnnotConfig().isLoadStamp()) {
            AnnotActionHandler actionHandler = (AnnotActionHandler) getDocumentManager().getActionCallback();
            if (actionHandler == null) {
                actionHandler = new AnnotActionHandler(mContext, mPdfViewCtrl);
            }
            getDocumentManager().setActionCallback(actionHandler);
        }

        pdfViewCtrl.setConnectedPDFEventListener(mConnectedPDFEventListener); // for CDRM Document
    }


    private void loadAllModules() {
        if (mModulesConfig.isLoadTextSelection()) {
            //text select module
            TextSelectModule tsModule = new TextSelectModule(mContext, mPdfViewCtrl, this);
            tsModule.loadModule();
        }

        if (mModulesConfig.isLoadAnnotations() || mModulesConfig.isLoadSignature()) {
            registerToolHandler(new BlankSelectToolHandler(mContext, mPdfViewCtrl, this));
        }

        if (mModulesConfig.isLoadAnnotations()) {
            Config.AnnotConfig annotConfig = mModulesConfig.getAnnotConfig();

            if (annotConfig.isLoadSquiggly()) {
                //squiggly annotation module
                SquigglyModule sqgModule = new SquigglyModule(mContext, mPdfViewCtrl, this);
                sqgModule.loadModule();
            }

            if (annotConfig.isLoadStrikeout() || annotConfig.isLoadReplaceText()) {
                //strikeout annotation module
                StrikeoutModule stoModule = new StrikeoutModule(mContext, mPdfViewCtrl, this);
                stoModule.loadModule();
            }

            if (annotConfig.isLoadUnderline()) {
                //underline annotation module
                UnderlineModule unlModule = new UnderlineModule(mContext, mPdfViewCtrl, this);
                unlModule.loadModule();
            }

            if (annotConfig.isLoadHighlight()) {
                //highlight annotation module
                HighlightModule hltModule = new HighlightModule(mContext, mPdfViewCtrl, this);
                hltModule.loadModule();
            }

            if (annotConfig.isLoadNote()) {
                //note annotation module
                NoteModule noteModule = new NoteModule(mContext, mParent, mPdfViewCtrl, this);
                noteModule.loadModule();
            }

            if (annotConfig.isLoadDrawCircle()) {
                //circle module
                CircleModule circleModule = new CircleModule(mContext, mPdfViewCtrl, this);
                circleModule.loadModule();
            }

            if (annotConfig.isLoadDrawSquare()) {
                //square module
                SquareModule squareModule = new SquareModule(mContext, mPdfViewCtrl, this);
                squareModule.loadModule();
            }

            if (annotConfig.isLoadTypewriter()) {
                //freetext: typewriter
                TypewriterModule typewriterModule = new TypewriterModule(mContext, mPdfViewCtrl, this);
                typewriterModule.loadModule();
            }

            if (annotConfig.isLoadCallout()) {
                // freetext: callout
                CalloutModule calloutModule = new CalloutModule(mContext, mPdfViewCtrl, this);
                calloutModule.loadModule();
            }

            if (annotConfig.isLoadStamp()) {
                //stamp module
                StampModule stampModule = new StampModule(mContext, mParent, mPdfViewCtrl, this);
                stampModule.loadModule();
            }

            if (annotConfig.isLoadInsertText() || annotConfig.isLoadReplaceText()) {
                //Caret module
                CaretModule caretModule = new CaretModule(mContext, mPdfViewCtrl, this);
                caretModule.loadModule();
            }

            if (annotConfig.isLoadDrawPencil() || annotConfig.isLoadEraser()) {
                //ink(pencil) module
                InkModule inkModule = new InkModule(mContext, mPdfViewCtrl, this);
                inkModule.loadModule();
            }

            if (annotConfig.isLoadEraser()) {
                //eraser module
                EraserModule eraserModule = new EraserModule(mContext, mPdfViewCtrl, this);
                eraserModule.loadModule();
            }

            if (annotConfig.isLoadDrawLine() || annotConfig.isLoadDrawArrow() || annotConfig.isLoadDrawDistance()) {
                //Line module
                LineModule lineModule = new LineModule(mContext, mPdfViewCtrl, this);
                lineModule.loadModule();
            }

            if (annotConfig.isLoadFileattach()) {
                //FileAttachment module
                FileAttachmentModule fileAttachmentModule = new FileAttachmentModule(mContext, mPdfViewCtrl, this);
                fileAttachmentModule.loadModule();
            }

            if (annotConfig.isLoadDrawPolygon() || annotConfig.isLoadDrawCloud()) {
                //Polygon module
                PolygonModule polygonModule = new PolygonModule(mContext, mPdfViewCtrl, this);
                polygonModule.loadModule();
            }

            if (annotConfig.isLoadDrawPolyLine()) {
                //PolyLine module
                PolyLineModule polyLineModule = new PolyLineModule(mContext, mPdfViewCtrl, this);
                polyLineModule.loadModule();
            }

            if (annotConfig.isLoadTextbox()) {
                //Textbox module
                TextBoxModule textBoxModule = new TextBoxModule(mContext, mPdfViewCtrl, this);
                textBoxModule.loadModule();
            }

            if (annotConfig.isLoadImage()) {
                // Image module
                PDFImageModule imageModule = new PDFImageModule(mContext, mPdfViewCtrl, this);
                imageModule.loadModule();
            }

            if (annotConfig.isLoadVideo() || annotConfig.isLoadAudio()) {
                // multimedia module
                MultimediaModule imageModule = new MultimediaModule(mContext, mPdfViewCtrl, this);
                imageModule.loadModule();
            }

            //link module
            LinkModule linkModule = new LinkModule(mContext, mPdfViewCtrl, this);
            linkModule.loadModule();

            //undo&redo module
            UndoModule undoModule = new UndoModule(mContext, mPdfViewCtrl, this);
            undoModule.loadModule();
        }
        if (mModulesConfig.isLoadPageNavigation()) {
            //page navigation module
            PageNavigationModule pageNavigationModule = new PageNavigationModule(mContext, mParent, mPdfViewCtrl, this);
            pageNavigationModule.loadModule();
        }

        if (mModulesConfig.isLoadForm()) {
            //form annotation module
            FormFillerModule formFillerModule = new FormFillerModule(mContext, mParent, mPdfViewCtrl, this);
            formFillerModule.loadModule();
        }

        if (mModulesConfig.isLoadSignature()) {
            //signature module
            SignatureModule signatureModule = new SignatureModule(mContext, mParent, mPdfViewCtrl, this);
            signatureModule.loadModule();

            DigitalSignatureModule dsgModule = new DigitalSignatureModule(mContext, mParent, mPdfViewCtrl, this);
            dsgModule.loadModule();
        }

        if (mModulesConfig.isLoadSearch()) {
            SearchModule searchModule = new SearchModule(mContext, mParent, mPdfViewCtrl, this);
            searchModule.loadModule();
        }

        if (mModulesConfig.isLoadReadingBookmark()) {
            ReadingBookmarkModule readingBookmarkModule = new ReadingBookmarkModule(mContext, mParent, mPdfViewCtrl, this);
            readingBookmarkModule.loadModule();
        }

        if (mModulesConfig.isLoadOutline()) {
            OutlineModule outlineModule = new OutlineModule(mContext, mParent, mPdfViewCtrl, this);
            outlineModule.loadModule();
        }

        if (mModulesConfig.isLoadAnnotations()) {
            //annot panel
            AnnotPanelModule annotPanelModule = new AnnotPanelModule(mContext, mPdfViewCtrl, this);
            annotPanelModule.loadModule();
        }

        if (mModulesConfig.isLoadAttachment()) {
            FileSpecPanelModule fileSpecPanelModule = new FileSpecPanelModule(mContext, mParent, mPdfViewCtrl, this);
            fileSpecPanelModule.loadModule();
        }

        if (mModulesConfig.isLoadThumbnail()) {
            ThumbnailModule thumbnailModule = new ThumbnailModule(mContext, mPdfViewCtrl, this);
            thumbnailModule.loadModule();
        }

        if (mModulesConfig.isLoadFileEncryption()) {
            //password module
            PasswordModule passwordModule = new PasswordModule(mContext, mPdfViewCtrl, this);
            passwordModule.loadModule();
        }

        //form navigation module
        FormNavigationModule formNavigationModule = new FormNavigationModule(mContext, mParent, this);
        formNavigationModule.loadModule();

        ReflowModule reflowModule = new ReflowModule(mContext, mParent, mPdfViewCtrl, this);
        reflowModule.loadModule();

        DocInfoModule docInfoModule = new DocInfoModule(mContext, mParent, mPdfViewCtrl, this);
        docInfoModule.loadModule();

        BrightnessModule brightnessModule = new BrightnessModule(mContext, mPdfViewCtrl, this);
        brightnessModule.loadModule();

        ScreenLockModule screenLockModule = new ScreenLockModule(this);
        screenLockModule.loadModule();

        PrintModule printModule = new PrintModule(mContext, mPdfViewCtrl, this);
        printModule.loadModule();

        MoreMenuModule mMoreMenuModule = new MoreMenuModule(mContext, mParent, mPdfViewCtrl, this);
        mMoreMenuModule.loadModule();

        CropModule cropModule = new CropModule(mContext, mParent, mPdfViewCtrl, this);
        cropModule.loadModule();

        PanZoomModule panZoomModule = new PanZoomModule(mContext, mParent, mPdfViewCtrl, this);
        panZoomModule.loadModule();

        DynamicXFAModule dynamicXFAModule = new DynamicXFAModule(mContext, mParent, mPdfViewCtrl, this);
        dynamicXFAModule.loadModule();
    }

    public ViewGroup getRootView() {
        return mParent;
    }

//    @Override
//    protected void finalize() throws Throwable {
//        super.finalize();
//    }

    /**
     * Register the {@link ToolHandler} changed listener.
     *
     * Note: This method is only used within RDK
     */
    public void registerToolHandlerChangedListener(ToolHandlerChangedListener listener) {
        mHandlerChangedListeners.add(listener);
    }

    /**
     * Unregister the {@link ToolHandler} changed listener.
     *
     * Note: This method is only used within RDK
     *
     * @param listener A <CODE>ToolHandlerChangedListener</CODE> object which specifies the {@link ToolHandler} changed listener.
     */
    public void unregisterToolHandlerChangedListener(ToolHandlerChangedListener listener) {
        mHandlerChangedListeners.remove(listener);
    }

    private void onToolHandlerChanged(ToolHandler lastTool, ToolHandler currentTool) {
        for (ToolHandlerChangedListener listener : mHandlerChangedListeners) {
            listener.onToolHandlerChanged(lastTool, currentTool);
        }
    }

    public void registerConfigurationChangedListener(ConfigurationChangedListener listener) {
        mConfigurationChangedListeners.add(listener);
    }

    /**
     * unregister the {@link ConfigurationChangedListener}.
     *
     * @param listener
     */
    public void unregisterConfigurationChangedListener(ConfigurationChangedListener listener) {
        mConfigurationChangedListeners.remove(listener);
    }

    /**
     * Register a xfa page event listener.
     *
     * @param listener An <CODE>IPageEventListener</CODE> object to be registered.
     */
    public void registerXFAPageEventListener(IXFAPageEventListener listener) {
        mXFAPageEventListeners.add(listener);
    }

    /**
     * Unregister a xfa page event listener.
     *
     * @param listener An <CODE>IPageEventListener</CODE> object to be unregistered.
     */
    public void unregisterXFAPageEventListener(IXFAPageEventListener listener) {
        mXFAPageEventListeners.remove(listener);
    }

    public void onXFAPageRemoved(boolean isSuccess, int pageIndex) {
        for (IXFAPageEventListener listener : mXFAPageEventListeners) {
            listener.onPagesRemoved(isSuccess, pageIndex);
        }
    }

    public void onXFAPagesInserted(boolean isSuccess, int pageIndex) {
        for (IXFAPageEventListener listener : mXFAPageEventListeners) {
            listener.onPagesInserted(isSuccess, pageIndex);
        }
    }


    /**
     * Register a xfa widget event listener.
     *
     * @param listener An <CODE>IXFAWidgetEventListener</CODE> object to be registered.
     */
    public void registerXFAWidgetEventListener(IXFAWidgetEventListener listener) {
        mXFAWidgetEventListener.add(listener);
    }

    /**
     * Unregister a xfa widget event listener.
     *
     * @param listener An <CODE>IXFAWidgetEventListener</CODE> object to be unregistered.
     */
    public void unregisterXFAWidgetEventListener(IXFAWidgetEventListener listener) {
        mXFAWidgetEventListener.remove(listener);
    }

    public void onXFAWidgetAdded(XFAWidget xfaWidget) {
        for (IXFAWidgetEventListener listener : mXFAWidgetEventListener) {
            listener.onXFAWidgetAdded(xfaWidget);
        }
    }

    public void onXFAWidgetWillRemove(XFAWidget xfaWidget) {
        for (IXFAWidgetEventListener listener : mXFAWidgetEventListener) {
            listener.onXFAWidgetWillRemove(xfaWidget);
        }
    }

    /**
     * Set the current tool handler.
     *
     * @param toolHandler A <CODE>ToolHandler</CODE> object which specifies the current tool handler.
     */
    public void setCurrentToolHandler(ToolHandler toolHandler) {
        if (toolHandler == null && mCurToolHandler == null) {
            return;
        }

        boolean canAdd = true;
        if (toolHandler != null) {
            if (toolHandler.getType().equals(ToolHandler.TH_TYPE_SIGNATURE)) {
                canAdd = getDocumentManager().canAddSignature();
            } else {
                // Now, others are all annotation tool handler
                canAdd = getDocumentManager().canAddAnnot();
            }
        }

        if (!canAdd || (toolHandler != null && mCurToolHandler != null && mCurToolHandler.getType().equals(toolHandler.getType()))) {
            return;
        }
        ToolHandler lastToolHandler = mCurToolHandler;
        if (lastToolHandler != null) {
            lastToolHandler.onDeactivate();
        }

        if (toolHandler != null) {
            if (getDocumentManager().getCurrentAnnot() != null) {
                getDocumentManager().setCurrentAnnot(null);
            }
        }

        mCurToolHandler = toolHandler;
        if (mCurToolHandler != null) {
            mCurToolHandler.onActivate();
        }

        changeToolBarState(lastToolHandler, mCurToolHandler);
        onToolHandlerChanged(lastToolHandler, mCurToolHandler);
    }

    /**
     * Get the current tool handler.
     *
     * @return A <CODE>ToolHandler</CODE> object which specifies the current tool handler.
     */
    public ToolHandler getCurrentToolHandler() {
        return mCurToolHandler;
    }

    /**
     * Register the specified tool handler to current UI extensions manager.
     *
     * Note: This method is only used within RDK
     *
     * @param handler A <CODE>ToolHandler</CODE> object to be registered.
     */
    public void registerToolHandler(ToolHandler handler) {
        mToolHandlerList.put(handler.getType(), handler);
    }

    /**
     * Unregister the specified tool handler from current UI extensions manager.
     *
     * Note: This method is only used within RDK
     *
     * @param handler A <CODE>ToolHandler</CODE> object to be unregistered.
     */
    public void unregisterToolHandler(ToolHandler handler) {
        mToolHandlerList.remove(handler.getType());
    }

    /**
     * get the specified tool handler from current UI extensions manager.
     *
     * @param type The tool handler type, refer to function {@link ToolHandler#getType()}.
     * @return A <CODE>ToolHandler</CODE> object with specified type.
     */
    public ToolHandler getToolHandlerByType(String type) {
        return mToolHandlerList.get(type);
    }

    protected void registerAnnotHandler(AnnotHandler handler) {
        mAnnotHandlerList.put(handler.getType(), handler);
    }

    protected void unregisterAnnotHandler(AnnotHandler handler) {
        mAnnotHandlerList.remove(handler.getType());
    }

    protected AnnotHandler getCurrentAnnotHandler() {
        Annot curAnnot = getDocumentManager().getCurrentAnnot();
        if (curAnnot == null) {
            return null;
        }

        return getAnnotHandlerByType(AppAnnotUtil.getAnnotHandlerType(curAnnot));
    }

    protected AnnotHandler getAnnotHandlerByType(int type) {
        return mAnnotHandlerList.get(type);
    }

    /**
     * Register the specified module to current UI extensions manager.
     *
     * Note: This method is only used within RDK
     *
     * @param module A <CODE>Module</CODE> object to be registered.
     */
    public void registerModule(Module module) {
        mModules.add(module);
    }

    /**
     * Unregister the specified module from current UI extensions manager.
     * Note: This method is only used within RDK
     *
     * @param module A <CODE>Module</CODE> object to be unregistered.
     */
    public void unregisterModule(Module module) {
        mModules.remove(module);
    }


    /**
     * Get the specified module from current UI extensions manager.
     *
     * @param name The specified module name, refer to {@link Module#getName()}.
     * @return A <CODE>Module</CODE> object with specified module name.
     */
    public Module getModuleByName(String name) {
        for (Module module : mModules) {
            String moduleName = module.getName();
            if (moduleName != null && moduleName.compareTo(name) == 0)
                return module;
        }
        return null;
    }

    /**
     * Enable link annotation action event.
     *
     * @param enable True means link annotation action event can be triggered, false for else.
     */
    public void enableLinks(boolean enable) {
        mEnableLinkAnnot = enable;
    }

    /**
     * Check whether link annotation action event can be triggered.
     *
     * @return True means link annotation action event can be triggered, false for else.
     */
    public boolean isLinksEnabled() {
        return mEnableLinkAnnot;
    }

    /**
     * Check whether link highlight can be display.
     *
     * @return True means link highlight can be display, false for else.
     */
    public boolean isLinkHighlightEnabled() {
        return mEnableLinkHighlight;
    }

    /**
     * Enable link highlight
     *
     * @param enable True means link highlight can be display, false for else.
     */
    public void enableLinkHighlight(boolean enable) {
        this.mEnableLinkHighlight = enable;
    }


    /**
     * get link highlight color
     *
     * @return link highlight color
     */
    public long getLinkHighlightColor() {
        return mLinkHighlightColor;
    }

    /**
     * set link highlight color
     *
     * @param color the link highlight color to be set
     */
    public void setLinkHighlightColor(long color) {
        this.mLinkHighlightColor = color;
    }

    /**
     * Check whether form highlight can be display.
     *
     * @return True means form highlight can be display, false for else.
     */
    public boolean isFormHighlightEnable() {
        return mEnableFormHighlight;
    }

    /**
     * get form highlight color
     *
     * @return form highlight color
     */
    public long getFormHighlightColor() {
        return mFormHighlightColor;
    }

    /**
     * Enable form highlight
     *
     * @param enable True means link highlight can be display, false for else.
     */
    public void enableFormHighlight(boolean enable) {
        this.mEnableFormHighlight = enable;
    }

    /**
     * set form highlight color
     *
     * @param color the form highlight color to be set
     */
    public void setFormHighlightColor(long color) {
        this.mFormHighlightColor = color;
    }

    /**
     * Set highlight color (including alpha) for text select tool handler.
     *
     * @param color The highlight color to be set.
     */
    public void setSelectionHighlightColor(int color) {
        mSelectHighlightColor = color;
    }

    /**
     * Get highlight color (including alpha) of text select tool handler.
     *
     * @return The highlight color.
     */
    public int getSelectionHighlightColor() {
        return mSelectHighlightColor;
    }

    /**
     * Get current selected text content from text select tool handler.
     *
     * @return The current selected text content.
     */
    public String getCurrentSelectedText() {
        ToolHandler selectionTool = getToolHandlerByType(ToolHandler.TH_TYPE_TEXTSELECT);
        if (selectionTool != null) {
            return ((TextSelectToolHandler) selectionTool).getCurrentSelectedText();
        }

        return null;
    }

    /**
     * Note: This method is only used within RDK
     */
    public void registerMenuEventListener(MenuEventListener listener) {
        mMenuEventListeners.add(listener);
    }

    /**
     * Note: This method is only used within RDK
     */
    public void unregisterMenuEventListener(MenuEventListener listener) {
        mMenuEventListeners.remove(listener);
    }

    /**
     * Trigger dismiss menu event.
     */
    public void triggerDismissMenuEvent() {
        for (MenuEventListener listener : mMenuEventListeners) {
            listener.onTriggerDismissMenu();
        }
    }

    /**
     * Note: This method is only used within RDK
     */
    @Override
    public boolean onTouchEvent(int pageIndex, MotionEvent motionEvent) {
        if (mPdfViewCtrl.isDynamicXFA()) {
            DynamicXFAModule dynamicXFAModule = (DynamicXFAModule) getModuleByName(Module.MODULE_NAME_DYNAMICXFA);
            if (dynamicXFAModule == null) return false;
            DynamicXFAWidgetHandler dynamicXFAWidgetHandler = (DynamicXFAWidgetHandler) dynamicXFAModule.getXFAWidgetHandler();
            if (dynamicXFAWidgetHandler == null) return false;
            return dynamicXFAWidgetHandler.onTouchEvent(pageIndex, motionEvent);
        }

        if (mPdfViewCtrl.getPageLayoutMode() == PDFViewCtrl.PAGELAYOUTMODE_REFLOW)
            return false;

        PanZoomModule panZoomModule = (PanZoomModule) getModuleByName(Module.MODULE_NAME_PANZOOM);
        if (panZoomModule != null) {
            panZoomModule.onTouchEvent(pageIndex, motionEvent);
        }

        if (motionEvent.getPointerCount() > 1) {
            return false;
        }

        if (mCurToolHandler != null) {
            if (mCurToolHandler.onTouchEvent(pageIndex, motionEvent)) {
                return true;
            }
            return false;
        } else {
            //annot handler
            if (getDocumentManager().onTouchEvent(pageIndex, motionEvent)) {
                return true;
            }

            //blank selection tool
            ToolHandler blankSelectionTool = getToolHandlerByType(ToolHandler.TH_TYPE_BLANKSELECT);
            if (blankSelectionTool != null && blankSelectionTool.onTouchEvent(pageIndex, motionEvent)) {
                return true;
            }

            //text selection tool
            ToolHandler textSelectionTool = getToolHandlerByType(ToolHandler.TH_TYPE_TEXTSELECT);
            if (textSelectionTool != null && textSelectionTool.onTouchEvent(pageIndex, motionEvent)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Note: This method is only used within RDK
     */
    @Override
    public boolean shouldViewCtrlDraw(Annot annot) {
        return getDocumentManager().shouldViewCtrlDraw(annot);
    }

    /**
     * Note: This method is only used within RDK
     */
    @Override
    public Annot getFocusAnnot() {
        if (mPdfViewCtrl == null) {
            return null;
        }
        return getDocumentManager().getFocusAnnot();
    }

    /**
     * Note: This method is only used within RDK
     */
    @SuppressLint("WrongCall")
    @Override
    public void onDraw(int pageIndex, Canvas canvas) {
        if (mPdfViewCtrl.isDynamicXFA()) {
            DynamicXFAModule dynamicXFAModule = (DynamicXFAModule) getModuleByName(Module.MODULE_NAME_DYNAMICXFA);
            if (dynamicXFAModule == null) return;
            DynamicXFAWidgetHandler dynamicXFAWidgetHandler = (DynamicXFAWidgetHandler) dynamicXFAModule.getXFAWidgetHandler();
            if (dynamicXFAWidgetHandler == null) return;
            dynamicXFAWidgetHandler.onDraw(pageIndex, canvas);
            return;
        }

        for (ToolHandler handler : mToolHandlerList.values()) {
            handler.onDraw(pageIndex, canvas);
        }

        for (int i = 0; i < mAnnotHandlerList.size(); i++) {
            int type = mAnnotHandlerList.keyAt(i);
            AnnotHandler handler = mAnnotHandlerList.get(type);
            if (handler != null)
                handler.onDraw(pageIndex, canvas);
        }
    }

    /**
     * Note: This method is only used within RDK
     */
    @Override
    public boolean onSingleTapConfirmed(MotionEvent motionEvent) {
        if (mPdfViewCtrl.isDynamicXFA()) {
            DynamicXFAModule dynamicXFAModule = (DynamicXFAModule) getModuleByName(Module.MODULE_NAME_DYNAMICXFA);
            if (dynamicXFAModule == null) return false;
            DynamicXFAWidgetHandler dynamicXFAWidgetHandler = (DynamicXFAWidgetHandler) dynamicXFAModule.getXFAWidgetHandler();
            if (dynamicXFAWidgetHandler == null) return false;
            PointF displayViewPt = new PointF(motionEvent.getX(), motionEvent.getY());
            int pageIndex = mPdfViewCtrl.getPageIndex(displayViewPt);
            return dynamicXFAWidgetHandler.onSingleTapConfirmed(pageIndex, motionEvent);
        }

        if (mPdfViewCtrl.getPageLayoutMode() == PDFViewCtrl.PAGELAYOUTMODE_REFLOW)
            return false;
        if (motionEvent.getPointerCount() > 1) {
            return false;
        }
        PointF displayViewPt = new PointF(motionEvent.getX(), motionEvent.getY());
        int pageIndex = mPdfViewCtrl.getPageIndex(displayViewPt);


        if (mCurToolHandler != null) {
            if (mCurToolHandler.onSingleTapConfirmed(pageIndex, motionEvent)) {
                return true;
            }
            return false;
        } else {
            //annot handler
            if (getDocumentManager().onSingleTapConfirmed(pageIndex, motionEvent)) {
                return true;
            }

            // blank selection tool
            ToolHandler blankSelectionTool = getToolHandlerByType(ToolHandler.TH_TYPE_BLANKSELECT);
            if (blankSelectionTool != null && blankSelectionTool.onSingleTapConfirmed(pageIndex, motionEvent)) {
                return true;
            }

            //text selection tool
            ToolHandler textSelectionTool = getToolHandlerByType(ToolHandler.TH_TYPE_TEXTSELECT);
            if (textSelectionTool != null && textSelectionTool.onSingleTapConfirmed(pageIndex, motionEvent)) {
                return true;
            }

            if (getDocumentManager().getCurrentAnnot() != null) {
                getDocumentManager().setCurrentAnnot(null);
                return true;
            }
        }
        return false;
    }

    /**
     * Note: This method is only used within RDK
     */
    @Override
    public boolean onDoubleTap(MotionEvent motionEvent) {
        return false;
    }

    /**
     * Note: This method is only used within RDK
     */
    @Override
    public boolean onDoubleTapEvent(MotionEvent motionEvent) {
        return false;
    }

    /**
     * Note: This method is only used within RDK
     */
    @Override
    public boolean onDown(MotionEvent motionEvent) {
        return false;
    }

    /**
     * Note: This method is only used within RDK
     */
    @Override
    public void onShowPress(MotionEvent motionEvent) {

    }

    /**
     * Note: This method is only used within RDK
     */
    @Override
    public boolean onSingleTapUp(MotionEvent motionEvent) {
        return false;
    }

    /**
     * Note: This method is only used within RDK
     */
    @Override
    public boolean onScroll(MotionEvent motionEvent, MotionEvent motionEvent1, float v, float v1) {
        return false;
    }

    /**
     * Note: This method is only used within RDK
     */
    @Override
    public void onLongPress(MotionEvent motionEvent) {
        if (mPdfViewCtrl.getPageLayoutMode() == PDFViewCtrl.PAGELAYOUTMODE_REFLOW)
            return;
        if (motionEvent.getPointerCount() > 1) {
            return;
        }
        PointF displayViewPt = new PointF(motionEvent.getX(), motionEvent.getY());
        int pageIndex = mPdfViewCtrl.getPageIndex(displayViewPt);

        if (mCurToolHandler != null) {
            if (mCurToolHandler.onLongPress(pageIndex, motionEvent)) {
                return;
            }
        } else {
            //annot handler
            if (getDocumentManager().onLongPress(pageIndex, motionEvent)) {
                return;
            }

            // blank selection tool
            ToolHandler blankSelectionTool = getToolHandlerByType(ToolHandler.TH_TYPE_BLANKSELECT);
            if (blankSelectionTool != null && blankSelectionTool.onLongPress(pageIndex, motionEvent)) {
                return;
            }

            //text selection tool
            ToolHandler textSelectionTool = getToolHandlerByType(ToolHandler.TH_TYPE_TEXTSELECT);
            if (textSelectionTool != null && textSelectionTool.onLongPress(pageIndex, motionEvent)) {
                return;
            }
        }
        return;
    }

    /**
     * Note: This method is only used within RDK
     */
    @Override
    public boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent1, float v, float v1) {
        return false;
    }

    /**
     * Note: This method is only used within RDK
     */
    @Override
    public boolean onScale(ScaleGestureDetector scaleGestureDetector) {
        return false;
    }

    /**
     * Note: This method is only used within RDK
     */
    @Override
    public boolean onScaleBegin(ScaleGestureDetector scaleGestureDetector) {
        return false;
    }

    /**
     * Note: This method is only used within RDK
     */
    @Override
    public void onScaleEnd(ScaleGestureDetector scaleGestureDetector) {
    }

    PDFViewCtrl.IDoubleTapEventListener mDoubleTapEventListener = new PDFViewCtrl.IDoubleTapEventListener() {
        @Override
        public boolean onSingleTapConfirmed(MotionEvent motionEvent) {
            if (getDocumentManager().getCurrentAnnot() != null) {
                getDocumentManager().setCurrentAnnot(null);
                return true;
            }
            if (mMainFrame.isToolbarsVisible()) {
                mMainFrame.hideToolbars();
            } else {
                mMainFrame.showToolbars();
            }
            return true;
        }

        @Override
        public boolean onDoubleTap(MotionEvent motionEvent) {
            if (mMainFrame.isToolbarsVisible()) {
                mMainFrame.hideToolbars();
            }
            return false;
        }

        @Override
        public boolean onDoubleTapEvent(MotionEvent motionEvent) {
            return false;
        }
    };

    private PDFViewCtrl.ITouchEventListener mTouchEventListener = new PDFViewCtrl.ITouchEventListener() {
        @Override
        public boolean onTouchEvent(MotionEvent event) {
            return false;
        }
    };

    PDFViewCtrl.IDocEventListener mDocEventListener = new PDFViewCtrl.IDocEventListener() {
        @Override
        public void onDocWillOpen() {
            mSaveFlag = PDFDoc.e_SaveFlagIncremental;
        }

        @Override
        public void onDocOpened(PDFDoc document, int errCode) {
            if (mProgressDlg != null && mProgressDlg.isShowing()) {
                AppDialogManager.getInstance().dismiss(mProgressDlg);
                mProgressDlg = null;
            }

            switch (errCode) {
                case e_ErrSuccess:
                    if (!mPdfViewCtrl.isDynamicXFA()) {
                        getDocumentManager().initDocProperties(document);
                    }
                    setFilePath(mPdfViewCtrl.getFilePath());

                    bDocClosed = false;
                    mPasswordError = false;
                    changeState(ReadStateConfig.STATE_NORMAL);
                    return;
                case e_ErrPassword:
                    String tips;
                    if (mPasswordError) {
                        tips = AppResource.getString(mContext, R.string.rv_tips_password_error);
                    } else {
                        tips = AppResource.getString(mContext, R.string.rv_tips_password);
                    }
                    final UITextEditDialog uiTextEditDialog = new UITextEditDialog(mMainFrame.getAttachedActivity());
                    uiTextEditDialog.getDialog().setCanceledOnTouchOutside(false);
                    uiTextEditDialog.getInputEditText().setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    uiTextEditDialog.setTitle(AppResource.getString(mContext, R.string.fx_string_passwordDialog_title));
                    uiTextEditDialog.getPromptTextView().setText(tips);
                    uiTextEditDialog.show();
                    AppUtil.showSoftInput(uiTextEditDialog.getInputEditText());
                    uiTextEditDialog.getOKButton().setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            uiTextEditDialog.dismiss();
                            AppUtil.dismissInputSoft(uiTextEditDialog.getInputEditText());
                            String pw = uiTextEditDialog.getInputEditText().getText().toString();
                            mPdfViewCtrl.openDoc(mDocPath, pw.getBytes());
                        }
                    });

                    uiTextEditDialog.getCancelButton().setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            uiTextEditDialog.dismiss();
                            AppUtil.dismissInputSoft(uiTextEditDialog.getInputEditText());
                            mPasswordError = false;
                            bDocClosed = true;
                            openDocumentFailed();
                        }
                    });

                    uiTextEditDialog.getDialog().setOnKeyListener(new DialogInterface.OnKeyListener() {
                        @Override
                        public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                            if (keyCode == KeyEvent.KEYCODE_BACK) {
                                uiTextEditDialog.getDialog().cancel();
                                mPasswordError = false;
                                bDocClosed = true;
                                openDocumentFailed();
                                return true;
                            }
                            return false;
                        }
                    });

                    if (!mPasswordError)
                        mPasswordError = true;
                    return;
                case e_ErrCanNotGetUserToken:
                    AccountModule.getInstance().showLoginDialog(mMainFrame.getAttachedActivity(), new IResult<Void, Void, Void>() {
                        @Override
                        public void onResult(boolean success, Void p1, Void p2, Void p3) {
                            if (success) {
                                mPdfViewCtrl.openDoc(mDocPath, null);
                            } else {
                                bDocClosed = true;
                                openDocumentFailed();
                            }
                        }
                    });
                    return;
                default:
                    bDocClosed = true;
                    String message = AppUtil.getMessage(mContext, errCode);
                    UIToast.getInstance(mContext).show(message);
                    openDocumentFailed();
                    break;
            }
        }

        @Override
        public void onDocWillClose(PDFDoc document) {
        }

        @Override
        public void onDocClosed(PDFDoc document, int errCode) {
            if (mProgressDlg != null && mProgressDlg.isShowing()) {
                AppDialogManager.getInstance().dismiss(mProgressDlg);
                mProgressDlg = null;
            }

            bDocClosed = true;
            closeDocumentSucceed();
            if (errCode == e_ErrSuccess && isSaveDocInCurPath) {
                updateThumbnail(mSavePath);
            }
        }

        @Override
        public void onDocWillSave(PDFDoc document) {
        }

        @Override
        public void onDocSaved(PDFDoc document, int errCode) {
            if (mProgressDlg != null && mProgressDlg.isShowing()) {
                AppDialogManager.getInstance().dismiss(mProgressDlg);
                mProgressDlg = null;
            }

            if (errCode == e_ErrSuccess && !isSaveDocInCurPath) {
                updateThumbnail(mSavePath);
            }

            if (isCloseDocAfterSaving){
                closeAllDocuments();
            }
        }
    };

    private PDFViewCtrl.IPageEventListener mPageEventListener = new PDFViewCtrl.IPageEventListener() {
        @Override
        public void onPageVisible(int index) {
        }

        @Override
        public void onPageInvisible(int index) {
        }

        @Override
        public void onPageChanged(int oldPageIndex, int curPageIndex) {
        }

        @Override
        public void onPageJumped() {
        }

        @Override
        public void onPagesWillRemove(int[] pageIndexes) {
        }

        @Override
        public void onPageWillMove(int index, int dstIndex) {
        }

        @Override
        public void onPagesWillRotate(int[] pageIndexes, int rotation) {
        }

        @Override
        public void onPagesRemoved(boolean success, int[] pageIndexes) {
            mSaveFlag = PDFDoc.e_SaveFlagXRefStream;
        }

        @Override
        public void onPageMoved(boolean success, int index, int dstIndex) {
        }

        @Override
        public void onPagesRotated(boolean success, int[] pageIndexes, int rotation) {
        }

        @Override
        public void onPagesInserted(boolean success, int dstIndex, int[] pageRanges) {
        }

        @Override
        public void onPagesWillInsert(int dstIndex, int[] pageRanges) {
        }
    };

    PDFViewCtrl.IRecoveryEventListener mRecoveryEventListener = new PDFViewCtrl.IRecoveryEventListener() {

        @Override
        public void onWillRecover() {
            mProgressMsg = "Recovering";
            showProgressDlg();
            getDocumentManager().mCurAnnot = null;
            getDocumentManager().clearUndoRedo();
            BlankSelectToolHandler toolHandler = (BlankSelectToolHandler) getToolHandlerByType(ToolHandler.TH_TYPE_BLANKSELECT);
            if (toolHandler != null) {
                toolHandler.dismissMenu();
            }

            synchronized (AppFileUtil.getInstance().isOOMHappened) {
                AppFileUtil.getInstance().isOOMHappened = true;
            }
        }

        @Override
        public void onRecovered() {
            if (mProgressDlg != null && mProgressDlg.isShowing()) {
                AppDialogManager.getInstance().dismiss(mProgressDlg);
                mProgressDlg = null;
            }
            if (!mPdfViewCtrl.isDynamicXFA()) {
                getDocumentManager().initDocProperties(mPdfViewCtrl.getDoc());
            }

            synchronized (AppFileUtil.getInstance().isOOMHappened) {
                AppFileUtil.getInstance().isOOMHappened = false;
            }
        }
    };

    /**
     * Set the attached activity.
     * <p>
     * If you want add a Note, FreeText, FileAttachment annotation; you must set the attached activity.
     * <p>
     * If you want to use the function of adding reply or comment to the annotation or about thumbnail,
     * you must set the attached activity and it must be a FragmentActivity.
     *
     * @param activity The attached activity.
     */
    public void setAttachedActivity(Activity activity) {
        mAttachActivity = activity;
    }

    /**
     * Get the attached activity.
     *
     * @return The attached activity.
     */
    public Activity getAttachedActivity() {
        return mAttachActivity;
    }

    /**
     * Return the current value in {@link #setPanelHidden}.
     *
     * @param panelType {@link PanelType#ReadingBookmarks}<br/>
     *                    {@link PanelType#Outline}<br/>
     *                    {@link PanelType#Annotations}<br/>
     *                    {@link PanelType#Attachments}<br/>
     * @return true means the panel is hidden.
     *
     * @see #setPanelHidden(boolean, PanelType)
     */
    public boolean isHiddenPanel(PanelSpec.PanelType panelType) {
        if (panelType == null) {
            return true;
        }

        if (mMapPanelHiddenState.get(panelType) == null) {
            switch (panelType) {
                case ReadingBookmarks:
                    return !mModulesConfig.isLoadReadingBookmark();
                case Outline:
                    return !mModulesConfig.isLoadOutline();
                case Annotations:
                    return !mModulesConfig.isLoadAnnotations();
                case Attachments:
                    return !mModulesConfig.isLoadAttachment();
                default:
                    break;
            }
        }
        return mMapPanelHiddenState.get(panelType).booleanValue();
    }

    /**
     * According to the {@link PanelType} control whether to show or hide the panel.
     *
     * It will be work while the annotation module has been loaded.
     *
     * @param isHidden  true means to hidden the panel.
     * @param panelType {@link PanelType#ReadingBookmarks}<br/>
     *                    {@link PanelType#Outline}<br/>
     *                    {@link PanelType#Annotations}<br/>
     *                    {@link PanelType#Attachments}<br/>
     */
    public void setPanelHidden(boolean isHidden, PanelSpec.PanelType panelType) {
        if (panelType == null || (mMapPanelHiddenState.get(panelType) == null && isHidden == false)) {
            return;
        }
        if (mMapPanelHiddenState.get(panelType) != null && mMapPanelHiddenState.get(panelType).booleanValue() == isHidden) {
            return;
        }

        if (isHidden) {
            Module module = getModuleByName(panelType.getModuleName());
            if (module != null) {

                if (module instanceof ReadingBookmarkModule) {
                    ((ReadingBookmarkModule) module).removePanel();
                } else {
                    module.unloadModule();
                    unregisterModule(module);
                }
                mMapPanelHiddenState.put(panelType, isHidden);
            }
        } else {
            switch (panelType) {
                case ReadingBookmarks:
                    if (mModulesConfig.isLoadReadingBookmark()) {
                        ReadingBookmarkModule readingBookmarkModule = (ReadingBookmarkModule) getModuleByName(panelType.getModuleName());
                        readingBookmarkModule.addPanel();
                    }
                    break;
                case Outline:
                    if (mModulesConfig.isLoadOutline()) {
                        OutlineModule outlineModule = new OutlineModule(mContext, mParent, mPdfViewCtrl, this);
                        outlineModule.loadModule();
                        outlineModule.prepareOutlinePanel();
                    }
                    break;
                case Annotations:
                    if (mModulesConfig.isLoadAnnotations()) {
                        AnnotPanelModule annotPanelModule = new AnnotPanelModule(mContext, mPdfViewCtrl, this);
                        annotPanelModule.loadModule();
                        annotPanelModule.prepareAnnotPanel();
                    }
                    break;
                case Attachments:
                    if (mModulesConfig.isLoadAttachment()) {
                        FileSpecPanelModule fileSpecPanelModule = new FileSpecPanelModule(mContext, mParent, mPdfViewCtrl, this);
                        fileSpecPanelModule.loadModule();
                    }
                    break;
                default:
                    break;
            }
            mMapPanelHiddenState.put(panelType, isHidden);
        }
    }

    /**
     * Print PDF documents and Static XFA documents(that is xfaDoc.getType() == XFADoc.e_Static)
     * <p>
     *  Note: Only when OS version is Kitkat and above (Android API >= 19) the print function can be used
     *
     * @param context The context to use. it must be instanceof Activity.
     * @param pdfDoc The {@link PDFDoc} Object, it can not be empty .
     * @param printJobName print job name, it is can be null or empty.
     * @param fileName The document name which may be shown to the user and
     * is the file name if the content it describes is saved as a PDF.
     * Cannot be empty.
     * @param callback print callback {@link IPrintResultCallback}
     */
    public void startPrintJob(Context context, PDFDoc pdfDoc, String printJobName, String fileName, IPrintResultCallback callback) {
        if (pdfDoc == null || pdfDoc.isEmpty()) return;
        new PDFPrint.Builder(context)
                .setAdapter(new PDFPrintAdapter(context, pdfDoc, fileName, callback))
                .setOutputFileName(fileName)
                .setPrintJobName(printJobName)
                .print();
    }

    /**
     * Print Dynamic XFA documents (that is xfaDoc.getType() == XFADoc.e_Dynamic)
     * <p>
     *  Note: Only when OS version is Kitkat and above (Android API >= 19) the print function can be used
     *
     * @param context The context to use. it must be instanceof Activity.
     * @param xfaDoc The {@link XFADoc} Object, it can not be empty .
     * @param printJobName print job name, it is can be null or empty.
     * @param fileName The document name which may be shown to the user and
     * is the file name if the content it describes is saved as a PDF.
     * Cannot be empty.
     * @param callback print callback {@link IPrintResultCallback}
     */
    public void startPrintJob(Context context, XFADoc xfaDoc, String printJobName, String fileName, IPrintResultCallback callback) {
        if (xfaDoc == null || xfaDoc.isEmpty()) return;
        new PDFPrint.Builder(context)
                .setAdapter(new XFAPrintAdapter(context, xfaDoc, fileName, callback))
                .setOutputFileName(fileName)
                .setPrintJobName(printJobName)
                .print();
    }

    /**
     * Note: This method is only used within RDK
     */
    public IPanelManager getPanelManager() {
        return mPanelManager;
    }

    UIExtensionsManager.MenuEventListener mMenuEventListener = new UIExtensionsManager.MenuEventListener() {
        @Override
        public void onTriggerDismissMenu() {
            if (getDocumentManager().getCurrentAnnot() != null) {
                getDocumentManager().setCurrentAnnot(null);
            }
        }
    };

    /**
     * Note: This method is only used within RDK
     */
    public Config getModulesConfig() {
        return mModulesConfig;
    }

    /**
     * @return true means The document can be modified
     */
    public boolean canModifyContents() {
        return getDocumentManager().canModifyContents();
    }

    /**
     * @return true means The document can add annot
     */
    public boolean canAddAnnot() {
        return getDocumentManager().canAddAnnot();
    }

    public void exitPanZoomMode() {
        PanZoomModule module = (PanZoomModule) getModuleByName(Module.MODULE_NAME_PANZOOM);
        if (module != null) {
            module.exit();
        }
    }

    private PDFViewCtrl.IConnectedPDFEventListener mConnectedPDFEventListener = new PDFViewCtrl.IConnectedPDFEventListener() {
        @Override
        public ClientInfo getClientInfo() {
            ClientInfo clientInfo = new ClientInfo();
            clientInfo.setDevice_id(AppDevice.getDeviceId(mAttachActivity));
            clientInfo.setDevice_name(AppDevice.getDeviceName());
            clientInfo.setDevice_model(AppDevice.getDeviceModel());
            clientInfo.setMac_address(AppDevice.getMacAddr(mAttachActivity));
            clientInfo.setOs("Android");
            clientInfo.setProduct_name(AppDevice.PRODUCT_NAME);
            clientInfo.setProduct_vendor(AppDevice.PRODUCT_VENDOR);
            clientInfo.setProduct_version(BuildConfig.VERSION_NAME);
            clientInfo.setProduct_language(AppSystemUtil.getLanguage(mContext));
            return clientInfo;
        }

        @Override
        public String getUserToken(PDFDoc pdfDoc) {
            return AccountModule.getInstance().getUserToken();
        }

        @Override
        public void onACLReceived(String acl, PDFDoc pdfDoc) {
            getDocumentManager().parseACL(acl);
        }
    };

    public static class Config {
        private static final String KEY_DEFAULTREADER = "defaultReader";
        private static final String KEY_MODULES = "modules";

        private static final String KEY_MODULE_READINGBOOKMARK = "readingbookmark";
        private static final String KEY_MODULE_OUTLINE = "outline";
        private static final String KEY_MODULE_ANNOTATIONS = "annotations";
        private static final String KEY_MODULE_THUMBNAIL = "thumbnail";
        private static final String KEY_MODULE_ATTACHMENT = "attachment";
        private static final String KEY_MODULE_SIGNATURE = "signature";
        private static final String KEY_MODULE_SEARCH = "search";
        private static final String KEY_MODULE_SELECTION = "selection";
        private static final String KEY_MODULE_PAGENAVIGATION = "pageNavigation";
        private static final String KEY_MODULE_ENCRYPTION = "encryption";
        private static final String KEY_MODULE_FORM = "form";

        private boolean isLoadReadingBookmark = true;
        private boolean isLoadOutline = true;
        private boolean isLoadAnnotations = true;
        private boolean isLoadThumbnail = true;
        private boolean isLoadAttachment = true;
        private boolean isLoadSignature = true;
        private boolean isLoadSearch = true;
        private boolean isLoadTextSelection = true;
        private boolean isLoadPageNavigation = true;
        private boolean isLoadFileEncryption = true;
        private boolean isLoadForm = true;

        private AnnotConfig annotConfig;

        public class AnnotConfig {

            // Text Markup
            private static final String KEY_TEXTMARK_HIGHLIGHT = "highlight";
            private static final String KEY_TEXTMARK_UNDERLINE = "underline";
            private static final String KEY_TEXTMARK_SQG = "squiggly";
            private static final String KEY_TEXTMARK_STO = "strikeout";
            private static final String KEY_TEXTMARK_INSERT = "inserttext";
            private static final String KEY_TEXTMARK_REPLACE = "replacetext";

            // Drawing
            private static final String KEY_DRAWING_LINE = "line";
            private static final String KEY_DRAWING_SQUARE = "rectangle";
            private static final String KEY_DRAWING_CIRCLE = "oval";
            private static final String KEY_DRAWING_ARROW = "arrow";
            private static final String KEY_DRAWING_PENCIL = "pencil";
            private static final String KEY_DRAWING_ERASER = "eraser";
            private static final String KEY_DRAWING_POLYGON = "polygon";
            private static final String KEY_DRAWING_CLOUD = "cloud";
            private static final String KEY_DRAWING_POLYLINE = "polyline";

            //Others
            private static final String KEY_TYPWRITER = "typewriter";
            private static final String KEY_CALLOUT = "callout";
            private static final String KEY_TEXTBOX = "textbox";
            private static final String KEY_NOTE = "note";
            private static final String KEY_STAMP = "stamp";
            private static final String KEY_FILEATTACH = "attachment";
            private static final String KEY_DISTANCE = "distance";
            private static final String KEY_IMAGE = "image";
            private static final String KEY_AUDIO = "audio";
            private static final String KEY_VIDEO = "video";

            private boolean isLoadHighlight = true;
            private boolean isLoadUnderline = true;
            private boolean isLoadSquiggly = true;
            private boolean isLoadStrikeout = true;
            private boolean isLoadInsertText = true;
            private boolean isLoadReplaceText = true;

            private boolean isLoadDrawLine = true;
            private boolean isLoadDrawSquare = true;
            private boolean isLoadDrawCircle = true;
            private boolean isLoadDrawArrow = true;
            private boolean isLoadDrawDistance = true;
            private boolean isLoadDrawPencil = true;
            private boolean isLoadEraser = true;
            private boolean isLoadDrawPolygon = true;
            private boolean isLoadDrawCloud = true;
            private boolean isLoadDrawPolyLine = true;

            private boolean isLoadTypewriter = true;
            private boolean isLoadTextbox = true;
            private boolean isLoadCallout = true;
            private boolean isLoadNote = true;
            private boolean isLoadStamp = true;
            private boolean isLoadFileattach = true;
            private boolean isLoadImage = true;
            private boolean isLoadAudio = true;
            private boolean isLoadVideo = true;

            private Map<String, Boolean> mMapSaveAnnotConfig = new HashMap<String, Boolean>();

            protected AnnotConfig() {
//                initMap();
            }

            protected void parseAnnotConfig(JSONObject annotObject) {

                if (annotObject != null) {
                    isLoadHighlight = getBooleanFromConfigModules(annotObject, KEY_TEXTMARK_HIGHLIGHT, true);
                    isLoadUnderline = getBooleanFromConfigModules(annotObject, KEY_TEXTMARK_UNDERLINE, true);
                    isLoadSquiggly = getBooleanFromConfigModules(annotObject, KEY_TEXTMARK_SQG, true);
                    isLoadStrikeout = getBooleanFromConfigModules(annotObject, KEY_TEXTMARK_STO, true);
                    isLoadInsertText = getBooleanFromConfigModules(annotObject, KEY_TEXTMARK_INSERT, true);
                    isLoadReplaceText = getBooleanFromConfigModules(annotObject, KEY_TEXTMARK_REPLACE, true);

                    isLoadDrawLine = getBooleanFromConfigModules(annotObject, KEY_DRAWING_LINE, true);
                    isLoadDrawSquare = getBooleanFromConfigModules(annotObject, KEY_DRAWING_SQUARE, true);
                    isLoadDrawCircle = getBooleanFromConfigModules(annotObject, KEY_DRAWING_CIRCLE, true);
                    isLoadDrawArrow = getBooleanFromConfigModules(annotObject, KEY_DRAWING_ARROW, true);
                    isLoadDrawPencil = getBooleanFromConfigModules(annotObject, KEY_DRAWING_PENCIL, true);
                    isLoadEraser = getBooleanFromConfigModules(annotObject, KEY_DRAWING_ERASER, true);
                    isLoadDrawPolygon = getBooleanFromConfigModules(annotObject, KEY_DRAWING_POLYGON, true);
                    isLoadDrawCloud = getBooleanFromConfigModules(annotObject, KEY_DRAWING_CLOUD, true);
                    isLoadDrawPolyLine = getBooleanFromConfigModules(annotObject, KEY_DRAWING_POLYLINE, true);

                    isLoadTypewriter = getBooleanFromConfigModules(annotObject, KEY_TYPWRITER, true);
                    isLoadCallout = getBooleanFromConfigModules(annotObject, KEY_CALLOUT, true);
                    isLoadTextbox = getBooleanFromConfigModules(annotObject, KEY_TEXTBOX, true);
                    isLoadNote = getBooleanFromConfigModules(annotObject, KEY_NOTE, true);
                    isLoadStamp = getBooleanFromConfigModules(annotObject, KEY_STAMP, true);
//                    isLoadFileattach = getBooleanFromConfigModules(annotObject, KEY_FILEATTACH, true);
                    isLoadDrawDistance = getBooleanFromConfigModules(annotObject, KEY_DISTANCE, true);
                    isLoadImage = getBooleanFromConfigModules(annotObject, KEY_IMAGE, true);
                    isLoadAudio = getBooleanFromConfigModules(annotObject, KEY_AUDIO, true);
                    isLoadVideo = getBooleanFromConfigModules(annotObject, KEY_VIDEO, true);
                }

                initMap();
            }

            protected void closeAnnotsConfig() {
                isLoadHighlight = false;
                isLoadUnderline = false;
                isLoadSquiggly = false;
                isLoadStrikeout = false;
                isLoadInsertText = false;
                isLoadReplaceText = false;

                isLoadDrawLine = false;
                isLoadDrawSquare = false;
                isLoadDrawCircle = false;
                isLoadDrawArrow = false;
                isLoadDrawPencil = false;
                isLoadEraser = false;
                isLoadDrawPolygon = false;
                isLoadDrawCloud = false;
                isLoadDrawPolyLine = false;

                isLoadTypewriter = false;
                isLoadCallout = false;
                isLoadTextbox = false;
                isLoadNote = false;
                isLoadStamp = false;
//                isLoadFileattach = false;
                isLoadDrawDistance = false;
                isLoadImage = false;
                isLoadAudio = false;
                isLoadVideo = false;
            }

            private void initMap() {
                mMapSaveAnnotConfig.put(KEY_TEXTMARK_HIGHLIGHT, isLoadHighlight);
                mMapSaveAnnotConfig.put(KEY_TEXTMARK_UNDERLINE, isLoadUnderline);
                mMapSaveAnnotConfig.put(KEY_TEXTMARK_SQG, isLoadSquiggly);
                mMapSaveAnnotConfig.put(KEY_TEXTMARK_STO, isLoadStrikeout);
                mMapSaveAnnotConfig.put(KEY_TEXTMARK_INSERT, isLoadInsertText);
                mMapSaveAnnotConfig.put(KEY_TEXTMARK_REPLACE, isLoadReplaceText);

                mMapSaveAnnotConfig.put(KEY_DRAWING_LINE, isLoadDrawLine);
                mMapSaveAnnotConfig.put(KEY_DRAWING_SQUARE, isLoadDrawSquare);
                mMapSaveAnnotConfig.put(KEY_DRAWING_CIRCLE, isLoadDrawCircle);
                mMapSaveAnnotConfig.put(KEY_DRAWING_ARROW, isLoadDrawArrow);
                mMapSaveAnnotConfig.put(KEY_DRAWING_PENCIL, isLoadDrawPencil);
                mMapSaveAnnotConfig.put(KEY_DRAWING_ERASER, isLoadEraser);
                mMapSaveAnnotConfig.put(KEY_DRAWING_POLYGON, isLoadDrawPolygon);
                mMapSaveAnnotConfig.put(KEY_DRAWING_CLOUD, isLoadDrawCloud);
                mMapSaveAnnotConfig.put(KEY_DRAWING_POLYLINE, isLoadDrawPolyLine);

                mMapSaveAnnotConfig.put(KEY_TYPWRITER, isLoadTypewriter);
                mMapSaveAnnotConfig.put(KEY_CALLOUT, isLoadCallout);
                mMapSaveAnnotConfig.put(KEY_TEXTBOX, isLoadTextbox);
                mMapSaveAnnotConfig.put(KEY_NOTE, isLoadNote);
                mMapSaveAnnotConfig.put(KEY_STAMP, isLoadStamp);
//                mMapSaveAnnotConfig.put(KEY_FILEATTACH, isLoadFileattach);
                mMapSaveAnnotConfig.put(KEY_DISTANCE, isLoadDrawDistance);
                mMapSaveAnnotConfig.put(KEY_IMAGE, isLoadImage);
                mMapSaveAnnotConfig.put(KEY_AUDIO,isLoadAudio);
                mMapSaveAnnotConfig.put(KEY_VIDEO,isLoadVideo);
            }

            public boolean isLoadHighlight() {
                return isLoadHighlight;
            }

            public boolean isLoadUnderline() {
                return isLoadUnderline;
            }

            public boolean isLoadSquiggly() {
                return isLoadSquiggly;
            }

            public boolean isLoadStrikeout() {
                return isLoadStrikeout;
            }

            public boolean isLoadInsertText() {
                return isLoadInsertText;
            }

            public boolean isLoadReplaceText() {
                return isLoadReplaceText;
            }

            public boolean isLoadDrawLine() {
                return isLoadDrawLine;
            }

            public boolean isLoadDrawSquare() {
                return isLoadDrawSquare;
            }

            public boolean isLoadDrawCircle() {
                return isLoadDrawCircle;
            }

            public boolean isLoadDrawArrow() {
                return isLoadDrawArrow;
            }

            public boolean isLoadDrawDistance() {
                return isLoadDrawDistance;
            }

            public boolean isLoadDrawPencil() {
                return isLoadDrawPencil;
            }

            public boolean isLoadEraser() {
                return isLoadEraser;
            }

            public boolean isLoadDrawPolygon() {
                return isLoadDrawPolygon;
            }

            public boolean isLoadDrawCloud() {
                return isLoadDrawCloud;
            }

            public boolean isLoadDrawPolyLine() {
                return isLoadDrawPolyLine;
            }

            public boolean isLoadTypewriter() {
                return isLoadTypewriter;
            }

            public boolean isLoadCallout() {
                return isLoadCallout;
            }

            public boolean isLoadTextbox() {
                return isLoadTextbox;
            }

            public boolean isLoadNote() {
                return isLoadNote;
            }

            public boolean isLoadStamp() {
                return isLoadStamp;
            }

            public boolean isLoadFileattach() {
                return isLoadFileattach;
            }

            public boolean isLoadImage() {
                return isLoadImage;
            }

            public boolean isLoadAudio() {
                return isLoadAudio;
            }

            public boolean isLoadVideo() {
                return isLoadVideo;
            }

            public Map<String, Boolean> getAnnotConfigMap() {
                return mMapSaveAnnotConfig;
            }

            protected void setLoadFileattach(boolean loadFileattach) {
                isLoadFileattach = loadFileattach;
            }
        }

        protected Config() {
            annotConfig = new AnnotConfig();
        }

        public Config(@NonNull InputStream stream) {
            annotConfig = new AnnotConfig();
            read(stream);
        }

        private boolean read(InputStream stream) {
            byte[] buffer = new byte[1 << 13];
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            int n = 0;
            try {
                while (-1 != (n = stream.read(buffer))) {
                    baos.write(buffer, 0, n);
                }

                String config = baos.toString("utf-8");
                if (config.trim().length() > 1) {
                    parseConfig(config);
                }
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            } finally {
                try {
                    baos.flush();
                    baos.close();
                } catch (IOException e) {
                }
            }

            return true;
        }

        private void parseConfig(@NonNull String config) {
            try {
                JSONObject jsonObject = new JSONObject(config);

                if (jsonObject.has(KEY_MODULES)) {
                    JSONObject modules = jsonObject.getJSONObject(KEY_MODULES);

                    if (null != modules) {
                        isLoadReadingBookmark = getBooleanFromConfigModules(modules, KEY_MODULE_READINGBOOKMARK, true);
                        isLoadOutline = getBooleanFromConfigModules(modules, KEY_MODULE_OUTLINE, true);
                        isLoadThumbnail = getBooleanFromConfigModules(modules, KEY_MODULE_THUMBNAIL, true);
                        isLoadAttachment = getBooleanFromConfigModules(modules, KEY_MODULE_ATTACHMENT, true);
                        isLoadSignature = getBooleanFromConfigModules(modules, KEY_MODULE_SIGNATURE, true);
                        isLoadSearch = getBooleanFromConfigModules(modules, KEY_MODULE_SEARCH, true);
                        isLoadTextSelection = getBooleanFromConfigModules(modules, KEY_MODULE_SELECTION, true);
                        isLoadPageNavigation = getBooleanFromConfigModules(modules, KEY_MODULE_PAGENAVIGATION, true);
                        isLoadFileEncryption = getBooleanFromConfigModules(modules, KEY_MODULE_ENCRYPTION, true);
                        isLoadForm = getBooleanFromConfigModules(modules, KEY_MODULE_FORM, true);

                        annotConfig.setLoadFileattach(isLoadAttachment);
                        if (modules.has(KEY_MODULE_ANNOTATIONS)) {
                            if (modules.get(KEY_MODULE_ANNOTATIONS) instanceof JSONObject) {
                                JSONObject annotObject = modules.getJSONObject(KEY_MODULE_ANNOTATIONS);
                                annotConfig.parseAnnotConfig(annotObject);

                                boolean isLoadAnnotsConfig = false;
                                Map<String, Boolean> mAnnotState = annotConfig.getAnnotConfigMap();
                                for (Boolean b : mAnnotState.values()) {
                                    if (true == b.booleanValue()) {
                                        isLoadAnnotsConfig = true;
                                        break;
                                    }
                                }
                                isLoadAnnotations = isLoadAnnotsConfig || isLoadAttachment();
                            } else {
                                boolean isLoadAnnotsConfig = getBooleanFromConfigModules(modules, KEY_MODULE_ANNOTATIONS, true);
                                if (isLoadAnnotsConfig == false) {
                                    annotConfig.closeAnnotsConfig();
                                }
                                isLoadAnnotations = isLoadAnnotsConfig || isLoadAttachment();
                            }
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        public AnnotConfig getAnnotConfig() {
            return annotConfig;
        }

        public boolean isLoadReadingBookmark() {
            return isLoadReadingBookmark;
        }

        public boolean isLoadOutline() {
            return isLoadOutline;
        }

        public boolean isLoadAnnotations() {
            return isLoadAnnotations;
        }

        public boolean isLoadThumbnail() {
            return isLoadThumbnail;
        }

        public boolean isLoadAttachment() {
            return isLoadAttachment;
        }

        public boolean isLoadSignature() {
            return isLoadSignature;
        }

        public boolean isLoadSearch() {
            return isLoadSearch;
        }

        public boolean isLoadTextSelection() {
            return isLoadTextSelection;
        }

        public boolean isLoadPageNavigation() {
            return isLoadPageNavigation;
        }

        public boolean isLoadFileEncryption() {
            return isLoadFileEncryption;
        }

        public boolean isLoadForm() {
            return isLoadForm;
        }

        private boolean getBooleanFromConfigModules(JSONObject modules, String name, boolean defaultValue) {
            try {
                if (modules.has(name) && modules.get(name) instanceof Boolean) {
                    boolean isLoadModule = modules.getBoolean(name);
                    return isLoadModule;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return defaultValue;
        }
    }

    private void release() {
        BlankSelectToolHandler selectToolHandler = (BlankSelectToolHandler) getToolHandlerByType(ToolHandler.TH_TYPE_BLANKSELECT);
        if (selectToolHandler != null) {
            selectToolHandler.unload();
            unregisterToolHandler(selectToolHandler);
        }

        for (Module module : mModules) {
            if (module instanceof LocalModule) continue;
            module.unloadModule();
        }

        mMenuEventListeners.clear();
        mLifecycleEventList.clear();
        mStateChangeEventList.clear();
        mXFAPageEventListeners.clear();
        mXFAWidgetEventListener.clear();
        mModules.clear();
        mModules = null;
        mPdfViewCtrl.unregisterDocEventListener(mDocEventListener);
        mPdfViewCtrl.unregisterRecoveryEventListener(mRecoveryEventListener);
        mPdfViewCtrl.unregisterDoubleTapEventListener(mDoubleTapEventListener);
        mPdfViewCtrl.unregisterTouchEventListener(mTouchEventListener);
        mPdfViewCtrl.unregisterPageEventListener(mPageEventListener);
        unregisterMenuEventListener(mMenuEventListener);
        getDocumentManager().destroy();

        mXFAPageEventListeners = null;
        mXFAWidgetEventListener = null;
        onFinishListener = null;
        mBackEventListener = null;
        mConnectedPDFEventListener = null;
        mTouchEventListener = null;
        mRecoveryEventListener = null;
        mDoubleTapEventListener = null;
        mPageEventListener = null;
        mDocEventListener = null;
        mMenuEventListener = null;
        mContext = null;
        mActivityLayout.removeAllViews();
        mActivityLayout = null;
        mCurToolHandler = null;
        mModulesConfig = null;
        mPanelManager = null;
        mPdfViewCtrl = null;
        mAttachActivity = null;

        documentManager = null;
        mPdfViewCtrl = null;
        mMainFrame.release();
        mMainFrame = null;
    }

    public void onCreate(Activity act, PDFViewCtrl pdfViewCtrl, Bundle bundle) {
        if (mMainFrame.getAttachedActivity() != null && mMainFrame.getAttachedActivity() != act) {
            for (ILifecycleEventListener listener : mLifecycleEventList) {
                listener.onDestroy(act);
            }
        }
        if (mLanguage == null)
            mLanguage = act.getResources().getConfiguration().locale.getLanguage();
        mMainFrame.setAttachedActivity(act);

        for (ILifecycleEventListener listener : mLifecycleEventList) {
            listener.onCreate(act, bundle);
        }
    }

    public void onConfigurationChanged(Activity act, Configuration newConfig) {
        if (mMainFrame.getAttachedActivity() != act)
            return;

        for (ConfigurationChangedListener listener : mConfigurationChangedListeners) {
            listener.onConfigurationChanged(newConfig);
        }
    }

    public void onStart(Activity act) {
        if (mMainFrame.getAttachedActivity() != act) return;
        for (ILifecycleEventListener listener : mLifecycleEventList) {
            listener.onStart(act);
        }
    }

    public void onPause(Activity act) {
        if (mMainFrame.getAttachedActivity() != act) return;
        for (ILifecycleEventListener listener : mLifecycleEventList) {
            listener.onPause(act);
        }
    }

    public void onHiddenChanged(boolean hidden) {
        for (ILifecycleEventListener listener : mLifecycleEventList) {
            listener.onHiddenChanged(hidden);
        }
    }

    public void onResume(Activity act) {
        if (mMainFrame.getAttachedActivity() != act) return;
        for (ILifecycleEventListener listener : mLifecycleEventList) {
            listener.onResume(act);
        }
        String curLanguage = act.getResources().getConfiguration().locale.getLanguage();
        if (!mLanguage.equals(curLanguage)) {
            mLanguage = curLanguage;
        }
    }

    public void onStop(Activity act) {
        if (mMainFrame.getAttachedActivity() != act) return;
        for (ILifecycleEventListener listener : mLifecycleEventList) {
            listener.onStop(act);
        }
    }

    public void onDestroy(Activity act) {
        if (mMainFrame.getAttachedActivity() != act) return;
        for (ILifecycleEventListener listener : mLifecycleEventList) {
            listener.onDestroy(act);
        }
        mMainFrame.setAttachedActivity(null);
        closeAllDocuments();
        release();
        AppDialogManager.getInstance().closeAllDialog();
    }

    public void handleActivityResult(Activity act, int requestCode, int resultCode, Intent data) {
        if (mMainFrame.getAttachedActivity() != act) return;

        mPdfViewCtrl.handleActivityResult(requestCode, resultCode, data);

        for (ILifecycleEventListener listener : mLifecycleEventList) {
            listener.onActivityResult(act, requestCode, resultCode, data);
        }
    }

    public void openDocument(String path, byte[] password) {
        _resetStatusBeforeOpen();
        mProgressMsg = "opening";
        showProgressDlg();

        setFilePath(path);
        mPdfViewCtrl.openDoc(path, password);
    }

    @Override
    public boolean registerLifecycleListener(ILifecycleEventListener listener) {
        mLifecycleEventList.add(listener);
        return true;
    }

    @Override
    public boolean unregisterLifecycleListener(ILifecycleEventListener listener) {
        mLifecycleEventList.remove(listener);
        return true;
    }

    @Override
    public boolean registerStateChangeListener(IStateChangeListener listener) {
        mStateChangeEventList.add(listener);
        return false;
    }

    @Override
    public boolean unregisterStateChangeListener(IStateChangeListener listener) {
        mStateChangeEventList.remove(listener);
        return false;
    }

    /**
     * Note: This method is only used within RDK
     */
    @Override
    public int getState() {
        return mState;
    }

    /**
     * Note: This method is only used within RDK
     */
    @Override
    public void changeState(int state) {
        int oldState = mState;
        mState = state;
        for (IStateChangeListener listener : mStateChangeEventList) {
            listener.onStateChanged(oldState, state);
        }
    }

    @Override
    public IMainFrame getMainFrame() {
        return mMainFrame;
    }

    /**
     * Get the PDF view control.
     *
     * @return A <CODE>PDFViewCtrl</CODE> object which indicates the current PDF view control.<br>
     */
    @Override
    public PDFViewCtrl getPDFViewCtrl() {
        return mPdfViewCtrl;
    }

    @Override
    public IBarsHandler getBarManager() {
        return mBaseBarMgr;
    }

    @Override
    public IMultiLineBar getSettingBar() {
        return mMainFrame.getSettingBar();
    }

    @Override
    public DocumentManager getDocumentManager() {
        return documentManager.on(mPdfViewCtrl);
    }

    @Override
    public RelativeLayout getContentView() {
        return mActivityLayout;
    }

    @Override
    public void backToPrevActivity() {
        if (getCurrentToolHandler() != null) {
            setCurrentToolHandler(null);
        }

        if (getDocumentManager() != null && getDocumentManager().getCurrentAnnot() != null) {
            getDocumentManager().setCurrentAnnot(null);
        }

        if (mPdfViewCtrl.isDynamicXFA()) {
            DynamicXFAModule dynamicXFAModule = (DynamicXFAModule) getModuleByName(Module.MODULE_NAME_DYNAMICXFA);
            if (dynamicXFAModule != null && dynamicXFAModule.getCurrentXFAWidget() != null) {
                dynamicXFAModule.setCurrentXFAWidget(null);
            }
        }

        if (mMainFrame.getAttachedActivity() == null) {
            mProgressMsg = "Closing";
            closeAllDocuments();
            return;
        }

        if (mPdfViewCtrl.getDoc() == null || !getDocumentManager().isDocModified()) {
            mProgressMsg = "Closing";
            closeAllDocuments();
            return;
        }

        final boolean hideSave = mPdfViewCtrl.isDynamicXFA() ? false : !getDocumentManager().canModifyContents();

        AlertDialog.Builder builder = new AlertDialog.Builder(mMainFrame.getAttachedActivity());
        String[] items;
        if (hideSave) {
            items = new String[]{
                    AppResource.getString(mContext, R.string.rv_back_saveas),
                    AppResource.getString(mContext, R.string.rv_back_discard_modify),
            };
        } else {
            items = new String[]{
                    AppResource.getString(mContext, R.string.rv_back_save),
                    AppResource.getString(mContext, R.string.rv_back_saveas),
                    AppResource.getString(mContext, R.string.rv_back_discard_modify),
            };
        }

        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if (hideSave) {
                    which += 1;
                }
                switch (which) {
                    case 0: // save
                        isCloseDocAfterSaving = true;
                        mProgressMsg = "Saving";
                        showProgressDlg();
                        if (mUserSavePath != null && mUserSavePath.length() > 0 && !mUserSavePath.equalsIgnoreCase(mDocPath)) {
                            File userSaveFile = new File(mUserSavePath);
                            File defaultSaveFile = new File(mDocPath);
                            if (userSaveFile.getParent().equalsIgnoreCase(defaultSaveFile.getParent())) {
                                isSaveDocInCurPath = true;
                                mSavePath = mUserSavePath;
                            } else {
                                isSaveDocInCurPath = false;
                            }
                            mPdfViewCtrl.saveDoc(mUserSavePath, mSaveFlag);
                        } else {
                            isSaveDocInCurPath = true;
                            mPdfViewCtrl.saveDoc(getCacheFile(), mSaveFlag);
                        }
                        break;
                    case 1: // save as
                        mProgressMsg = "Saving";
                        onSaveAsClicked();
                        break;
                    case 2: // discard modify
                        mProgressMsg = "Closing";
                        closeAllDocuments();
                        break;
                    default:
                        break;
                }
                dialog.dismiss();
                mSaveAlertDlg = null;
            }

            void showInputFileNameDialog(final String fileFolder) {
                String newFilePath = fileFolder + "/" + AppFileUtil.getFileName(mDocPath);
                final String filePath = AppFileUtil.getFileDuplicateName(newFilePath);
                final String fileName = AppFileUtil.getFileNameWithoutExt(filePath);

                final UITextEditDialog rmDialog = new UITextEditDialog(mMainFrame.getAttachedActivity());
                rmDialog.setPattern("[/\\:*?<>|\"\n\t]");
                rmDialog.setTitle(AppResource.getString(mContext, R.string.fx_string_saveas));
                rmDialog.getPromptTextView().setVisibility(View.GONE);
                rmDialog.getInputEditText().setText(fileName);
                rmDialog.getInputEditText().selectAll();
                rmDialog.show();
                AppUtil.showSoftInput(rmDialog.getInputEditText());

                rmDialog.getOKButton().setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        rmDialog.dismiss();
                        String inputName = rmDialog.getInputEditText().getText().toString();
                        String newPath = fileFolder + "/" + inputName;
                        newPath += ".pdf";
                        File file = new File(newPath);
                        if (file.exists()) {
                            showAskReplaceDialog(fileFolder, newPath);
                        } else {
                            mProgressMsg = "Saving";
                            showProgressDlg();
                            isCloseDocAfterSaving = true;
                            mSavePath = newPath;
                            mPdfViewCtrl.saveDoc(newPath, mSaveFlag);
                        }
                    }
                });
            }

            void showAskReplaceDialog(final String fileFolder, final String newPath) {
                final UITextEditDialog rmDialog = new UITextEditDialog(mMainFrame.getAttachedActivity());
                rmDialog.setTitle(AppResource.getString(mContext, R.string.fx_string_saveas));
                rmDialog.getPromptTextView().setText(AppResource.getString(mContext, R.string.fx_string_filereplace_warning));
                rmDialog.getInputEditText().setVisibility(View.GONE);
                rmDialog.show();

                rmDialog.getOKButton().setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        rmDialog.dismiss();
                        mSavePath = newPath;
                        isCloseDocAfterSaving = true;
                        mProgressMsg = "Saving";
                        showProgressDlg();
                        if (newPath.equalsIgnoreCase(mDocPath)) {
                            isSaveDocInCurPath = true;
                            mPdfViewCtrl.saveDoc(getCacheFile(), mSaveFlag);
                        } else {
                            isSaveDocInCurPath = false;
                            mPdfViewCtrl.saveDoc(newPath, mSaveFlag);
                        }
                    }
                });

                rmDialog.getCancelButton().setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        rmDialog.dismiss();
                        showInputFileNameDialog(fileFolder);
                    }
                });
            }

            void onSaveAsClicked() {
                final UIFolderSelectDialog dialog = new UIFolderSelectDialog(mMainFrame.getAttachedActivity());
                dialog.setFileFilter(new FileFilter() {
                    @Override
                    public boolean accept(File pathname) {
                        return !(pathname.isHidden() || !pathname.canRead()) && !pathname.isFile();
                    }
                });
                dialog.setTitle(AppResource.getString(mContext, R.string.fx_string_saveas));
                dialog.setButton(MatchDialog.DIALOG_OK | MatchDialog.DIALOG_CANCEL);
                dialog.setListener(new MatchDialog.DialogListener() {
                    @Override
                    public void onResult(long btType) {
                        if (btType == MatchDialog.DIALOG_OK) {
                            String fileFolder = dialog.getCurrentPath();
                            showInputFileNameDialog(fileFolder);
                        }
                        dialog.dismiss();
                    }

                    @Override
                    public void onBackClick() {
                    }
                });
                dialog.showDialog();
            }
        });

        mSaveAlertDlg = builder.create();
        mSaveAlertDlg.setCanceledOnTouchOutside(true);
        mSaveAlertDlg.show();
    }

    @Override
    public void setBackEventListener(BackEventListener listener) {
        mBackEventListener = listener;
    }

    @Override
    public BackEventListener getBackEventListener() {
        return mBackEventListener;
    }

    public boolean onKeyDown(Activity act, int keyCode, KeyEvent event) {
        if (mMainFrame.getAttachedActivity() != act) return false;
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (backToNormalState()) return true;

            if (event.getRepeatCount() == 0) {
                backToPrevActivity();
                return true;
            }
        }
        return false;
    }

    public boolean backToNormalState() {
        if (mPdfViewCtrl.isDynamicXFA()) {
            DynamicXFAModule dynamicXFAModule = (DynamicXFAModule) getModuleByName(Module.MODULE_NAME_DYNAMICXFA);
            if (dynamicXFAModule != null && dynamicXFAModule.onKeyBack()) {
                changeState(ReadStateConfig.STATE_NORMAL);
                mMainFrame.showToolbars();
                return true;
            }
        }

        SearchModule searchModule = ((SearchModule) getModuleByName(Module.MODULE_NAME_SEARCH));
        if (searchModule != null && searchModule.onKeyBack()) {
            changeState(ReadStateConfig.STATE_NORMAL);
            mMainFrame.showToolbars();
            return true;
        }

        FormFillerModule formFillerModule = (FormFillerModule) getModuleByName(Module.MODULE_NAME_FORMFILLER);
        if (formFillerModule != null && formFillerModule.onKeyBack()) {
            changeState(ReadStateConfig.STATE_NORMAL);
            mMainFrame.showToolbars();
            return true;
        }

        ToolHandler currentToolHandler = getCurrentToolHandler();
        SignatureModule signature_module = (SignatureModule) getModuleByName(Module.MODULE_NAME_PSISIGNATURE);
        if (signature_module != null && currentToolHandler instanceof SignatureToolHandler && signature_module.onKeyBack()) {
            changeState(ReadStateConfig.STATE_NORMAL);
            mMainFrame.showToolbars();
            mPdfViewCtrl.invalidate();
            return true;
        }

        FileAttachmentModule fileAttachmentModule = (FileAttachmentModule) getModuleByName(Module.MODULE_NAME_FILEATTACHMENT);
        if (fileAttachmentModule != null && fileAttachmentModule.onKeyBack()) {
            mMainFrame.showToolbars();
            return true;
        }

        FileSpecPanelModule fileSpecPanelModule = (FileSpecPanelModule) getModuleByName(Module.MODULE_NAME_FILE_PANEL);
        if (fileSpecPanelModule != null && fileSpecPanelModule.onKeyBack()) {
            mMainFrame.showToolbars();
            return true;
        }

        CropModule cropModule = (CropModule) getModuleByName(Module.MODULE_NAME_CROP);
        if (cropModule != null && cropModule.onKeyBack()) {
            mMainFrame.showToolbars();
            return true;
        }

        PanZoomModule panZoomModule = (PanZoomModule) getModuleByName(Module.MODULE_NAME_PANZOOM);
        if (panZoomModule != null && panZoomModule.exit()) {
            mMainFrame.showToolbars();
            return true;
        }

        BlankSelectToolHandler selectToolHandler = (BlankSelectToolHandler) getToolHandlerByType(ToolHandler.TH_TYPE_BLANKSELECT);
        if (selectToolHandler != null && selectToolHandler.onKeyBack()) {
            return true;
        }

        TextSelectModule textSelectModule = (TextSelectModule) getModuleByName(Module.MODULE_NAME_SELECTION);
        if (textSelectModule != null && textSelectModule.onKeyBack()) {
            return true;
        }

        if (getDocumentManager().onKeyBack()) {
            return true;
        }

        if (currentToolHandler != null) {
            setCurrentToolHandler(null);
            return true;
        }

        if (getState() != ReadStateConfig.STATE_NORMAL) {
            changeState(ReadStateConfig.STATE_NORMAL);
            return true;
        }

        return false;
    }

    /**
     * Note: This method is only used within RDK
     */
    public void setFilePath(String path) {
        mDocPath = path;
        MoreMenuModule module = ((MoreMenuModule) getModuleByName(Module.MODULE_MORE_MENU));
        if (module != null) {
            module.setFilePath(path);
        }
    }

    public boolean onPrepareOptionsMenu(Activity act, Menu menu) {
        return mMainFrame.getAttachedActivity() == act && mPdfViewCtrl.getDoc() != null;
    }

    public void enableTopToolbar(boolean isEnabled) {
        if (mMainFrame != null) {
            mMainFrame.enableTopToolbar(isEnabled);
        }
    }

    public void enableBottomToolbar(boolean isEnabled) {
        if (mMainFrame != null) {
            mMainFrame.enableBottomToolbar(isEnabled);
        }
    }

    /**
     * Note: This method is only used within RDK
     */
    public void setSaveDocFlag(int flag) {
        mSaveFlag = flag;
    }

    public int getSaveDocFlag() {
        return mSaveFlag;
    }

    private void openDocumentFailed() {
        if (mMainFrame.getAttachedActivity() != null){
            if (onFinishListener != null){
                onFinishListener.onFinish();
            } else {
                mMainFrame.getAttachedActivity().finish();
            }
        }
    }

    private void _resetStatusAfterClose() {
        changeState(ReadStateConfig.STATE_NORMAL);
    }

    private void _resetStatusBeforeOpen() {
        mMainFrame.showToolbars();
        mState = ReadStateConfig.STATE_NORMAL;
    }

    private void closeAllDocuments() {
        if (!bDocClosed) {
            _closeDocument();
        } else if (mMainFrame.getAttachedActivity() != null) {
            if (onFinishListener != null){
                onFinishListener.onFinish();
            } else {
                mMainFrame.getAttachedActivity().finish();
            }
        }
    }

    private void showProgressDlg() {
        if (mProgressDlg == null && mMainFrame.getAttachedActivity() != null) {
            mProgressDlg = new ProgressDialog(mMainFrame.getAttachedActivity());
            mProgressDlg.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mProgressDlg.setCancelable(false);
            mProgressDlg.setIndeterminate(false);
        }

        if (mProgressDlg != null && !mProgressDlg.isShowing()) {
            mProgressDlg.setMessage(mProgressMsg);
            AppDialogManager.getInstance().showAllowManager(mProgressDlg, null);
        }
    }

    private void _closeDocument() {
        showProgressDlg();

        mPdfViewCtrl.closeDoc();
        _resetStatusAfterClose();
        mMainFrame.resetMaskView();
    }

    private void closeDocumentSucceed() {
        if (mMainFrame != null && mMainFrame.getAttachedActivity() != null) {

            if (onFinishListener != null){
                onFinishListener.onFinish();
            } else {
                mMainFrame.getAttachedActivity().finish();
            }
        }

        if (isSaveDocInCurPath) {
            if (currentFileCachePath == null) return;
            if (mDocPath.endsWith(".ppdf")) {
                currentFileCachePath = AppFileUtil.replaceFileExtension(currentFileCachePath, ".ppdf");
            }
            File file = new File(currentFileCachePath);
            File docFile = new File(mDocPath);
            if (file.exists()) {
                docFile.delete();
                if (!file.renameTo(docFile))
                    UIToast.getInstance(mContext).show("Save document failed!");
            } else {
                UIToast.getInstance(mContext).show("Save document failed!");
            }
        }
    }

    private void updateThumbnail(String path) {
        LocalModule module = (LocalModule) getModuleByName(Module.MODULE_NAME_LOCAL);
        if (module != null && path != null) {
            module.updateThumbnail(path);
        }
    }

    private String getCacheFile() {
        mSavePath = mDocPath;
        File file = new File(mDocPath);
        String dir = file.getParent() + "/";
        while (file.exists()) {
            currentFileCachePath = dir + AppDmUtil.randomUUID(null) + ".pdf";
            file = new File(currentFileCachePath);
        }
        return currentFileCachePath;
    }

    private void changeToolBarState(ToolHandler oldToolHandler, ToolHandler newToolHandler) {
        if (newToolHandler instanceof SignatureToolHandler) {
            mMainFrame.resetAnnotCustomBottomBar();
            mMainFrame.resetAnnotCustomTopBar();
            changeState(ReadStateConfig.STATE_SIGNATURE);
        } else if (newToolHandler != null) {
            changeState(ReadStateConfig.STATE_ANNOTTOOL);
        } else if (getState() == ReadStateConfig.STATE_ANNOTTOOL) {
            changeState(ReadStateConfig.STATE_EDIT);
        }
        if (oldToolHandler instanceof SignatureToolHandler && newToolHandler == null)
            changeState(ReadStateConfig.STATE_NORMAL);
        if (oldToolHandler != null && newToolHandler != null) {
            mMainFrame.showToolbars();
        }
    }

    public void setSavePath(String savePath) {
        mUserSavePath = savePath;
    }

    public String getSavePath() {
        return mUserSavePath;
    }
}
