/**
 * Copyright (C) 2003-2018, Foxit Software Inc..
 * All Rights Reserved.
 * <p>
 * http://www.foxitsoftware.com
 * <p>
 * The following code is copyrighted and is the proprietary of Foxit Software Inc.. It is not allowed to
 * distribute any parts of Foxit PDF SDK to third party or public without permission unless an agreement
 * is signed between Foxit Software Inc. and customers to explicitly grant customers permissions.
 * Review legal.txt for additional license and legal information.
 */
package com.foxit.uiextensions.modules.signature;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.foxit.sdk.PDFViewCtrl;
import com.foxit.uiextensions.utils.AppDisplay;


public class SignatureFragment extends DialogFragment {


    interface SignatureInkCallback {
        void onSuccess(boolean isFromFragment, Bitmap bitmap, Rect rect, int color, String dsgPath);

        void onBackPressed();
    }

    private Context mContext;
    private ViewGroup mParent;
    private PDFViewCtrl mPdfViewCtrl;
    private SignatureViewController mSupport;
    private SignatureInkCallback mCallback;
    private int mOrientation;
    private boolean mAttach;
    private AppDisplay mDisplay;
    private SignatureInkItem mInkItem;

    boolean isAttached() {
        return mAttach;
    }

    void setInkCallback(SignatureInkCallback callback) {
        this.mCallback = callback;
    }

    void setInkCallback(SignatureInkCallback callback, SignatureInkItem item) {
        this.mCallback = callback;
        mInkItem = item;
    }

    private boolean checkInit() {

        return mCallback != null;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        int theme;
        if (Build.VERSION.SDK_INT >= 21) {
            theme = android.R.style.Theme_Holo_Light_NoActionBar_Fullscreen;
        } else if (Build.VERSION.SDK_INT >= 14) {
            theme = android.R.style.Theme_DeviceDefault_Light_NoActionBar_Fullscreen;
        } else if (Build.VERSION.SDK_INT >= 13) {
            theme = android.R.style.Theme_Holo_Light_NoActionBar_Fullscreen;
        } else {
            theme = android.R.style.Theme_Light_NoTitleBar_Fullscreen;
        }
        setStyle(STYLE_NO_TITLE, theme);

    }

    protected void init(Context context, ViewGroup parent, PDFViewCtrl pdfViewCtrl) {
        mContext = context;
        mParent = parent;
        mPdfViewCtrl = pdfViewCtrl;

        mDisplay = AppDisplay.getInstance(mContext);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        if (!checkInit()) {
            getActivity().getSupportFragmentManager().popBackStack();
            return;
        }
        if (mSupport == null) {
            mSupport = new SignatureViewController(mContext, mParent, mPdfViewCtrl, mCallback);
        }
        mOrientation = activity.getRequestedOrientation();
        if (android.os.Build.VERSION.SDK_INT <= 8) {
            activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        } else {
            activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
        }
        mAttach = true;
    }

    private boolean mCheckCreateView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        if (mSupport == null) {
            getActivity().getSupportFragmentManager().popBackStack();
            return super.onCreateView(inflater, container, savedInstanceState);
        }
        ViewGroup view = (ViewGroup) mSupport.getView().getParent();
        if (view != null) {

            view.removeView(mSupport.getView());
        }

        this.getDialog().setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    mCallback.onBackPressed();
                    dismiss();
                    return true;
                }
                return false;
            }
        });

        mSupport.resetLanguage();
        mCheckCreateView = true;
        int screenWidth = mDisplay.getScreenWidth();
        int screenHeight = mDisplay.getScreenHeight();
        if (screenWidth < screenHeight) {
            screenWidth = mDisplay.getScreenHeight();
            screenHeight = mDisplay.getScreenHeight();
        }

        if (mInkItem == null) {
            mSupport.init(screenWidth, screenHeight);
        } else {
            mSupport.init(screenWidth,
                    screenHeight,
                    mInkItem.key,
                    mInkItem.bitmap,
                    mInkItem.rect,
                    mInkItem.color,
                    mInkItem.diameter,
                    mInkItem.dsgPath);
        }

        return mSupport.getView();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        if (!mCheckCreateView && mDisplay.getScreenWidth() > mDisplay.getScreenHeight()) {
            mCheckCreateView = true;

            if (mInkItem == null) {
                mSupport.init(mDisplay.getScreenWidth(), mDisplay.getScreenHeight());
            } else {
                mSupport.init(mDisplay.getScreenWidth(),
                        mDisplay.getScreenHeight(),
                        mInkItem.key,
                        mInkItem.bitmap,
                        mInkItem.rect,
                        mInkItem.color,
                        mInkItem.diameter,
                        mInkItem.dsgPath);
            }
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        getActivity().setRequestedOrientation(mOrientation);

        if (mSupport != null) {
            mSupport.unInit();
        }
        mAttach = false;
    }

}
